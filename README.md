# Sample Java Spring Boot Application

## ..::Still In Progress::..

Application consists of three main modules and supporting modules.

- Foosball BE module [here](./wbbe/README.md)
- Number encoding module [here](./number-encoding/README.md)
- Other module (containing simple algorithms) [here](./other/README.md)

## How to run application (on Windows)

### Run as spring-boot application

##### Steps

- Download project
- Run ````mvn clean install```` in project root
- Locate [run-springboot.bat](./war-runner/run-springboot.bat)
- Run batch script

## DB

You can access H2 db console via ````http://localhost:8080/h2```` with default credentials (
user ````sa```` with no password) and jdbc url ````jdbc:h2:mem:foosballdb````.

**Note** that application use **in-memory db**, so if you turn it off the data will be lost.

## Endpoints

Actuator is available on ````http://localhost:8080/actuator````

API is described [here](./wbbe/controller/README.md) and [here](./number-encoding/controller/README.md).

## Authorization and Headers

By default, all requests required authorization. \
Testing JWT token can be used
````eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiIiLCJpYXQiOjE2MTE2MDE5NzIsImV4cCI6MjUyNjc1MDc3MiwiYXVkIjoiaHJjdiIsInN1YiI6ImFkbWluX2p3dCJ9.6kJJJ4QD1CfcD9LNI_3Fac15x4T1n5cx56iVOYFPnPo````

Or can be generated using claims
provided [here](./config/src/main/java/sk/hrcv/config/security/jwt/AuthenticationService.java)

Requests also required headers Content-Type: application/json and call-id-header: {call identifier}

## Logs and configuration

Logs of running application are located in [/config/src/main/resources/log](./config/src/main/resources/log)

Configuration yaml is located [here](./config/src/main/resources/conf/application.yml)

## TO-DO List

- [x] Fix tests
- [ ] Implement real integration tests
- [ ] Resolve TODOs
- [ ] Clean-up poms and dependencies
- [ ] Implement swagger, hateoas
- [ ] Connection to remote DB

## Contact

<r.hricov[at]gmail.com> Radoslav Hricov