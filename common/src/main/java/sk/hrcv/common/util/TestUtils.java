package sk.hrcv.common.util;

import com.google.common.io.CharStreams;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public final class TestUtils {

    private static final Logger log = LoggerFactory.getLogger(TestUtils.class);

    private TestUtils() {
        //utility class with static methods
    }

    /**
     * This method reads resources from input and returns new TestRequestResponse that contains request and expected response
     * for testing purpose
     */
    public static TestRequestResponse prepareTestRequestResponse(final Resource requestResource, final Resource responseResource) {
        return new TestRequestResponse(
                readResource(requestResource),
                readResource(responseResource)
        );
    }

    /**
     * This method reads resources from input and returns it as expected response string for testing purpose
     */
    public static String prepareTestResponse(final Resource responseResource) {
        return readResource(responseResource);
    }

    /**
     * Returns object built from json
     */
    public static <T> T getMockedResponse(Class<T> clazz, final Resource mockedResponseResource) {
        String stringJsonResponse = readResource(mockedResponseResource);

        Gson gson = new GsonBuilder().registerTypeAdapter(LocalDateTime.class, (JsonDeserializer<LocalDateTime>) (json, type, jsonDeserializationContext) ->
                LocalDateTime.parse(json.getAsJsonPrimitive().getAsString(), DateTimeFormatter.ISO_DATE_TIME))
                .create();

        return gson.fromJson(stringJsonResponse, clazz);
    }

    private static String readResource(Resource resource) {
        String s;

        try {
            s = CharStreams.toString(new InputStreamReader(resource.getInputStream(), StandardCharsets.UTF_8));
            log.debug("Resource {} successfully read", resource.getFilename());
        } catch (final IOException e) {
            throw new IllegalStateException("No resource found: " + ((ClassPathResource) resource).getPath());
        }

        return s;
    }

    public static final class TestRequestResponse {
        private final String request;
        private final String response;

        public TestRequestResponse(String request, String response) {
            this.request = request;
            this.response = response;
        }

        public String getRequest() {
            return request;
        }

        public String getResponse() {
            return response;
        }
    }
}