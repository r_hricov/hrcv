package sk.hrcv.common.util

import org.slf4j.Logger
import org.slf4j.LoggerFactory


inline fun <reified R : Any> R.logger(): Lazy<Logger> = lazy {
    LoggerFactory.getLogger(this::class.java.name)
}