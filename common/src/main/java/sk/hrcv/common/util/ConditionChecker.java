package sk.hrcv.common.util;

import org.jetbrains.annotations.Contract;
import sk.hrcv.config.exception.CommonBusinessException;

import javax.annotation.Nullable;
import java.util.Objects;

/**
 * This utility checks the most common conditions. If condition is not fulfilled a {@link CommonBusinessException} containing errorCode is thrown
 */
public final class ConditionChecker {

    private ConditionChecker() {
        //utility class with static methods
    }

    @Contract("true, _, _ -> fail")
    public static void checkCondition(boolean condition, String errorCode, Object... parameters) {
        if (condition) {
            throw new CommonBusinessException(errorCode);
        }
    }

    @Contract("null, _, _ -> fail")
    public static void assertNotNull(@Nullable Object nullableObject, String errorCode, Object... parameters) {
        if (Objects.isNull(nullableObject)) {
            throw new CommonBusinessException(errorCode);
        }
    }
}
