package sk.hrcv.common.validator;

import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Nullable;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.List;

public class NameAbbrListValidator implements ConstraintValidator<TeamNameAbbr, List<String>> {

    private final TeamNameAbbrValidator teamNameAbbrValidator;

    private TeamNameAbbr teamNameAbbr;

    @Autowired
    public NameAbbrListValidator(TeamNameAbbrValidator teamNameAbbrValidator) {
        this.teamNameAbbrValidator = teamNameAbbrValidator;
    }

    @Override
    public void initialize(TeamNameAbbr constraintAnnotation) {
        this.teamNameAbbr = constraintAnnotation;
    }

    @Override
    public boolean isValid(@Nullable List<String> values, ConstraintValidatorContext context) {
        boolean isValid = true;

        if (values != null) {
            for (String value : values) {
                isValid = isValid && teamNameAbbrValidator.validate(value, context, teamNameAbbr.ignoreCase());
            }
        }

        return isValid;
    }
}
