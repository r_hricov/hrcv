package sk.hrcv.common.validator;

import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.Nullable;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class NameAbbrValidator implements ConstraintValidator<TeamNameAbbr, String> {

    private final TeamNameAbbrValidator teamNameAbbrValidator;

    private TeamNameAbbr teamNameAbbr;

    @Autowired
    public NameAbbrValidator(TeamNameAbbrValidator teamNameAbbrValidator) {
        this.teamNameAbbrValidator = teamNameAbbrValidator;
    }

    @Override
    public void initialize(TeamNameAbbr constraintAnnotation) {
        this.teamNameAbbr = constraintAnnotation;
    }

    @Override
    public boolean isValid(@Nullable String value, ConstraintValidatorContext context) {
        return teamNameAbbrValidator.validate(value, context, teamNameAbbr.ignoreCase());
    }
}
