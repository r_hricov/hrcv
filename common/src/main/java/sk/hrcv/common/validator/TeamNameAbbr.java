package sk.hrcv.common.validator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({
        ElementType.METHOD,
        ElementType.FIELD,
        ElementType.PARAMETER
})
@Constraint(validatedBy = {
        NameAbbrValidator.class,
        NameAbbrListValidator.class
})
public @interface TeamNameAbbr {

    boolean ignoreCase() default true;

    // additional required by ConstraintValidator
    String message() default "not defined";

    Class[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
