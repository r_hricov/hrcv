package sk.hrcv.common.validator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Nullable;
import javax.sql.DataSource;
import javax.validation.ConstraintValidatorContext;
import java.util.Objects;

@Component
public class TeamNameAbbrValidator {
    private static final String QUERY = "SELECT COUNT(*) FROM HRCV.TEAM t WHERE t.TEAM_NAME_ABBR = ?";
    private static final String QUERY_IGNORING_CASE = "SELECT COUNT(*) FROM HRCV.TEAM t WHERE UPPER(t.TEAM_NAME_ABBR) = UPPER(?)";

    private static final Logger log = LoggerFactory.getLogger(TeamNameAbbrValidator.class);
    private final JdbcTemplate jdbcTemplate;
    private final CacheManager cacheManager;

    @Autowired
    public TeamNameAbbrValidator(@Qualifier("foosballDbDataSource") DataSource dataSource,
                                 @Qualifier("teamNameAbbrValidatorCacheManager") CacheManager cacheManager) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.cacheManager = cacheManager;
    }

    public boolean validate(@Nullable String value, ConstraintValidatorContext context, boolean ignoreCase) {
        if (value == null) {
            return true;
        }

        Cache cache = cacheManager.getCache("teamNameAbbr");
        Objects.requireNonNull(cache);
        Boolean isValid = cache.get(value, Boolean.class);

        log.debug(String.format("Cached value for key [%s] is: [%s]", value, isValid));

        if (isValid == null) {
            String query = ignoreCase ? QUERY_IGNORING_CASE : QUERY;
            //only one result should be find so returned 0/1 from sql is translated to false/true
            isValid = jdbcTemplate.queryForObject(query, Boolean.class, value);
            cache.put(value, isValid);

            if (isValid == null || !isValid) {
                String constraintValidationTemplateMessage = String.format("Not valid name found: %s", value);
                context.disableDefaultConstraintViolation();
                context.buildConstraintViolationWithTemplate(constraintValidationTemplateMessage)
                        .addBeanNode()
                        .addConstraintViolation();
                log.debug(constraintValidationTemplateMessage);
            }
        }

        return Boolean.TRUE.equals(isValid);
    }
}
