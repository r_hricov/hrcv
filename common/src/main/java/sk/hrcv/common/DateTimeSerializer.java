package sk.hrcv.common;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * https://www.baeldung.com/jackson-serialize-dates
 */
public class DateTimeSerializer extends StdSerializer<LocalDateTime> {
    public DateTimeSerializer() {
        this(null);
    }

    public DateTimeSerializer(Class<LocalDateTime> t) {
        super(t);
    }

    @Override
    public void serialize(LocalDateTime value, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeString(DateTimeFormatter.ISO_DATE_TIME.format(value));
    }
}
