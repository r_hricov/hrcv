package sk.hrcv.common;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.time.LocalDateTime;

@Deprecated
public class DateTimeDeserializer extends StdDeserializer<LocalDateTime> {
    public DateTimeDeserializer() {
        this(null);
    }

    public DateTimeDeserializer(Class<LocalDateTime> t) {
        super(t);
    }

    @Override
    public LocalDateTime deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        //DateTimeFormatter.ISO_DATE_TIME
        return null;
    }
}
