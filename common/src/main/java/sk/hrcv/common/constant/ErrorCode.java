package sk.hrcv.common.constant;

/**
 * This is enumeration of valid "business" errors.
 * Controllers can return valid response or error response that contains error code describing what unexpected
 * happened during processing request.
 */
public class ErrorCode {

    public static final String GENERAL_FAULT = "00000";
    public static final String PLAYER_ALREADY_EXISTS = "00001";
    public static final String PLAYER_NOT_EXISTS = "00002";
    public static final String TEAM_ALREADY_EXISTS = "00003";
    public static final String SCORE_ALREADY_EXISTS = "00004";

}
