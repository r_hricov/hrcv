package sk.hrcv.common.constant;

public class UrlBase {
    private static final String API = "/api";
    private static final String VERSION_1 = "/v1";
    private static final String VERSION_2 = "/v2";

    public static final String API_V1 = API + VERSION_1;
}