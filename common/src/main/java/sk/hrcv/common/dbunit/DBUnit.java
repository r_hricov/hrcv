package sk.hrcv.common.dbunit;

import org.dbunit.database.DatabaseConfig;
import org.dbunit.database.DatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.dbunit.ext.h2.H2DataTypeFactory;
import org.dbunit.operation.DatabaseOperation;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.DriverManager;

//TODO @SpringBootApplication - prevent running this during tests
public class DBUnit implements ApplicationRunner {

    public static void main(String... args) {
        SpringApplication dbUnitApp = new SpringApplication(DBUnit.class);
        dbUnitApp.setBannerMode(Banner.Mode.OFF);

        dbUnitApp.run(args);
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        //TODO test on non in-memory db
        Connection connection = DriverManager.getConnection("url", "user", "passwd");
        DatabaseConnection databaseConnection = new DatabaseConnection(connection, "schema");
        DatabaseConfig config = databaseConnection.getConfig();
        config.setProperty(DatabaseConfig.PROPERTY_DATATYPE_FACTORY, new H2DataTypeFactory());

        IDataSet dataSet = new FlatXmlDataSetBuilder()
                .setColumnSensing(true)
                .build(new InputStreamReader(new FileInputStream(""), StandardCharsets.UTF_8));

        DatabaseOperation.REFRESH.execute(databaseConnection, dataSet);
    }

    private void createDataSet() throws Exception {
        Connection connection = DriverManager.getConnection("url", "user", "passwd");
        DatabaseConnection databaseConnection = new DatabaseConnection(connection, "schema");
        IDataSet dataSet = databaseConnection.createDataSet();
        FlatXmlDataSet.write(dataSet, new FileOutputStream("filename"));

    }
}