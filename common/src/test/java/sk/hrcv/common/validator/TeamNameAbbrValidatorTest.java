package sk.hrcv.common.validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.DataSourceTransactionManagerAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.testng.Assert;
import org.testng.annotations.Test;
import sk.hrcv.test.config.AbstractTransactionalSpringBootTestWithData;
import sk.hrcv.test.config.TestConfig;
import sk.hrcv.test.config.TestGroup;

import javax.validation.ConstraintViolationException;
import java.util.Arrays;

@SpringBootTest(classes = {TestConfig.class, DataSourceTransactionManagerAutoConfiguration.class})
@Test(groups = TestGroup.UNIT)
public class TeamNameAbbrValidatorTest extends AbstractTransactionalSpringBootTestWithData {

    @Autowired
    private DummyBeanToBeValidated bean;

    public void testValidTeamName() {
        bean.validateTeamName("FOO");
    }

    public void testWithInvalidName() {
        Assert.expectThrows(ConstraintViolationException.class, () ->
                bean.validateTeamName("something not existing in db")
        );
    }

    public void testValidTeamNameMatchCase() {
        Assert.expectThrows(ConstraintViolationException.class, () ->
                bean.validateTeamNameMatchCase("foo")
        );
    }

    public void testValidTeamNameList() {
        bean.validateTeamNames(Arrays.asList("FOO", "BAR"));
    }


}
