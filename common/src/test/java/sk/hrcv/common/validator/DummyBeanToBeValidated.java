package sk.hrcv.common.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import java.util.List;

@Component
@Validated
public class DummyBeanToBeValidated {

    public void validateTeamName(@TeamNameAbbr String teamName) {
        //nothing to do here
    }

    public void validateTeamNameMatchCase(@TeamNameAbbr(ignoreCase = false) String teamName) {
        //nothing to do here
    }

    public void validateTeamNames(@TeamNameAbbr List<String> teamNames) {
        //nothing to do here
    }
}