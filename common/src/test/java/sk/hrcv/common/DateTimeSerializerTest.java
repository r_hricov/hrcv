package sk.hrcv.common;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.testng.Assert;
import org.testng.annotations.Test;
import sk.hrcv.test.config.TestGroup;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Test(groups = TestGroup.UNIT)
public class DateTimeSerializerTest {

    public void testDateTimeSerializer() throws JsonProcessingException {
        String expectedDateTimeString = "2020-05-20T10:00:00";
        LocalDateTime testDateTime = LocalDateTime.parse(expectedDateTimeString, DateTimeFormatter.ISO_DATE_TIME);

        DateTimeTestPOJO testObj = new DateTimeTestPOJO();
        testObj.setDateTime(testDateTime);

        ObjectMapper mapper = new ObjectMapper();
        String result = mapper.writeValueAsString(testObj);

        Assert.assertTrue(result.contains(expectedDateTimeString));
    }
}
