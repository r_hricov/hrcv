package sk.hrcv.war.runner;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import javax.annotation.PreDestroy;

@SpringBootApplication(scanBasePackages = {"sk.hrcv"})
public class HrcvApplication extends SpringBootServletInitializer {

    public static void main(String[] args) {
        SpringApplication.run(HrcvApplication.class, args);
    }

    @PreDestroy
    public void onExit() {

        System.out.println("###STOPping###");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }
        System.out.println("###STOP FROM THE LIFECYCLE###");
    }
}
