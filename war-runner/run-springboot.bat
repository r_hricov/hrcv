@echo off

SETLOCAL
set curdir=%~dp0

pushd %curdir%\..\config\src\main\resources
set APPLICATION_DIR=%cd%
popd

echo %APPLICATION_DIR%

set CONF_DIRECTORY=%APPLICATION_DIR%/conf
echo %CONF_DIRECTORY%

set LOGS_DIRECTORY=%APPLICATION_DIR%/log
echo %LOGS_DIRECTORY%
cd target

set DEBUG_ARGS=
if "%1" == "--debug" set DEBUG_ARGS=-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=1080

"%JAVA_HOME%\bin\java" %DEBUG_ARGS% -jar foosball.war ^
 %APPLICATION_DIR% ^
 --spring.config.location=file:%CONF_DIRECTORY%/application.yml ^
 --logging.config=%CONF_DIRECTORY%/logback.xml

ENDLOCAL
EXIT /B