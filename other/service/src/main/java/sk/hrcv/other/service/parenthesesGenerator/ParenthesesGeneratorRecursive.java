package sk.hrcv.other.service.parenthesesGenerator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * https://leetcode.com/problems/generate-parentheses/
 * Given n pairs of parentheses, write a function to generate all combinations of well-formed parentheses.
 * <p>
 * Example 1:
 * <p>
 * Input: n = 3
 * Output: ["((()))","(()())","(())()","()(())","()()()"]
 */
public class ParenthesesGeneratorRecursive {
    private static final Logger log = LoggerFactory.getLogger(ParenthesesGeneratorRecursive.class);

    public List<String> generate(long n) {
        return generate(n, n);
    }

    private List<String> generate(long leftCount, long rightCount) {
        //TODO
        return null;
    }
}
