package sk.hrcv.other.service.infiniteMonkeyTheorem;

import java.util.List;

public interface InfiniteMonkeyTheoremStrategy {
    /**
     * This method returns minimum number of spaces/commas needed to divide whole string using favorite strings
     *
     * @param wholeString     string to be divided by favoriteStrings
     * @param favoriteStrings list of string to be used to divide wholeString
     * @return minimum number of spaces/commas and list of used strings OR -1 if no result found
     */
    ResultTuple find(final String wholeString, final List<String> favoriteStrings);
}
