package sk.hrcv.other.service.other;

public class BinarySearch {

    /**
     * array must be sorted
     * there should not be duplicities in array
     * returns position of searched key
     */
    public int binarySearchRecursive(int[] array, int low, int upp, int key) {
        if (low >= array.length) {
            //key not found in array
            return -1;
        }

        int half = low + (upp - low) / 2;
        if (array[half] == key) {
            return half;
        }

        if (array[half] >= key) {
            // key is in lower half
            return binarySearchRecursive(array, low, half, key);
        } else {
            //key is in upper half
            return binarySearchRecursive(array, half + 1, upp, key);
        }
    }

    /**
     * array must be sorted
     * there should not be duplicities in array
     * returns position of searched key
     */
    public int binarySearchSequential(int[] array, int low, int upp, int key) {
        int half = (low + upp) / 2;//low + (upp - low) / 2;
        while (low <= upp) {
            if (array[half] == key) {
                return half;
            }

            if (array[half] >= key) {
                // key is in lower half
                upp = half;
            } else {
                //key is in upper half
                low = half + 1;
            }
            half = (low + upp) / 2; //low + (upp - low) / 2;
        }

        //key not found in array
        return -1;
    }
}