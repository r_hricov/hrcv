package sk.hrcv.other.service.infiniteMonkeyTheorem;

import com.google.common.base.Joiner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class InfiniteMonkeyTheoremRecursive implements InfiniteMonkeyTheoremStrategy {

    private static final Logger log = LoggerFactory.getLogger(InfiniteMonkeyTheoremRecursive.class);

    /**
     * Complexity:
     * Time:
     * Space:
     */
    @Override
    public ResultTuple find(final String wholeString, final List<String> favoriteStrings) {
        List<List<String>> results = new ArrayList<>();
        findRecursively(wholeString, favoriteStrings, results);

        List<String> result = results.stream()
                .filter(it -> wholeString.equals(Joiner.on("").join(it)))
                .min(Comparator.comparingInt(List::size))
                .orElse(new ArrayList<>());

        log.debug("Result {}", result);
        return new ResultTuple(result.size() - 1L, result);
    }

    private List<String> findRecursively(String subString, List<String> favoriteStrings, List<List<String>> results) {
        List<String> result = new ArrayList<>();
        for (String favoriteString : favoriteStrings) {
            log.debug("actual subString [{}] | favoriteString [{}] | list {}", subString, favoriteString, favoriteStrings);
            //find prefix
            if (subString.startsWith(favoriteString)) {
                ArrayList<String> newStrings = new ArrayList<>(favoriteStrings);
                newStrings.remove(favoriteString);
                log.debug("removing {} | {}", favoriteString, newStrings);

                result = new ArrayList<>();
                result.add(favoriteString);
                result.addAll(findRecursively(subString.replaceFirst(favoriteString, ""), newStrings, results));
                log.debug("result {}", result);
                results.add(result); //this contains not only final results
            }
        }
        return result;
    }

}
