package sk.hrcv.other.service.infiniteMonkeyTheorem;

import java.util.List;

public class ResultTuple {
    private Long numberOfSpace;
    private List<String> myNumbers;

    public ResultTuple(Long numberOfSpace, List<String> myNumbers) {
        this.numberOfSpace = numberOfSpace;
        this.myNumbers = myNumbers;
    }

    public Long getNumberOfSpace() {
        return numberOfSpace;
    }

    public void setNumberOfSpace(Long numberOfSpace) {
        this.numberOfSpace = numberOfSpace;
    }

    public List<String> getMyNumbers() {
        return myNumbers;
    }

    public void setMyNumbers(List<String> myNumbers) {
        this.myNumbers = myNumbers;
    }
}
