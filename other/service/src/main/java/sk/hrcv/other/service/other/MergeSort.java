package sk.hrcv.other.service.other;

public class MergeSort {

    public int[] mergeSort(int[] array, int low, int upp) {
        int half = low + (upp - low) / 2;

        if (low < upp) {
            int[] arr1 = mergeSort(array, low, half);
            int[] arr2 = mergeSort(array, half + 1, upp);

            //join sorted arrays
            if (arr1[arr1.length - 1] <= arr2[0]) {
                return mergeSortedArrays(arr1, arr2);
            } else {
                return mergeSortedArrays(arr2, arr1);
            }

        } else {
            //this case is evaluated if and only if low == upp
            int[] singleArray = new int[1];
            singleArray[0] = array[low];
            return singleArray;
        }
    }

    private int[] mergeSortedArrays(int[] arr1, int[] arr2) {
        int[] sorted = new int[arr1.length + arr2.length];

        int i = 0;
        int j = 0;

        while (i < arr1.length && j < arr2.length) {
            if (arr1[i] <= arr2[j]) {
                sorted[i + j] = arr1[i++];
            } else {
                sorted[i + j] = arr2[j++];
            }
        }

        if (i == arr1.length) {
            //arr2 should be copied to sorted
            System.arraycopy(arr2, j, sorted, i + j, arr2.length - j);
//            while (j < arr2.length) {
//                sorted[i + j] = arr2[j++];
//            }
        }

        if (j == arr2.length) {
            //arr1 should be copied to sorted
            System.arraycopy(arr1, i, sorted, i + j, arr1.length - i);
//            while (i < arr1.length) {
//                sorted[i + j] = arr1[i++];
//            }
        }

        return sorted;
    }

}