package sk.hrcv.other.service.infiniteMonkeyTheorem;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

public class InfiniteMonkeyTheoremDynamic implements InfiniteMonkeyTheoremStrategy {

    private static final Logger log = LoggerFactory.getLogger(InfiniteMonkeyTheoremDynamic.class);

    @Override
    public ResultTuple find(final String wholeString, final List<String> favoriteStrings) {
        //initialize
        Map<String, List<List<String>>> subResults = new HashMap<>();
        //empty string is substring of wholeString
        String substring = "";
        //create and insert sub-result for empty string
        List<List<String>> outer = new ArrayList<>();
        List<String> inner = new ArrayList<>();
        outer.add(inner);
        subResults.put(substring, outer);

        for (char c : wholeString.toCharArray()) {
            //build wholeString char by char
            substring += c;
            subResults.put(substring, new ArrayList<>());

            for (String favoriteString : favoriteStrings) {
                //check if favourite string is possible to use to divide substring of wholeString
                if (substring.endsWith(favoriteString)) {
                    //find prefix
                    String prefix = substring.substring(0, substring.length() - favoriteString.length());
                    //get sub-results for prefix
                    List<List<String>> stringLists = subResults.get(prefix);

                    //add suffix/favoriteString to all sub-results
                    List<List<String>> newStrings = stringLists.stream().map(it -> {
                        List<String> newList = new ArrayList<>(it);
                        newList.add(favoriteString);
                        return newList;
                    }).collect(Collectors.toList());
                    subResults.get(substring).addAll(newStrings);
                }
            }
            log.debug("Subresults {}", subResults);
        }

        //find list of minimum size as result
        List<String> result = subResults.get(wholeString).stream()
                .min(Comparator.comparingInt(List::size))
                .orElse(new ArrayList<>());

        log.debug("Result {}", result);
        return new ResultTuple(result.size() - 1L, result);
    }
}
