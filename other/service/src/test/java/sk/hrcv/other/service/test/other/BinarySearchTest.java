package sk.hrcv.other.service.test.other;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import sk.hrcv.other.service.other.BinarySearch;
import sk.hrcv.test.config.TestGroup;

@Test(groups = TestGroup.UNIT)
public class BinarySearchTest {

    @Test(dataProvider = "testData")
    public void testBinarySearchRecursive(int key, int expectedPosition) {
        BinarySearch binarySearch = new BinarySearch();

        int[] array = {1, 2, 3, 5, 8, 13, 21, 34, 55, 89};
        int i = binarySearch.binarySearchRecursive(array, 0, array.length - 1, key);
        Assert.assertEquals(i, expectedPosition);
    }

    @Test(dataProvider = "testData")
    public void testBinarySearchSequential(int key, int expectedPosition) {
        BinarySearch binarySearch = new BinarySearch();

        int[] array = {1, 2, 3, 5, 8, 13, 21, 34, 55, 89};
        int i = binarySearch.binarySearchSequential(array, 0, array.length - 1, key);
        Assert.assertEquals(i, expectedPosition);
    }

    @DataProvider
    private Object[][] testData() {
        return new Object[][]{
                {1, 0},
                {2, 1},
                {3, 2},
                {5, 3},
                {8, 4},
                {13, 5},
                {21, 6},
                {34, 7},
                {55, 8},
                {89, 9},
                {100, -1}
        };
    }

}