package sk.hrcv.other.service.test.infiniteMonkeyTheorem;

import com.google.common.base.Stopwatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;
import sk.hrcv.other.service.infiniteMonkeyTheorem.InfiniteMonkeyTheoremDynamic;
import sk.hrcv.other.service.infiniteMonkeyTheorem.InfiniteMonkeyTheoremRecursive;
import sk.hrcv.other.service.infiniteMonkeyTheorem.InfiniteMonkeyTheoremStrategy;
import sk.hrcv.other.service.infiniteMonkeyTheorem.ResultTuple;
import sk.hrcv.test.config.TestGroup;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

@Test(groups = TestGroup.UNIT)
public class InfiniteMonkeyTheoremTest {
    private static final Logger log = LoggerFactory.getLogger(InfiniteMonkeyTheoremTest.class);

    private static final String PI = "3141592653589793238462643383279";
    private static final String[] FAVORITE_NUMBERS = {"3", "314", "49", "9001", "159", "15926535897", "26535897", "14", "9323", "8462643383279", "4", "793"};
    private static final ResultTuple EXPECTED_OUTPUT =
            new ResultTuple(3L, Arrays.asList("314", "15926535897", "9323", "8462643383279"));

    private final ThreadLocal<Stopwatch> stopWatch = new ThreadLocal<>();

    public void testRecursive() {
        ResultTuple resultTuple = find(new InfiniteMonkeyTheoremRecursive());

        Assert.assertEquals(resultTuple.getNumberOfSpace(), EXPECTED_OUTPUT.getNumberOfSpace());
        Assert.assertTrue(EXPECTED_OUTPUT.getMyNumbers().containsAll(resultTuple.getMyNumbers()));
    }

    public void testDynamic() {
        ResultTuple resultTuple = find(new InfiniteMonkeyTheoremDynamic());

        Assert.assertEquals(resultTuple.getNumberOfSpace(), EXPECTED_OUTPUT.getNumberOfSpace());
        Assert.assertTrue(EXPECTED_OUTPUT.getMyNumbers().containsAll(resultTuple.getMyNumbers()));
    }

    private ResultTuple find(InfiniteMonkeyTheoremStrategy infiniteMonkeyTheorem) {
        stopWatch.set(Stopwatch.createStarted());
        ResultTuple resultTuple = infiniteMonkeyTheorem.find(PI, Arrays.asList(FAVORITE_NUMBERS));
        long duration = stopWatch.get().elapsed(TimeUnit.MILLISECONDS);
        stopWatch.set(null);
        log.info("Duration: {}ms", duration);

        return resultTuple;
    }
}