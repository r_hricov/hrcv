package sk.hrcv.other.service.test.other;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;
import sk.hrcv.other.service.other.MergeSort;
import sk.hrcv.test.config.TestGroup;

@Test(groups = TestGroup.UNIT)
public class MergeSortTest {
    private static final Logger log = LoggerFactory.getLogger(MergeSortTest.class);

    public void testMergeSort() {
        MergeSort mergeSort = new MergeSort();
        int[] expectedArray = {1, 2, 3, 5, 8, 13, 21, 34, 34, 34, 55, 89};
        int[] array = {13, 34, 89, 3, 34, 8, 1, 21, 5, 55, 2, 34};

        int[] sortedArray = mergeSort.mergeSort(array, 0, array.length - 1);
        log.debug("{}", sortedArray);
        Assert.assertEquals(sortedArray, expectedArray);
    }

}
