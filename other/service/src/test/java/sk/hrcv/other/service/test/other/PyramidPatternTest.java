package sk.hrcv.other.service.test.other;

import org.testng.annotations.Test;
import sk.hrcv.test.config.TestGroup;

@Test(groups = TestGroup.UNIT)
public class PyramidPatternTest {

    /**
     * 9
     * 8 9 8
     * 7 8 9 8 7
     * 6 7 8 9 8 7 6
     * 5 6 7 8 9 8 7 6 5
     * 4 5 6 7 8 9 8 7 6 5 4
     * 3 4 5 6 7 8 9 8 7 6 5 4 3
     * 2 3 4 5 6 7 8 9 8 7 6 5 4 3 2
     * 1 2 3 4 5 6 7 8 9 8 7 6 5 4 3 2 1
     */
    public void printPyramid() {
        int n = 9;

        for (int i = n; i >= 1; i--) {
            int numberOfSpaces = 2 * i;

            for (int j = 0; j < numberOfSpaces; j++) {
                System.out.print(" ");
            }

            for (int j = i; j < n; j++) {
                System.out.print(j + " ");
            }

            for (int j = n; j >= i; j--) {
                System.out.print(j + " ");
            }


            System.out.println();
        }
    }

    /**
     * 1
     * 1 2 1
     * 1 2 3 2 1
     * 1 2 3 4 3 2 1
     * 1 2 3 4 5 4 3 2 1
     * 1 2 3 4 5 6 5 4 3 2 1
     * 1 2 3 4 5 6 7 6 5 4 3 2 1
     * 1 2 3 4 5 6 7 8 7 6 5 4 3 2 1
     * 1 2 3 4 5 6 7 8 9 8 7 6 5 4 3 2 1
     */
    public void printPyramid2() {
        int n = 9;

        for (int i = 1; i <= n; i++) {
            int numberOfSpaces = 2 * (n - i);

            for (int j = 0; j < numberOfSpaces; j++) {
                System.out.print(" ");
            }

            for (int j = 1; j < i; j++) {
                System.out.print(j + " ");
            }

            for (int j = i; j >= 1; j--) {
                System.out.print(j + " ");
            }

            System.out.println();
        }
    }
}
