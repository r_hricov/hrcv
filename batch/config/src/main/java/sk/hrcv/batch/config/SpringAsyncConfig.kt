package sk.hrcv.batch.config

import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.scheduling.annotation.AsyncConfigurer
import org.springframework.scheduling.annotation.EnableAsync
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler
import sk.hrcv.common.util.logger
import java.lang.reflect.Method
import java.util.concurrent.Executor
import javax.annotation.PostConstruct

@Configuration
@ComponentScan("sk.hrcv")

@EnableAsync
open class SpringAsyncConfig : AsyncConfigurer {
    @PostConstruct
    fun foo() {
        println("Started")
    }

    override fun getAsyncExecutor(): Executor {
        val scheduler = ThreadPoolTaskScheduler()
        scheduler.poolSize = 5

        scheduler.initialize()
        return scheduler
    }

    override fun getAsyncUncaughtExceptionHandler(): AsyncUncaughtExceptionHandler {
        return CustomAsyncExceptionHandler()
    }
}

class CustomAsyncExceptionHandler : AsyncUncaughtExceptionHandler {
    companion object {
        val log by logger()
    }

    override fun handleUncaughtException(throwable: Throwable, method: Method, vararg obj: Any) {
        log.debug("Exception message - " + throwable.message)
        log.debug("Method name - " + method.name)
        for (param in obj) {
            println("Parameter value - $param")
        }

        throw throwable
    }
}
