package sk.hrcv.batch.config

import org.springframework.batch.core.configuration.annotation.DefaultBatchConfigurer
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.context.annotation.Configuration
import org.springframework.scheduling.annotation.EnableScheduling
import javax.sql.DataSource

@Configuration
@EnableBatchProcessing
@EnableScheduling
class SpringBatchConfig(
    @Qualifier("foosballDbDataSource")
    val foosballDbDataSource: DataSource
) : DefaultBatchConfigurer() {

    override fun setDataSource(dataSource: DataSource) {
        super.setDataSource(foosballDbDataSource)
    }
}