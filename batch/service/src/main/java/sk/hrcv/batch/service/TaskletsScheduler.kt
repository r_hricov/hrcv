package sk.hrcv.batch.service

import org.springframework.batch.core.Job
import org.springframework.batch.core.JobParametersBuilder
import org.springframework.batch.core.launch.JobLauncher
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Component
import sk.hrcv.common.util.logger
import java.util.*

@Component
class TaskletsScheduler(
    private val launcher: JobLauncher,
    @Qualifier("taskletsJob") private val taskletJob: Job
) {
    companion object {
        val log by logger()
    }

    //TODO @Scheduled(cron = "0 17 * * SUN") //At 17:00 on Sunday //https://crontab.guru/
    //TODO for testing purpose only
    @Scheduled(fixedDelay = 1000, initialDelay = 1000)
    fun doTaskletJob() {
        try {
            launcher.run(taskletJob, JobParametersBuilder().addDate("date", Date()).toJobParameters())
        } catch (e: Exception) {
            log.debug("Something went wrong with processing batch job {}", taskletJob.name)
            log.error("Exception caught", e)
        }
    }

}