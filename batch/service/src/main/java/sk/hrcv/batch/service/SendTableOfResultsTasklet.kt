package sk.hrcv.batch.service

import org.springframework.batch.core.StepContribution
import org.springframework.batch.core.scope.context.ChunkContext
import org.springframework.batch.core.step.tasklet.Tasklet
import org.springframework.batch.repeat.RepeatStatus
import org.springframework.stereotype.Component

@Component
class SendTableOfResultsTasklet(
    private val sendTableOfResultsService: SendTableOfResultsService
) : Tasklet {

    override fun execute(p0: StepContribution, p1: ChunkContext): RepeatStatus {
        sendTableOfResultsService.sendEmailWithTableOfResults()

        return RepeatStatus.FINISHED
    }
}