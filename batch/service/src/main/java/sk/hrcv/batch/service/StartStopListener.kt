package sk.hrcv.batch.service

import org.springframework.batch.core.JobExecution
import org.springframework.batch.core.JobExecutionListener

class StartStopListener : JobExecutionListener {
    override fun beforeJob(p0: JobExecution) {
        println(p0.jobInstance.jobName + " job is starting in: " + p0.jobParameters.getDate("date"))
    }

    override fun afterJob(p0: JobExecution) {
        println(p0.jobInstance.jobName + " job finished")
    }
}