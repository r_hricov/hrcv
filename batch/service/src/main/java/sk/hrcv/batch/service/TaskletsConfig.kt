package sk.hrcv.batch.service

import org.springframework.batch.core.Job
import org.springframework.batch.core.Step
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration


@Configuration
class TaskletsConfig(
    private val jobs: JobBuilderFactory,
    private val steps: StepBuilderFactory,
    private val sendTableOfResultsTasklet: SendTableOfResultsTasklet
) {

    @Bean
    fun sendTableOfResultsStep(): Step =
        steps["sendTableOfResultsStep"]
            .allowStartIfComplete(true)
            .tasklet(sendTableOfResultsTasklet)
            .build()


    @Bean(name = ["taskletsJob"])
    fun sendTableOfResultsJob(): Job = jobs["sendTableOfResultsJob"]
        .listener(StartStopListener())
        .start(sendTableOfResultsStep())
        .build()

}