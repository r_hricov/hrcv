package sk.hrcv.batch.service

interface EmailSender {
    fun sendMessage(message: String)
}