package sk.hrcv.batch.service

import org.springframework.scheduling.annotation.Async
import java.util.concurrent.Future

interface SendTableOfResultsService {
    @Async
    fun sendEmailWithTableOfResults()

    /**
     * just for purpose of POC
     */
    @Async
    fun asyncMethodWithReturnType(): Future<String>

    /**
     * just for purpose of POC
     */
    @Async
    fun doProcess()
}