package sk.hrcv.batch.service.impl

import org.springframework.scheduling.annotation.Async
import org.springframework.scheduling.annotation.AsyncResult
import org.springframework.stereotype.Service
import sk.hrcv.batch.service.EmailSender
import sk.hrcv.batch.service.SendTableOfResultsService
import sk.hrcv.common.util.logger
import sk.hrcv.wbbe.service.TableService
import java.util.concurrent.Future
import javax.transaction.Transactional


@Service
@Transactional
class SendTableOfResultsServiceImpl(
    private val tableService: TableService,
    private val emailSender: EmailSender
) : SendTableOfResultsService {
    companion object {
        val log by logger()
    }

    @Async
    override fun sendEmailWithTableOfResults() {
        log.debug("It is about to sent table of records by mail")

        tableService.tableRecords.also {
            emailSender.sendMessage(it.toString())
        }
    }

    @Deprecated("just for purpose of POC")
    @Async
    override fun doProcess() {
        try {
            Thread.sleep(5000)
            println("Execute method doProcess asynchronously. " + Thread.currentThread().name)
        } catch (e: InterruptedException) {
            //
        }
    }

    @Deprecated("just for purpose of POC")
    @Async
    override fun asyncMethodWithReturnType(): Future<String> {
        println(
            "Execute method asynchronously - "
                    + Thread.currentThread().name
        )
        try {
            Thread.sleep(15000)
            return AsyncResult("hello world !!!!")
        } catch (e: InterruptedException) {
            //
        }
        return AsyncResult("nope")
    }

}