package sk.hrcv.batch.service.impl

import org.springframework.stereotype.Service
import sk.hrcv.batch.service.EmailSender
import sk.hrcv.common.util.logger

@Service
class DummyEmailSenderServiceImpl : EmailSender {
    companion object {
        val log by logger()
    }

    override fun sendMessage(message: String) {
        log.debug(message)
    }

}