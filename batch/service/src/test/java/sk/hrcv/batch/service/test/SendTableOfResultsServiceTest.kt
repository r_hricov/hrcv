package sk.hrcv.batch.service.test

import org.springframework.beans.factory.annotation.Autowired
import org.testng.annotations.Test
import sk.hrcv.batch.service.SendTableOfResultsService
import sk.hrcv.test.config.AbstractTransactionalSpringBootTestWithData
import sk.hrcv.test.config.TestGroup
import java.util.concurrent.Future

/**
 * This test is part of POC
 */
@Test(groups = [TestGroup.UNIT])
class SendTableOfResultsServiceTest : AbstractTransactionalSpringBootTestWithData() {

    @Autowired
    private lateinit var sendTableOfResultsService: SendTableOfResultsService

    fun `Test Async Annotation For Methods Without Return Type`() {
        println("Invoking an asynchronous method. " + Thread.currentThread().name)
        sendTableOfResultsService.doProcess()
    }

    fun `Test Async Annotation For Methods With Return Type`() {
        println("Invoking an asynchronous method. " + Thread.currentThread().name)

        val future: Future<String> = sendTableOfResultsService.asyncMethodWithReturnType()
        while (true) {
            if (future.isDone) {
                println("Result from asynchronous process - " + future.get())
                break
            }
            println("Continue doing something else. ")
            Thread.sleep(10000)
        }
    }

}

