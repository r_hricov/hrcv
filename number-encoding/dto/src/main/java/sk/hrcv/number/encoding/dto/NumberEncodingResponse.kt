package sk.hrcv.number.encoding.dto

import sk.hrcv.number.encoding.dto.type.EncodedNumber

data class NumberEncodingResponse(
    val encodedNumbers: List<EncodedNumber> = ArrayList(),
)