package sk.hrcv.number.encoding.dto

import javax.validation.constraints.NotEmpty

data class NumberEncodingRequest(
    @field:NotEmpty
    val numbersToEncode: List<String> = ArrayList()
)