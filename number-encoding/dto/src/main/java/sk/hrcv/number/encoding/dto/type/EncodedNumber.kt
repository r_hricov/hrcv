package sk.hrcv.number.encoding.dto.type

data class EncodedNumber(
    val originalNumber: String = "",
    val possibleEncodings: List<String>?
)