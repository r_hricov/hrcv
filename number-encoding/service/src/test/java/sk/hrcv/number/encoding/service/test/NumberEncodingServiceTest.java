package sk.hrcv.number.encoding.service.test;

import mockit.Mock;
import mockit.MockUp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import sk.hrcv.number.encoding.service.NumberEncodingService;
import sk.hrcv.number.encoding.service.impl.DictionaryServiceImpl;
import sk.hrcv.test.config.AbstractSpringBootTest;
import sk.hrcv.test.config.TestGroup;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Test(groups = TestGroup.UNIT)
public class NumberEncodingServiceTest extends AbstractSpringBootTest {

    @Autowired
    private NumberEncodingService numberEncodingService;

    @Test(dataProvider = "testData")
    public void testNumberEncodingService(String input, List<String> expectation, boolean correctFlag) {

        List<String> words = numberEncodingService.translatePhoneNumber(input);

        if (correctFlag) {
            Assert.assertEquals(words.size(), expectation.size());
            Assert.assertTrue(words.containsAll(expectation));
        } else {
            expectation.forEach(it -> Assert.assertFalse(words.contains(it)));
        }
    }

    @DataProvider
    public Object[][] testData() {
        return new Object[][]{
                {"112", Collections.emptyList(), true},
                {"5624-82", Arrays.asList("mir Tor", "Mix Tor"), true},
                {"4824", Arrays.asList("Torf", "fort", "Tor 4"), true},
                {"0721/608-4067", Collections.emptyList(), true},
                {"10/783--5", Arrays.asList("neu o\"d 5", "je bo\"s 5", "je Bo\" da"), true},
                {"1078-913-5", Collections.emptyList(), true},
                {"381482", Collections.singletonList("so 1 Tor"), true},
                {"04824", Arrays.asList("0 Torf", "0 fort", "0 Tor 4"), true},

                {"10/783--5", Collections.singletonList("je bos 5"), false}, //because the formatting of the phone number is incorrect
                {"4824", Collections.singletonList("4 Ort"), false}, //because in place of the first digit the words Torf, fort, Tor 4 could be used
                {"1078-913-5", Collections.singletonList("je Bo\" 9 1 da"), false}, //since there are two subsequent digits in the encoding
                {"04824", Collections.singletonList("0 Tor"), false}, //because the encoding does not cover the whole phone number
                {"5624-82", Collections.singletonList("mir Torf"), false}, //because the encoding is longer than the phone number
        };
    }

    @BeforeTest
    private void mockDictionaryService() {
        new MockUp<DictionaryServiceImpl>() {
            @Mock
            private List<String> getDictionaryInput(Resource resource) {
                return DICTIONARY_INPUT;
            }
        };
    }

    private static final List<String> DICTIONARY_INPUT = Arrays.asList(
            "an",
            "blau",
            "Bo\"",
            "Boot",
            "bo\"s",
            "da",
            "Fee",
            "fern",
            "Fest",
            "fort",
            "je",
            "jemand",
            "mir",
            "Mix",
            "Mixer",
            "Name",
            "neu",
            "o\"d",
            "Ort",
            "so",
            "Tor",
            "Torf",
            "Wasser"
    );
}