package sk.hrcv.number.encoding.service.test;

import mockit.Mock;
import mockit.MockUp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import sk.hrcv.number.encoding.service.DictionaryService;
import sk.hrcv.number.encoding.service.impl.DictionaryServiceImpl;
import sk.hrcv.test.config.AbstractSpringBootTest;

import java.util.Arrays;
import java.util.List;

@Test
public class DictionaryServiceTest extends AbstractSpringBootTest {

    @Autowired
    private DictionaryService dictionaryService;

    @Test(dataProvider = "testData")
    public void testDictionaryConversion(String input, String output) {
        Assert.assertTrue(dictionaryService.getNumberRepresentation(output).contains(input));
    }

    @DataProvider
    private Object[][] testData() {
        return new Object[][]{
                {"an", "51"},
                {"blau", "7857"},
                {"Bo\"", "78"},
                {"Boot", "7884"},
                {"bo\"s", "783"},
                {"da", "35"},
                {"Fee", "400"},
                {"fern", "4021"},
                {"Fest", "4034"},
                {"fort", "4824"},
                {"je", "10"},
                {"jemand", "105513"},
                {"mir", "562"},
                {"Mix", "562"},
                {"Mixer", "56202"},
                {"Name", "1550"},
                {"neu", "107"},
                {"o\"d", "83"},
                {"Ort", "824"},
                {"so", "38"},
                {"Tor", "482"},
                {"Torf", "4824"},
                {"Wasser", "253302"}
        };
    }

    @BeforeTest
    private void mockDictionaryService() {
        new MockUp<DictionaryServiceImpl>() {
            @Mock
            private List<String> getDictionaryInput(Resource resource) {
                return DICTIONARY_INPUT;
            }
        };
    }

    private static final List<String> DICTIONARY_INPUT = Arrays.asList(
            "an",
            "blau",
            "Bo\"",
            "Boot",
            "bo\"s",
            "da",
            "Fee",
            "fern",
            "Fest",
            "fort",
            "je",
            "jemand",
            "mir",
            "Mix",
            "Mixer",
            "Name",
            "neu",
            "o\"d",
            "Ort",
            "so",
            "Tor",
            "Torf",
            "Wasser"
    );
}
