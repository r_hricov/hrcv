package sk.hrcv.number.encoding.service.impl;

import com.google.common.base.Joiner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sk.hrcv.number.encoding.service.DictionaryService;
import sk.hrcv.number.encoding.service.NumberEncodingService;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class NumberEncodingServiceImpl implements NumberEncodingService {

    private static final Logger log = LoggerFactory.getLogger(NumberEncodingServiceImpl.class);

    private static final String DASH = "-";
    private static final String SLASH = "/";

    private final DictionaryService dictionaryService;

    @Autowired
    public NumberEncodingServiceImpl(DictionaryService dictionaryService) {
        this.dictionaryService = dictionaryService;
    }

    @Override
    public List<String> translatePhoneNumber(String phoneNumber) {
        String number = stripPhoneNumber(phoneNumber);
        List<List<String>> relevantPhoneNumberShreds = findRelevantPhoneNumberShreds(number);

        if (relevantPhoneNumberShreds.isEmpty()) {
            return Collections.emptyList();
        } else {
            List<String> result = relevantPhoneNumberShreds.stream()
                    .map(shreds -> shreds.stream()
                            .map(shred -> {
                                //do not translate single digit shred - number [0..9]
                                if (shred.length() != 1) {
                                    return dictionaryService.getNumberRepresentation(shred);
                                } else {
                                    return Collections.singletonList(shred);
                                }
                            })
                            .collect(Collectors.toList()))
                    .map(this::doCombinations)
                    .flatMap(it -> it.stream().map(itt -> Joiner.on(" ").join(itt)))
                    .collect(Collectors.toList());
            log.info("Result {}", result);
            return result;
        }
    }

    /**
     * Replaces all dashes and slashes
     */
    private String stripPhoneNumber(String phoneNumber) {
        return phoneNumber.replace(DASH, "").replace(SLASH, "").trim();
    }

    private List<List<String>> findRelevantPhoneNumberShreds(String phoneNumber) {
        //initialize
        List<List<String>>[] subResultsArray = new ArrayList[phoneNumber.length() + 1];

        //empty string is substring of wholeString
        String substring = "";
        //create and insert sub-result for empty string
        List<List<String>> outer = new ArrayList<>();
        List<String> inner = new ArrayList<>();
        outer.add(inner);
        subResultsArray[substring.length()] = outer;

        for (char c : phoneNumber.toCharArray()) {
            //build wholeString char by char
            substring += c;
            subResultsArray[substring.length()] = new ArrayList<>();

            List<String> favoriteStrings = findFavoriteStrings(substring);
            favoriteStrings.add(substring.substring(substring.length() - 1));

            for (String favoriteString : favoriteStrings) {
                //check if favourite string is possible to use to divide substring of wholeString
                if (substring.endsWith(favoriteString)) {
                    //find prefix
                    String prefix = substring.substring(0, substring.length() - favoriteString.length());
                    //get sub-results for prefix
                    List<List<String>> stringLists = subResultsArray[prefix.length()];

                    //if exists suffix in favoriteStrings then remove combination that ends with only/one number
                    subResultsArray[prefix.length() + 1] = subResultsArray[prefix.length() + 1].stream()
                            .filter(it -> it.get(it.size() - 1).length() != 1)
                            .collect(Collectors.toList());

                    //add suffix/favoriteString to all sub-results
                    List<List<String>> newStrings = stringLists.stream()
                            .filter(it -> it.isEmpty() || it.get(it.size() - 1).length() != 1 || it.get(it.size() - 1).length() == 1 && favoriteString.length() != 1)
                            .map(it -> {
                                List<String> newList = new ArrayList<>(it);
                                newList.add(favoriteString);
                                return newList;
                            }).collect(Collectors.toList());
                    subResultsArray[substring.length()].addAll(newStrings);
                }
            }
            log.debug("Subresults {}", Arrays.asList(subResultsArray));
        }

        //result
        List<List<String>> result = subResultsArray[phoneNumber.length()];
        log.debug("Result {}", result);
        return result;
    }

    /**
     * Try to find all substrings of string (on input) from dictionary
     */
    private List<String> findFavoriteStrings(String string) {
        List<String> favoriteStrings = new ArrayList<>();

        for (int i = 0; i < string.length(); i++) {
            String testedSubstring = string.substring(i);

            List<String> translatedSubstring = dictionaryService.getNumberRepresentation(testedSubstring);
            if (translatedSubstring != null && !translatedSubstring.isEmpty()) {
                favoriteStrings.add(testedSubstring);
            }
        }
        return favoriteStrings;
    }

    /**
     * downloaded implementation
     */
    private List<List<String>> doCombinations(List<List<String>> arrays) {
        List<List<String>> combinations = Collections.singletonList(Collections.emptyList());
        for (List<String> list : arrays) {
            List<List<String>> extraColumnCombinations = new ArrayList<>();
            for (List<String> combination : combinations) {
                for (String element : list) {
                    List<String> newCombination = new ArrayList<>(combination);
                    newCombination.add(element);
                    extraColumnCombinations.add(newCombination);
                }
            }
            combinations = extraColumnCombinations;
        }
        return combinations;
    }
}