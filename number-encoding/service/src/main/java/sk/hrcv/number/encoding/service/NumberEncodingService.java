package sk.hrcv.number.encoding.service;

import java.util.List;

public interface NumberEncodingService {

    List<String> translatePhoneNumber(String phoneNumber);

}
