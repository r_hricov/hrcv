package sk.hrcv.number.encoding.service;


import javax.annotation.Nullable;
import java.util.List;

public interface DictionaryService {

    @Nullable
    List<String> getNumberRepresentation(String input);

}
