package sk.hrcv.number.encoding.service.impl;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import sk.hrcv.number.encoding.service.DictionaryService;

import javax.annotation.Nullable;
import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
public class DictionaryServiceImpl implements DictionaryService {

    private static final String UMLAUT_CHARACTER = "\"";

    @Value("classpath:dictionary.txt")
    private Resource dictionaryResource;

    @Override
    @Nullable
    public List<String> getNumberRepresentation(String input) {
        return getDICTIONARY().get(input);
    }

    @Cacheable(cacheManager = "numberEncodingCacheManager", cacheNames = "dictionary")
    public Map<String, List<String>> getDICTIONARY() {
        return DICTIONARY;
    }

    @PostConstruct
    private void initLettersMapping() {
        LETTERS_MAPPING.put("e", "0");

        LETTERS_MAPPING.put("j", "1");
        LETTERS_MAPPING.put("n", "1");
        LETTERS_MAPPING.put("q", "1");

        LETTERS_MAPPING.put("r", "2");
        LETTERS_MAPPING.put("w", "2");
        LETTERS_MAPPING.put("x", "2");

        LETTERS_MAPPING.put("d", "3");
        LETTERS_MAPPING.put("s", "3");
        LETTERS_MAPPING.put("y", "3");

        LETTERS_MAPPING.put("f", "4");
        LETTERS_MAPPING.put("t", "4");

        LETTERS_MAPPING.put("a", "5");
        LETTERS_MAPPING.put("m", "5");

        LETTERS_MAPPING.put("c", "6");
        LETTERS_MAPPING.put("i", "6");
        LETTERS_MAPPING.put("v", "6");

        LETTERS_MAPPING.put("b", "7");
        LETTERS_MAPPING.put("k", "7");
        LETTERS_MAPPING.put("u", "7");

        LETTERS_MAPPING.put("l", "8");
        LETTERS_MAPPING.put("o", "8");
        LETTERS_MAPPING.put("p", "8");

        LETTERS_MAPPING.put("g", "9");
        LETTERS_MAPPING.put("h", "9");
        LETTERS_MAPPING.put("z", "9");

        DICTIONARY = getDictionaryInput(dictionaryResource).stream()
                .collect(Collectors.groupingBy(o -> encodeWord(stripWord(o)), Collectors.mapping(o -> o, Collectors.toList())));
    }

    /**
     * This translates/encodes word from input into string of numbers
     */
    private String encodeWord(String s) {
        List<String> numbers = Stream.of(s.split("(?!^)")).map(LETTERS_MAPPING::get).collect(Collectors.toList());
        return String.join("", numbers);
    }

    /**
     * This removes "umlaut" (") from word and makes sting lowercase
     */
    private String stripWord(String s) {
        return s.toLowerCase().replace(UMLAUT_CHARACTER, "");
    }

    private static final Map<String, String> LETTERS_MAPPING = new HashMap<>();

    private static Map<String, List<String>> DICTIONARY = new HashMap<>();

    /**
     * This reads dictionary from file line by line
     */
    private List<String> getDictionaryInput(Resource resource) {
        List<String> strings = new ArrayList<>();

        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new InputStreamReader(resource.getInputStream(), StandardCharsets.UTF_8));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                strings.add(line);
            }
        } catch (final IOException e) {
            throw new IllegalStateException("No resource found: " + ((ClassPathResource) resource).getPath());
        } finally {
            if (bufferedReader != null) {
                try {
                    bufferedReader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        return strings;
    }
}