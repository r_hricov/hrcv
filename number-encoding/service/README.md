# Number encoding service

##### Services:

- [DictionaryService](./src/main/java/sk/hrcv/number/encoding/service/DictionaryService.java) - Processes words from a
  dictionary and translates it into numbers
- [NumberEncodingService](./src/main/java/sk/hrcv/number/encoding/service/NumberEncodingService.java) - Encodes phone
  numbers using words from dictionary 



