package sk.hrcv.number.encoding.controller.constant;

import sk.hrcv.common.constant.UrlBase;

public class Url {
    private static final String NUMBER_ENCODING = "/numberEncoding";

    public static final String NUMBER_ENCODING_URL = UrlBase.API_V1 + NUMBER_ENCODING;
}
