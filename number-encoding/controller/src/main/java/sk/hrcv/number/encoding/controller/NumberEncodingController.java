package sk.hrcv.number.encoding.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import sk.hrcv.number.encoding.controller.constant.Url;
import sk.hrcv.number.encoding.dto.NumberEncodingRequest;
import sk.hrcv.number.encoding.dto.NumberEncodingResponse;
import sk.hrcv.number.encoding.dto.type.EncodedNumber;
import sk.hrcv.number.encoding.service.NumberEncodingService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping(path = Url.NUMBER_ENCODING_URL, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
public class NumberEncodingController {

    private final NumberEncodingService numberEncodingService;

    @Autowired
    public NumberEncodingController(NumberEncodingService numberEncodingService) {
        this.numberEncodingService = numberEncodingService;
    }

    /**
     * This returns results only for numbers for that at least one valid encoding was found
     */
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    @Valid
    public NumberEncodingResponse encodeNumbers(@Valid @RequestBody NumberEncodingRequest request) {
        ArrayList<EncodedNumber> encodedNumbers = new ArrayList<>();

        for (String number : request.getNumbersToEncode()) {
            List<String> encodings = numberEncodingService.translatePhoneNumber(number);
            if (!encodings.isEmpty()) {
                EncodedNumber encodedNumber = new EncodedNumber(number, encodings);
                encodedNumbers.add(encodedNumber);
            }
        }

        return new NumberEncodingResponse(encodedNumbers);
    }
}