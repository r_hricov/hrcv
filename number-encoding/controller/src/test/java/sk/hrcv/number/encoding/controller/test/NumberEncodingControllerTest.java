package sk.hrcv.number.encoding.controller.test;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import sk.hrcv.number.encoding.controller.constant.Url;
import sk.hrcv.number.encoding.dto.NumberEncodingRequest;
import sk.hrcv.test.config.AbstractTransactionalSpringBootTest;
import sk.hrcv.test.config.TestGroup;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

@Test(groups = {TestGroup.UNIT})
@Sql({"classpath:authTEST.sql"})
public class NumberEncodingControllerTest extends AbstractTransactionalSpringBootTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Value("classpath:input.txt")
    private Resource req;

    @BeforeClass
    private void init() {
        super.initTest(webApplicationContext);
    }

    public void testGetEncodings() throws Exception {
        get(Url.NUMBER_ENCODING_URL, getTestRequest(req))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andReturn();
    }

    private String getTestRequest(Resource resource) {
        List<String> strings = new ArrayList<>();

        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(resource.getInputStream(), StandardCharsets.UTF_8));
            String line;
            while ((line = bufferedReader.readLine()) != null) {
                strings.add(line);
            }
        } catch (final IOException e) {
            throw new IllegalStateException("No resource found: " + ((ClassPathResource) resource).getPath());
        }

        NumberEncodingRequest request = new NumberEncodingRequest(strings);
        Gson gson = new Gson();
        return gson.toJson(request);
    }
}