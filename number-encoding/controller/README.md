# Number encoding controller

##### GET encoded numbers

Service is available by:\
````{GET /api/v1/numberEncoding, consumes [application/json], produces [application/json]}````

Sample

- [call](./src/test/resource/requests.http)
- [request](./src/test/resource/sample_input.json)
- [response](./src/test/resource/sample_output.json)



