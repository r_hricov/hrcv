# Number encoding

This is my solution of number encoding problem.

Assigment of problem is available [here](./assignment_numberencoding.txt).

It consists of two modules:

- [service](./service/README.md)
- [controller](./controller/README.md) (contains sample request)