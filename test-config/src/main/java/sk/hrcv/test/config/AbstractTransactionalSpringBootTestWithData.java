package sk.hrcv.test.config;

import org.springframework.test.context.jdbc.Sql;

//TODO refactor to not create db and load data before each test, or use remote testing database instead
//this creates db and loads data into db before EACH test
@Sql({"classpath:authTEST.sql", "classpath:/schemaTEST.sql", "classpath:/dataTEST.sql"})
public abstract class AbstractTransactionalSpringBootTestWithData extends AbstractTransactionalSpringBootTest {

}