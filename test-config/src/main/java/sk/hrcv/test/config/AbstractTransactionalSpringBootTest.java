package sk.hrcv.test.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.Listeners;
import sk.hrcv.config.constant.HeaderParameters;
import sk.hrcv.config.security.SecurityProperties;

import java.util.Base64;

@SpringBootTest(classes = TestConfig.class)
@AutoConfigureMockMvc
@Listeners({AfterTestFlushListener.class})
public class AbstractTransactionalSpringBootTest extends AbstractTransactionalTestNGSpringContextTests {

    public static final String CALL_ID_HEADER = HeaderParameters.CALL_ID_HEADER;
    public static final String TEST_CALL_ID_HEADER_VALUE = "HRCV123456789";

    //@AutoConfigureMockMvc doesn't register all the converters, but @WebMvcTest does.
    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private SecurityProperties securityProperties;

    public void initTest(WebApplicationContext webApplicationContext) {
        // nothing to do here
    }

    public ResultActions post(String urlTemplate, String request) throws Exception {
        return mockMvc.perform(MockMvcRequestBuilders.post(urlTemplate)
                .header(CALL_ID_HEADER, TEST_CALL_ID_HEADER_VALUE)
                .header(HttpHeaders.AUTHORIZATION, getAuthToken())
                .contentType(MediaType.APPLICATION_JSON)
                .content(request));
    }

    public ResultActions get(String urlTemplate) throws Exception {
        return mockMvc.perform(MockMvcRequestBuilders.get(urlTemplate)
                .header(CALL_ID_HEADER, TEST_CALL_ID_HEADER_VALUE)
                .header(HttpHeaders.AUTHORIZATION, getAuthToken())
                .contentType(MediaType.APPLICATION_JSON));
    }

    public ResultActions get(String urlTemplate, String request) throws Exception {
        return mockMvc.perform(MockMvcRequestBuilders.get(urlTemplate)
                .header(CALL_ID_HEADER, TEST_CALL_ID_HEADER_VALUE)
                .header(HttpHeaders.AUTHORIZATION, getAuthToken())
                .contentType(MediaType.APPLICATION_JSON)
                .content(request));
    }

    public ResultActions put(String urlTemplate, String request) throws Exception {
        return mockMvc.perform(MockMvcRequestBuilders.put(urlTemplate)
                .header(CALL_ID_HEADER, TEST_CALL_ID_HEADER_VALUE)
                .header(HttpHeaders.AUTHORIZATION, getAuthToken())
                .contentType(MediaType.APPLICATION_JSON)
                .content(request));
    }

    public ResultActions delete(String urlTemplate) throws Exception {
        return mockMvc.perform(MockMvcRequestBuilders.delete(urlTemplate)
                .header(CALL_ID_HEADER, TEST_CALL_ID_HEADER_VALUE)
                .header(HttpHeaders.AUTHORIZATION, getAuthToken())
                .contentType(MediaType.APPLICATION_JSON));
    }

    /**
     * returns security tokens for testing purpose
     */
    private String getAuthToken() {
        switch (securityProperties.getSecurityType()) {
            case JWT:
                return "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiIiLCJpYXQiOjE2MTE2MDE5NzIsImV4cCI6MjUyNjc1MDc3MiwiYXVkIjoiaHJjdiIsInN1YiI6ImFkbWluX2p3dCJ9.6kJJJ4QD1CfcD9LNI_3Fac15x4T1n5cx56iVOYFPnPo";
            case BASIC:
                return "Basic " + new String(Base64.getEncoder().encode(("ADMIN:ADMIN").getBytes()));
            default:
                throw new IllegalStateException(String.format("Not supported security type %s", securityProperties.getSecurityType()));
        }
    }
}