package sk.hrcv.test.config;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;

@SpringBootTest(classes = TestConfig.class)
@TestPropertySource(properties = {"spring.config.location = classpath:/conf/applicationTEST.yml"})
public abstract class AbstractSpringBootTest extends AbstractTestNGSpringContextTests {
}
