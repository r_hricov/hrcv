package sk.hrcv.test.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.testng.ITestResult;
import org.testng.Reporter;
import org.testng.TestListenerAdapter;

import javax.persistence.EntityManager;
import java.util.Map;

/**
 * When you create entity and you do not set its attributes you can still call save() on your repository but it does not throw any exception
 * although some of fields are required. Entity manager does not flush automaticaly data into DB after test so tested entity could be invalid and test still passes.
 * This listener class flushes after test, so entities are propagated to and data are validated in DB.
 */
public class AfterTestFlushListener extends TestListenerAdapter {

    private static final Logger log = LoggerFactory.getLogger(AfterTestFlushListener.class);

    @Override
    public void onTestSuccess(ITestResult iTestResult) {
        ApplicationContext applicationContext = TestApplicationContextHolder.getApplicationContext();
        if (applicationContext == null) {
            return;
        }
        Map<String, EntityManager> entityManagers = applicationContext.getBeansOfType(EntityManager.class);

        for (EntityManager em : entityManagers.values()) {
            try {
                if (em.isJoinedToTransaction()) {
                    em.flush();
                    log.trace("Flushing changes into DB");
                }
            } catch (Exception e) {
                iTestResult.setStatus(ITestResult.FAILURE);
                iTestResult.setThrowable(e);

                Reporter.setCurrentTestResult(iTestResult);
            }
        }
    }
}