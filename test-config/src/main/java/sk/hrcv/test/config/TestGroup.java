package sk.hrcv.test.config;

public final class TestGroup {
    public static final String UNIT = "unit";
    public static final String SMOKE = "smoke";
    public static final String INTEGRATION = "integration";
    public static final String BROKEN = "broken";
    public static final String DO_NOT_RUN = "doNotRun";

    private TestGroup() {
        throw new IllegalStateException("Class is not designed to be instantiate.");
    }
}
