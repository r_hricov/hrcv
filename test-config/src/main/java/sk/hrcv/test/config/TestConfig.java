package sk.hrcv.test.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import sk.hrcv.config.ProjectSpringConfig;

@Configuration
@Import(ProjectSpringConfig.class)
@EnableWebMvc
public class TestConfig {
}
