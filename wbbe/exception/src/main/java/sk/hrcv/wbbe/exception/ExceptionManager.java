package sk.hrcv.wbbe.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import sk.hrcv.config.exception.CommonBusinessException;
import sk.hrcv.config.exception.ErrorResponse;
import sk.hrcv.config.exception.ResourceNotFoundException;

@RestControllerAdvice
public class ExceptionManager {

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ErrorResponse resolveGeneralException(Exception e) {
        return createErrorResponse(e);
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public ErrorResponse resolveResourceNotFoundException(ResourceNotFoundException e) {
        return createErrorResponse(e);
    }

    @ExceptionHandler(CommonBusinessException.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    public ErrorResponse resolveCommonBusinessException(CommonBusinessException e) {
        return createErrorResponse(e);
    }

    private ErrorResponse createErrorResponse(Exception e) {
        ErrorResponse errorResponse = new ErrorResponse();

        errorResponse.setErrorCode(e.getMessage());
        errorResponse.setErrorMessage("Something went wrong: " + e.getMessage());

        return errorResponse;
    }
}
