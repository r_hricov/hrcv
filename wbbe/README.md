# Foosball BE

Simple backend REST application to record results of foosball matches. The application is based on Spring Boot with
in-memory embedded H2 database and basic (or jwt) authentication.

Project assignment: [link](./Java_ukazkovy_projekt.pdf)

- Design of data model is provided [here](../war-runner/src/main/resources/schema-h2.sql) and initial
  data [here](../war-runner/src/main/resources/data-h2.sql)
- Json objects are described [~~here~~](./dto/README.md) [deprecated]
- Sample requests are [here](./controller/README.md)

Other features

- [Import Game Tool](./service/src/main/java/sk/hrcv/wbbe/service/importtool)
- [Game Simulator](./service/src/main/java/sk/hrcv/wbbe/service/simulator)
- [Calendar service for searching free slots for football matches](./service/src/main/java/sk/hrcv/wbbe/service/impl/CalendarServiceImpl.java)