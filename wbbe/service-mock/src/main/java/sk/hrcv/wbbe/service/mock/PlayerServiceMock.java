package sk.hrcv.wbbe.service.mock;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import sk.hrcv.wbbe.dto.CreatePlayerRequest;
import sk.hrcv.wbbe.dto.type.Player;
import sk.hrcv.wbbe.repository.entity.PlayerEntity;
import sk.hrcv.wbbe.repository.entity.TechnicalColumns;
import sk.hrcv.wbbe.service.PlayerService;
import sk.hrcv.wbbe.service.mapper.MappersKt;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

@Primary
@Service
public class PlayerServiceMock implements PlayerService {

    @Override
    public Player createNewPlayer(CreatePlayerRequest request) {
        return getMockedPlayer();
    }

    @Override
    public List<Player> getAllPlayersInfo() {
        return Collections.singletonList(getMockedPlayer());
    }

    @Override
    public Player getPlayerInfo(Long playerId) {
        return getMockedPlayer();
    }

    @Override
    public void deletePlayer(Long playerId) {

    }

    @Override
    public void updatePlayer(Long playerId, String newPlayerNickname) {

    }

    @Override
    public PlayerEntity getPlayer(Long playerId) {
        TechnicalColumns technicalColumns = new TechnicalColumns(LocalDateTime.now().minusMinutes(5));

        return new PlayerEntity(technicalColumns, 0L, "mockedPlayer");
    }

    private Player getMockedPlayer() {


        return MappersKt.toPlayerDto(getPlayer(0L));
    }
}