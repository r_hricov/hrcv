package sk.hrcv.wbbe.service.mapper

import sk.hrcv.wbbe.dto.type.Game
import sk.hrcv.wbbe.dto.type.Player
import sk.hrcv.wbbe.dto.type.Score
import sk.hrcv.wbbe.dto.type.Team
import sk.hrcv.wbbe.repository.entity.GameEntity
import sk.hrcv.wbbe.repository.entity.PlayerEntity
import sk.hrcv.wbbe.repository.entity.ScoreEntity
import sk.hrcv.wbbe.repository.entity.TeamEntity

fun TeamEntity.toTeamDto() = Team(
    teamId = teamId,
    teamName = teamName,
    teamNameAbbr = teamNameAbbr,
    playerOne = playerOne.toPlayerDto(),
    playerTwo = playerTwo?.toPlayerDto()
)

fun List<TeamEntity>.toTeamDtoList() = map { it.toTeamDto() }

fun PlayerEntity.toPlayerDto() = Player(
    playerId = playerId,
    nickname = nickname
)

fun List<PlayerEntity>.toPlayerDtoList() = map { it.toPlayerDto() }

fun GameEntity.toGameDto() = Game(
    gameId = gameId,
    teamA = teamA.toTeamDto(),
    teamB = teamB.toTeamDto(),
    date = date,
    season = season
)

fun List<GameEntity>.toGameDtoList() = map { it.toGameDto() }

fun ScoreEntity.toScoreDto() = Score(
    gameId = game.gameId,
    homeScore = homeScore,
    visitorScore = visitorScore
)
