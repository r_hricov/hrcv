package sk.hrcv.wbbe.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import sk.hrcv.common.constant.ErrorCode;
import sk.hrcv.common.util.ConditionChecker;
import sk.hrcv.config.exception.ResourceNotFoundException;
import sk.hrcv.wbbe.dto.CreateScoreRequest;
import sk.hrcv.wbbe.dto.type.Score;
import sk.hrcv.wbbe.repository.GameRepository;
import sk.hrcv.wbbe.repository.ScoreRepository;
import sk.hrcv.wbbe.repository.entity.ScoreEntity;
import sk.hrcv.wbbe.service.ScoreService;
import sk.hrcv.wbbe.service.mapper.MappersKt;

@Service
@Validated
public class ScoreServiceImpl implements ScoreService {

    private final ScoreRepository scoreRepository;
    private final GameRepository gameRepository;

    @Autowired
    public ScoreServiceImpl(ScoreRepository scoreRepository, GameRepository gameRepository) {
        this.scoreRepository = scoreRepository;
        this.gameRepository = gameRepository;
    }

    @Override
    public Score createScore(CreateScoreRequest request) {
        if (gameRepository.existsByGameId(request.getGameId())) {
            ConditionChecker.checkCondition(scoreRepository.existsByGame_GameId(request.getGameId()), ErrorCode.SCORE_ALREADY_EXISTS);

            ScoreEntity score = new ScoreEntity();
            score.setGame(gameRepository.findByGameId(request.getGameId()));
            score.setHomeScore(request.getScore().getHomeScore());
            score.setVisitorScore(request.getScore().getVisitorScore());

            scoreRepository.saveAndFlush(score);
            return MappersKt.toScoreDto(score);
        } else {
            throw new ResourceNotFoundException(String.format("Game with id = %s does not exist", request.getGameId()));
        }
    }
}