package sk.hrcv.wbbe.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import sk.hrcv.wbbe.dto.ImportGameRequest;
import sk.hrcv.wbbe.dto.type.GameToImport;
import sk.hrcv.wbbe.service.GameService;
import sk.hrcv.wbbe.service.ImportGamesService;
import sk.hrcv.wbbe.service.importtool.ExpressionCreator;

import java.util.ArrayList;
import java.util.List;

@Service
@Validated
public class ImportGamesServiceImpl implements ImportGamesService {
    private static final Logger log = LoggerFactory.getLogger(GameServiceImpl.class);

    private final ExpressionCreator expressionCreator;
    private final GameService gameService;

    public ImportGamesServiceImpl(ExpressionCreator expressionCreator, GameService gameService) {
        this.expressionCreator = expressionCreator;
        this.gameService = gameService;
    }

    @Override
    public List<String> importGames(ImportGameRequest request) {
        List<String> gamesNotImported = new ArrayList<>();

        for (String gameStringToImport : request.getGamesToImport()) {
            try {
                GameToImport gameToImport = expressionCreator.createGameFromString(gameStringToImport).process();
                gameService.createNewImportedGame(gameToImport);
                log.debug(String.format("Game string successfully imported: %s", gameStringToImport));
            } catch (Exception e) {
                log.debug(String.format("Not imported: %s", gameStringToImport));
                log.debug(e.getMessage());
                gamesNotImported.add(gameStringToImport);
            }
        }
        return gamesNotImported;
    }
}
