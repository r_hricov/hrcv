package sk.hrcv.wbbe.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import sk.hrcv.wbbe.dto.type.TableRecord;
import sk.hrcv.wbbe.repository.GameRepository;
import sk.hrcv.wbbe.repository.ScoreRepository;
import sk.hrcv.wbbe.repository.entity.GameEntity;
import sk.hrcv.wbbe.repository.entity.ScoreEntity;
import sk.hrcv.wbbe.repository.entity.TeamEntity;
import sk.hrcv.wbbe.service.TableService;
import sk.hrcv.wbbe.service.mapper.MappersKt;
import sk.hrcv.wbbe.service.model.TeamStatus;
import sk.hrcv.wbbe.service.properties.TableServiceProperties;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service
@Validated
public class TableServiceImpl implements TableService {

    private final GameRepository gameRepository;
    private final ScoreRepository scoreRepository;
    private final TableServiceProperties properties;

    @Autowired
    public TableServiceImpl(GameRepository gameRepository, ScoreRepository scoreRepository, TableServiceProperties properties) {
        this.gameRepository = gameRepository;
        this.scoreRepository = scoreRepository;
        this.properties = properties;
    }

    @Override
    @Cacheable(cacheManager = "tableCacheManager", cacheNames = "tableRecord")
    public List<TableRecord> getTableRecords() {
        List<TableRecord> tableRecords = new ArrayList<>();
        List<GameEntity> gamesPlayedInActualSeason = gameRepository.findAllByDateNotNullAndSeason(properties.getActualSeason());

        Map<TeamEntity, List<GameEntity>> map1 = gamesPlayedInActualSeason.stream().collect(Collectors.groupingBy(GameEntity::getTeamA));
        Map<TeamEntity, List<GameEntity>> map2 = gamesPlayedInActualSeason.stream().collect(Collectors.groupingBy(GameEntity::getTeamB));
        Map<TeamEntity, List<GameEntity>> teams = Stream.of(map1, map2)
                .flatMap(map -> map.entrySet().stream())
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue,
                        (v1, v2) -> Stream.of(v1, v2)
                                .flatMap(Collection::stream)
                                .collect(Collectors.toList())
                ));


        List<Long> gamesIds = gamesPlayedInActualSeason.stream().map(GameEntity::getGameId).collect(Collectors.toList());
        List<ScoreEntity> scores = scoreRepository.findByGame_GameIdIn(gamesIds);
        Map<Long, ScoreEntity> gameScoresMap = scores.stream().collect(Collectors.toMap(score -> score.getGame().getGameId(), score -> score));

        for (Map.Entry<TeamEntity, List<GameEntity>> team : teams.entrySet()) {
            TeamStatus teamStatus = countGamesByResult(gameScoresMap, team);

            TableRecord tableRecord = new TableRecord();
            tableRecord.setTeam(MappersKt.toTeamDto(team.getKey()));
            tableRecord.setPlayedGamesCount(team.getValue().size());
            tableRecord.setWonGamesCount(teamStatus.getWonGamesCount());
            tableRecord.setDrawnGamesCount(teamStatus.getDrawnGamesCount());
            tableRecord.setLostGamesCount(teamStatus.getLostGamesCount());
            tableRecord.setPoints(teamStatus.getWonGamesCount() * properties.getWonGamesMultiplier() +
                    teamStatus.getDrawnGamesCount() * properties.getDrawnGamesMultiplier());

            tableRecords.add(tableRecord);
        }

        return tableRecords.stream()
                .sorted(Comparator.comparingLong(TableRecord::getPoints).reversed())
                .collect(Collectors.toList());
    }

    private TeamStatus countGamesByResult(Map<Long, ScoreEntity> gameScoresMap, Map.Entry<TeamEntity, List<GameEntity>> team) {
        int drawnCount = 0;
        int wonCount = 0;
        int lostCount = 0;

        for (GameEntity game : team.getValue()) {
            ScoreEntity score = gameScoresMap.get(game.getGameId());

            if (Objects.equals(score.getHomeScore(), score.getVisitorScore())) {
                drawnCount++;
                continue;
            }

            boolean isHomeTeamWinner = score.getHomeScore() > score.getVisitorScore();
            if ((Objects.equals(game.getTeamA().getTeamId(), team.getKey().getTeamId()) && isHomeTeamWinner) ||
                    (Objects.equals(game.getTeamB().getTeamId(), team.getKey().getTeamId()) && !isHomeTeamWinner)) {
                wonCount++;
            } else {
                lostCount++;
            }
        }
        return new TeamStatus((long) drawnCount, (long) wonCount, (long) lostCount);
    }
}