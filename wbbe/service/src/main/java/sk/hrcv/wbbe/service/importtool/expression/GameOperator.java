package sk.hrcv.wbbe.service.importtool.expression;

import sk.hrcv.wbbe.dto.type.GameToImport;
import sk.hrcv.wbbe.dto.type.GameToImportKt;
import sk.hrcv.wbbe.dto.type.Score;
import sk.hrcv.wbbe.service.importtool.expression.elements.GameOperation;

import java.time.LocalDateTime;

public class GameOperator extends BinaryOperator {

    public GameOperator(Expression firstOperand) {
        super(firstOperand, new GameOperation());
    }

    public GameOperator(Expression firstOperand, Expression secondOperand) {
        super(firstOperand, secondOperand);
        super.setExpressionElement(new GameOperation());
    }

    public TeamOperator getTeams() {
        return (TeamOperator) super.getFirst();
    }

    public ScoreOperator getScore() {
        return (ScoreOperator) super.getSecond();
    }

    @Override
    public GameToImport process() {
        TeamOperand teamAOperand = this.getTeams().getTeamA();
        TeamOperand teamBOperand = this.getTeams().getTeamB();

        ScoreOperand scoreA = this.getScore().getScoreOfTeamA();
        ScoreOperand scoreB = this.getScore().getScoreOfTeamB();

        Score score = new Score();
        score.setHomeScore(scoreA.getValue());
        score.setVisitorScore(scoreB.getValue());

        return GameToImportKt.builder()
                .withTeamA(teamAOperand.getTeamAbbr())
                .withTeamB(teamBOperand.getTeamAbbr())
                .withScore(score)
                .withDate(LocalDateTime.now())
                .build();
    }
}
