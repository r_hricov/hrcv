package sk.hrcv.wbbe.service.importtool;

import sk.hrcv.wbbe.service.importtool.expression.*;

import java.util.EmptyStackException;
import java.util.Stack;

public class GameExpressionBuilder {
    private final Stack<Expression> stack = new Stack<>();

    /**
     * It takes scoreOperator from stack if exists and adds second score value...
     * ...Then it finishes gameOperator and pushes it into stack
     * If there is only gameOperator on stack it pushes new score value into stack
     */
    public void withNumericOperand(Long number) {
        if (stack.peek() instanceof ScoreOperator) {
            ScoreOperator scoreOperator = (ScoreOperator) stack.pop();
            scoreOperator.setSecond(new ScoreOperand(number));

            GameOperator game = (GameOperator) stack.pop();
            game.setSecond(scoreOperator);
            stack.push(game);
        } else {
            stack.push(new ScoreOperand(number));
        }
    }

    /**
     * It takes teamOperator from stack if exists and adds second team
     * If stack is empty it pushes new team into stack
     */
    public void withTeamOperand(String team) {
        try {
            TeamOperator teamOperator = (TeamOperator) stack.pop();
            teamOperator.setSecond(new TeamOperand(team));

            stack.push(teamOperator);
        } catch (EmptyStackException e) {
            stack.push(new TeamOperand(team));
        }
    }

    /**
     * Gets first team from stack and creates teamOperator
     * Team operator is 'vs.'
     */
    public void withTeamOperator() {
        Expression first = stack.pop();
        Expression teamOperator = new TeamOperator(first);
        stack.push(teamOperator);
    }

    /**
     * Gets first value of score from stack and creates scoreOperator
     * Score operator is ':'
     */
    public void withScoreOperator() {
        Expression first = stack.pop();
        Expression scoreOperator = new ScoreOperator(first);
        stack.push(scoreOperator);
    }

    public void withGameOperator() {
        Expression teamOperator = stack.pop();
        Expression gameOperator = new GameOperator(teamOperator);
        stack.push(gameOperator);
    }

    public Expression getResult() {
        if (stack.isEmpty()) {
            throw new IllegalArgumentException();
        }
        if (stack.size() == 1) {
            return (stack.pop());
        }
        throw new IllegalArgumentException("Something wrong");
    }
}