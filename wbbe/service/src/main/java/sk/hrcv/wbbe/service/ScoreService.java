package sk.hrcv.wbbe.service;

import sk.hrcv.wbbe.dto.CreateScoreRequest;
import sk.hrcv.wbbe.dto.type.Score;

import javax.validation.Valid;

public interface ScoreService {

    @Valid
    Score createScore(@Valid final CreateScoreRequest request);

}
