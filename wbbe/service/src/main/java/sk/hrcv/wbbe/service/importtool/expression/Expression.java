package sk.hrcv.wbbe.service.importtool.expression;

import sk.hrcv.wbbe.dto.type.GameToImport;
import sk.hrcv.wbbe.service.importtool.expression.elements.ExpressionElement;

import java.util.Iterator;

public interface Expression {

    default GameToImport process() {
        throw new UnsupportedOperationException();
    }

    Iterator<ExpressionElement> getInOrderIterator();
}
