package sk.hrcv.wbbe.service;

import java.util.List;

public interface CalendarService {

    List<List<String>> computeFreeSlots(final List<List<String>> firstUnavailabilityCalendar,
                                        final List<String> firstOverallAvailability,
                                        final List<List<String>> secondUnavailabilityCalendar,
                                        final List<String> secondOverallAvailability);

}
