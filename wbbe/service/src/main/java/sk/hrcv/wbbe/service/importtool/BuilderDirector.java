package sk.hrcv.wbbe.service.importtool;

import org.apache.logging.log4j.util.Strings;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BuilderDirector {

    /**
     * [a-zA-Z]{3} matches three letters of team abbreviation
     * vs. matches team operator
     * > matches game operator
     * \d matches digit of score
     * : matches score operator
     * \s* matches zero or more whitespaces including [\r\n\t\f\v ]
     */
    private static final String IMPORT_GAME_PATTERN = "(^[a-zA-Z]{3})\\s*(vs.)\\s*([a-zA-Z]{3})\\s*(>)\\s*(\\d)\\s*(:)\\s*(\\d)";

    private final GameExpressionBuilder builder;

    public BuilderDirector(GameExpressionBuilder aeb) {
        this.builder = aeb;
    }

    public void construct(String input) {
        Pattern r = Pattern.compile(IMPORT_GAME_PATTERN);
        Matcher m = r.matcher(input);

        if (Strings.isBlank(input) || !m.matches()) {
            throw new IllegalArgumentException();
        }

        for (int i = 1; i <= m.groupCount(); i++) {
            String shred = m.group(i);

            if (shred.matches("\\d")) {
                builder.withNumericOperand(Long.parseLong(shred));
            } else if (shred.matches("[a-zA-Z]{3}")) {
                builder.withTeamOperand(shred);
            } else if (shred.matches("vs\\.")) {
                builder.withTeamOperator();
            } else if (shred.matches(":")) {
                builder.withScoreOperator();
            } else if (shred.matches(">")) {
                builder.withGameOperator();
            } else {
                throw new IllegalArgumentException(String.format("Unexpected token: %s", shred));
            }
        }
    }
}
