package sk.hrcv.wbbe.service.importtool.expression.elements;

public interface ExpressionElement {
    String stringValue();
}
