package sk.hrcv.wbbe.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import sk.hrcv.common.constant.ErrorCode;
import sk.hrcv.common.util.ApplicationContextHolder;
import sk.hrcv.common.util.ConditionChecker;
import sk.hrcv.config.exception.ResourceNotFoundException;
import sk.hrcv.wbbe.dto.CreatePlayerRequest;
import sk.hrcv.wbbe.dto.type.Player;
import sk.hrcv.wbbe.repository.PlayerRepository;
import sk.hrcv.wbbe.repository.TeamRepository;
import sk.hrcv.wbbe.repository.entity.PlayerEntity;
import sk.hrcv.wbbe.service.PlayerService;
import sk.hrcv.wbbe.service.TeamService;
import sk.hrcv.wbbe.service.mapper.MappersKt;

import java.util.List;

@Service
@Validated
public class PlayerServiceImpl implements PlayerService {

    private static final Logger log = LoggerFactory.getLogger(GameServiceImpl.class);

    private final PlayerRepository playerRepository;
    private final TeamRepository teamRepository;

    @Autowired
    public PlayerServiceImpl(PlayerRepository playerRepository, TeamRepository teamRepository) {
        this.playerRepository = playerRepository;
        this.teamRepository = teamRepository;
    }

    @Override
    public Player createNewPlayer(final CreatePlayerRequest request) {
        ConditionChecker.checkCondition(playerRepository.existsByNickname(request.getNickname()), ErrorCode.PLAYER_ALREADY_EXISTS);

        PlayerEntity playerToSave = new PlayerEntity();
        playerToSave.setNickname(request.getNickname());

        return MappersKt.toPlayerDto(
                playerRepository.saveAndFlush(playerToSave)
        );
    }

    @Override
    public List<Player> getAllPlayersInfo() {
        return MappersKt.toPlayerDtoList(
                playerRepository.findAll()
        );
    }

    @Override
    public Player getPlayerInfo(final Long playerId) {
        return MappersKt.toPlayerDto(
                getPlayer(playerId)
        );
    }

    @Override
    public void deletePlayer(final Long playerId) {
        //Obtain teamService via static method instead of autowiring
        TeamService teamService = ApplicationContextHolder.getApplicationContext().getBean(TeamService.class);
        //Ensure that player to be deleted exits in db. Throw exception if player not present in db.
        getPlayer(playerId);

        List<Long> allTeamsIdsContainsPlayer = teamRepository.findAllByPlayerIdInPlayerOneOrPlayerTwo(playerId);

        allTeamsIdsContainsPlayer.forEach(teamService::deleteTeam);

        playerRepository.deletePlayerById(playerId);
        log.trace(String.format("Player with id: %s was successfully deleted)", playerId));
    }

    @Override
    public void updatePlayer(final Long playerId, final String newPlayerNickname) {
        PlayerEntity player = getPlayer(playerId);
        player.setNickname(newPlayerNickname);

        playerRepository.saveAndFlush(player);
    }

    public PlayerEntity getPlayer(Long playerId) {
        PlayerEntity player = playerRepository.findByPlayerId(playerId);
        if (player == null) {
            throw new ResourceNotFoundException(String.format("Player with id = %s  not found", playerId));
        }
        return player;
    }
}