package sk.hrcv.wbbe.service.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.annotation.PostConstruct;

@Validated
@ConfigurationProperties(prefix = "game-service-properties")
@Component
public class GameServiceProperties {

    private Long actualSeason;
    private Long maximumGameDuration;

    @PostConstruct
    private void setDefaultValues() {
        //set default values if properties are not set in application.yml
        this.maximumGameDuration = 30L;
    }

    public Long getActualSeason() {
        return actualSeason;
    }

    public void setActualSeason(Long actualSeason) {
        this.actualSeason = actualSeason;
    }

    public Long getMaximumGameDuration() {
        return maximumGameDuration;
    }

    public void setMaximumGameDuration(Long maximumGameDuration) {
        this.maximumGameDuration = maximumGameDuration;
    }
}