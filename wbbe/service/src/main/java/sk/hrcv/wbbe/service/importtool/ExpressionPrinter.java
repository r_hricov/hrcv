package sk.hrcv.wbbe.service.importtool;

import sk.hrcv.wbbe.service.importtool.expression.Expression;
import sk.hrcv.wbbe.service.importtool.expression.elements.ExpressionElement;

import java.util.Iterator;

/**
 * Printer for {@link Expression}s. It can print inOrder notation (3 + 1) or postOrder
 */
public class ExpressionPrinter {
    private final Expression expression;

    public ExpressionPrinter(Expression expression) {
        this.expression = expression;
    }

    public String printInOrder() {
        StringBuilder result = new StringBuilder();
        for (Iterator<ExpressionElement> it = expression.getInOrderIterator(); it.hasNext(); ) {
            result.append(it.next().stringValue());
            result.append(" ");
        }
        System.out.println("result " + result.toString());
        return result.toString().trim();
    }
}
