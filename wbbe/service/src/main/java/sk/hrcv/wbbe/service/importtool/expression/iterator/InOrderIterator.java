package sk.hrcv.wbbe.service.importtool.expression.iterator;

import sk.hrcv.wbbe.service.importtool.expression.elements.ExpressionElement;

import java.util.Iterator;
import java.util.Stack;

public class InOrderIterator implements Iterator<ExpressionElement> {

    private final Stack<ExpressionElement> stack = new Stack<>();
    private int stackPosition = 0;

    public InOrderIterator(ExpressionElement elem) {
        stack.add(elem);
    }

    public InOrderIterator(InOrderIterator first, InOrderIterator second, InOrderIterator op) {
        this.stack.addAll(first.stack);
        this.stack.addAll(op.stack);
        this.stack.addAll(second.stack);
    }

    @Override
    public boolean hasNext() {
        return (stackPosition < stack.size());
    }

    @Override
    public ExpressionElement next() {
        return stack.get(stackPosition++);
    }

    @Override
    public void remove() {
        throw new UnsupportedOperationException("Don't know how to do it :(");
    }
}
