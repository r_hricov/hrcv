package sk.hrcv.wbbe.service.importtool.expression.elements;

import com.google.common.base.Objects;

public class GameOperation implements ExpressionElement {
    private static final String GAME_OPERATION = ">";

    @Override
    public String stringValue() {
        return GAME_OPERATION;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GameOperation that = (GameOperation) o;
        return Objects.equal(GAME_OPERATION, that.stringValue());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(GAME_OPERATION);
    }
}
