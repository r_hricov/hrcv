package sk.hrcv.wbbe.service.importtool;

import org.springframework.stereotype.Component;
import sk.hrcv.wbbe.service.importtool.expression.Expression;

@Component
public class ExpressionCreator {

    public Expression createGameFromString(String input) {
        GameExpressionBuilder aeBuilder = new GameExpressionBuilder();
        BuilderDirector bDirector = new BuilderDirector(aeBuilder);

        bDirector.construct(input);
        return aeBuilder.getResult();
    }
}
