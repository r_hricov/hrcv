package sk.hrcv.wbbe.service.importtool.expression;

import com.google.common.base.Objects;
import sk.hrcv.wbbe.service.importtool.expression.elements.ExpressionElement;
import sk.hrcv.wbbe.service.importtool.expression.elements.Number;
import sk.hrcv.wbbe.service.importtool.expression.iterator.InOrderIterator;

import java.util.Iterator;

/**
 * Represents number in the arithmetic expression
 */
public class ScoreOperand implements Expression {

    private final Long value;

    public ScoreOperand(Long value) {
        this.value = value;
    }

    public Long getValue() {
        return value;
    }

    @Override
    public Iterator<ExpressionElement> getInOrderIterator() {
        return new InOrderIterator(new Number(value));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ScoreOperand that = (ScoreOperand) o;
        return Objects.equal(value, that.value);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(value);
    }
}
