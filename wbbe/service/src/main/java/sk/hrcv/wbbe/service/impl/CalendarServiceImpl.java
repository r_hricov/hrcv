package sk.hrcv.wbbe.service.impl;

import com.google.common.base.Preconditions;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import sk.hrcv.wbbe.service.CalendarService;
import sk.hrcv.wbbe.service.properties.GameServiceProperties;

import java.util.*;
import java.util.stream.Collectors;

/**
 * According to input returns all time slots available for both teams for foosball match
 */
@Service
@Validated
public class CalendarServiceImpl implements CalendarService {

    private static final Logger log = LoggerFactory.getLogger(CalendarServiceImpl.class);
    private final GameServiceProperties properties;

    @Autowired
    public CalendarServiceImpl(GameServiceProperties properties) {
        this.properties = properties;
    }

    @Override
    public List<List<String>> computeFreeSlots(List<List<String>> firstUnavailabilityCalendar, List<String> firstOverallAvailability,
                                               List<List<String>> secondUnavailabilityCalendar, List<String> secondOverallAvailability) {

        //manageNotWorkingHours
        List<List<String>> notWorkingHours = manageNonWorkingHours(firstOverallAvailability, secondOverallAvailability);

        //join two unavailability calendars and not working hours
        List<List<String>> allUnavailableSlots = new ArrayList<>();
        allUnavailableSlots.addAll(notWorkingHours);
        allUnavailableSlots.addAll(firstUnavailabilityCalendar);
        allUnavailableSlots.addAll(secondUnavailabilityCalendar);

        //check data validity
        checkDataValidity(allUnavailableSlots);

        //sort joined list
        Comparator<List<String>> timesComparator = (s1, s2) -> compareTimes(s1.get(0), s2.get(0));
        allUnavailableSlots.sort(timesComparator);
        log.debug("result: {}", allUnavailableSlots);

        //merge slots if they overlap
        allUnavailableSlots = mergeTimes(allUnavailableSlots);
        log.debug("result: {}", allUnavailableSlots);

        //get free slots
        List<List<String>> freeSlots = new ArrayList<>();
        for (int i = 0; i < allUnavailableSlots.size() - 1; i++) {
            freeSlots.add(Arrays.asList(allUnavailableSlots.get(i).get(1), allUnavailableSlots.get(i + 1).get(0)));
        }
        log.debug("result: {}", freeSlots);

        //filter free slots with desired duration
        List<List<String>> finalFreeSlots = freeSlots.stream()
                .filter(it -> convertToMinutes(it.get(1)) - convertToMinutes(it.get(0)) >= properties.getMaximumGameDuration())
                .collect(Collectors.toList());
        log.debug("Final Free Slots: {}", finalFreeSlots);

        return finalFreeSlots;
    }

    private void checkDataValidity(List<List<String>> allUnavailableSlots) {
        allUnavailableSlots.forEach(unavailableSlot -> {
            Preconditions.checkArgument(unavailableSlot.size() == 2, "Invalid time slot data");
            Preconditions.checkArgument(0 > compareTimes(unavailableSlot.get(0), unavailableSlot.get(1)),
                    "Start of time slot must be before end");
        });
    }

    private List<List<String>> manageNonWorkingHours(List<String> firstOverallAvailability, List<String> secondOverallAvailability) {
        return new ArrayList<>(Arrays.asList(
                Arrays.asList("00:00", firstOverallAvailability.get(0)),
                Arrays.asList("00:00", secondOverallAvailability.get(0)),
                Arrays.asList(firstOverallAvailability.get(1), "23:59"),
                Arrays.asList(secondOverallAvailability.get(1), "23:59")
        ));
    }

    private int compareTimes(String t1, String t2) {
        int time1 = convertToMinutes(t1);
        int time2 = convertToMinutes(t2);

        return Integer.compare(time1, time2);
    }

    private int convertToMinutes(String time) {
        String[] hourAndMinutes = time.split(":");
        Preconditions.checkArgument(hourAndMinutes.length == 2, "Malformed time format. Should be hh:mm");
        return Integer.parseInt(hourAndMinutes[0]) * 60 + Integer.parseInt(hourAndMinutes[1]);
    }

    private List<List<String>> mergeTimes(List<List<String>> allUnavailableSlots) {
        if (allUnavailableSlots.isEmpty()) {
            return Collections.emptyList();
        }

        List<List<String>> resultList = new ArrayList<>();
        List<String> lastMerged = allUnavailableSlots.get(0);

        //merge two slots if they overlap
        for (int i = 1; i < allUnavailableSlots.size(); i++) {
            //end time of first is after start time of other
            if (-1 != compareTimes(lastMerged.get(1), allUnavailableSlots.get(i).get(0))) {
                //end time of first is after end time of other
                if (1 == compareTimes(lastMerged.get(1), allUnavailableSlots.get(i).get(1))) {
                    continue;
                }
                // should merge
                lastMerged = Arrays.asList(lastMerged.get(0), allUnavailableSlots.get(i).get(1));
            } else {
                resultList.add(lastMerged);
                lastMerged = allUnavailableSlots.get(i);
            }
        }
        resultList.add(lastMerged);

        return resultList;
    }
}
