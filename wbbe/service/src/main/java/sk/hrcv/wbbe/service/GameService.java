package sk.hrcv.wbbe.service;

import sk.hrcv.wbbe.dto.CreateGameRequest;
import sk.hrcv.wbbe.dto.type.Game;
import sk.hrcv.wbbe.dto.type.GameToImport;
import sk.hrcv.wbbe.repository.entity.GameEntity;

import javax.validation.Valid;
import java.util.List;

public interface GameService {

    /**
     * The method creates a new Game and stores it to the DB
     *
     * @param request CreateGameRequest containing at least all mandatory fields
     * @return new created Game object
     */
    @Valid
    Game createNewGame(@Valid final CreateGameRequest request);

    void createNewImportedGame(@Valid final GameToImport gameToImport);

    /**
     * @return List of all stored Games
     */
    @Valid
    List<Game> getAllGamesInfo(boolean played, boolean proposed);

    /**
     * @param teamId Id of desired team
     * @return All Games in which team based on teamId from request is presented
     */
    @Valid
    List<Game> getAllGamesInfoForTeam(final Long teamId, boolean played, boolean proposed);

    /**
     * @param playerId Id of Player to find games in that Player played
     * @return All Games in which player based on playerId from request is presented
     */
    @Valid
    List<Game> getAllGamesInfoForPlayer(final Long playerId, boolean played, boolean proposed);

    /**
     * @param gameId Id of desired Game
     * @return Game object based on its id
     */
    @Valid
    Game getGameInfo(final Long gameId);

    GameEntity getGame(final Long gameId);
}