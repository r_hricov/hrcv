package sk.hrcv.wbbe.service.importtool.expression.elements;

import com.google.common.base.Objects;

public class VersusOperation implements ExpressionElement {
    private static final String VERSUS_OPERATION = "vs.";

    @Override
    public String stringValue() {
        return VERSUS_OPERATION;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VersusOperation that = (VersusOperation) o;
        return Objects.equal(VERSUS_OPERATION, that.stringValue());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(VERSUS_OPERATION);
    }
}
