package sk.hrcv.wbbe.service.importtool.expression.elements;

public class TeamAbbr implements ExpressionElement {
    private final String teamAbbr;

    public TeamAbbr(String teamAbbr) {
        this.teamAbbr = teamAbbr;
    }

    @Override
    public String stringValue() {
        return teamAbbr;
    }
}
