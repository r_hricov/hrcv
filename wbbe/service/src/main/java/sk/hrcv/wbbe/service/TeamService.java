package sk.hrcv.wbbe.service;

import sk.hrcv.wbbe.dto.CreateTeamRequest;
import sk.hrcv.wbbe.dto.type.Team;
import sk.hrcv.wbbe.repository.entity.TeamEntity;

import javax.validation.Valid;
import java.util.List;

public interface TeamService {

    /**
     * The method creates a new Team based on Players and stores it to the DB
     *
     * @param createTeamRequest CreateTeamRequest containing at least all mandatory fields
     * @return new created Team object
     */
    @Valid
    Team createNewTeam(@Valid final CreateTeamRequest createTeamRequest);

    /**
     * @return List of all stored Teams
     */
    @Valid
    List<Team> getAllTeamsInfo();

    /**
     * @param teamId Id of desired Team
     * @return Team object based on its id
     */
    @Valid
    Team getTeamInfo(final Long teamId);

    /**
     * Method deletes Team from DB based on its id.
     * It also deletes all Games that contains deleted Team.
     *
     * @param teamId Id of desired Team
     */
    void deleteTeam(final Long teamId);

    TeamEntity getTeam(final Long teamId);
}
