package sk.hrcv.wbbe.service;

import sk.hrcv.wbbe.dto.type.TableRecord;

import javax.validation.Valid;
import java.util.List;

public interface TableService {

    @Valid
    List<TableRecord> getTableRecords();

}
