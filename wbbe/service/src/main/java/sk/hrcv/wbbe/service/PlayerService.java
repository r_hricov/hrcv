package sk.hrcv.wbbe.service;

import sk.hrcv.wbbe.dto.CreatePlayerRequest;
import sk.hrcv.wbbe.dto.type.Player;
import sk.hrcv.wbbe.repository.entity.PlayerEntity;

import javax.validation.Valid;
import java.util.List;

public interface PlayerService {

    /**
     * The method creates a new Player and stores it to the DB
     *
     * @param request CreatePlayerRequest containing at least all mandatory fields
     * @return new created Player object
     */
    @Valid
    Player createNewPlayer(@Valid final CreatePlayerRequest request);

    /**
     * @return List of all stored Players
     */
    @Valid
    List<Player> getAllPlayersInfo();

    /**
     * @param playerId Id of desired Player
     * @return Player object based on its id
     */
    @Valid
    Player getPlayerInfo(final Long playerId);

    /**
     * Method deletes Player from DB based on its id.
     * It also deletes all Teams that contains Player and all Games that contains deleted Teams.
     *
     * @param playerId Id of Player
     */
    void deletePlayer(final Long playerId);

    /**
     * Method updates Player given by playe id.
     *
     * @param playerId          Id of Player* @param player Object of player to be updated
     * @param newPlayerNickname new name
     */
    void updatePlayer(final Long playerId, final String newPlayerNickname);

    PlayerEntity getPlayer(Long playerId);
}