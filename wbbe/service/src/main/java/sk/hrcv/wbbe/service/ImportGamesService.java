package sk.hrcv.wbbe.service;

import sk.hrcv.wbbe.dto.ImportGameRequest;

import javax.validation.Valid;
import java.util.List;

public interface ImportGamesService {

    List<String> importGames(@Valid ImportGameRequest request);

}
