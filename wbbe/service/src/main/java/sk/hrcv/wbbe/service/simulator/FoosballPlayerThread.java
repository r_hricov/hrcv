package sk.hrcv.wbbe.service.simulator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.Stopwatch;

import java.util.Random;

public class FoosballPlayerThread implements Runnable {
    public static final int MAX_RANDOM_VALUE = 10;

    @JsonIgnore
    private final Foosball game;
    private final Stopwatch stopWatch;
    private final ScoreObj scoreObj;
    private final int luckyNumber;
    private final boolean isHomeTeam;

    public FoosballPlayerThread(Foosball game, ScoreObj scoreObj, boolean isHomeTeam, Stopwatch stopWatch) {
        this.game = game;
        this.stopWatch = stopWatch;
        this.scoreObj = scoreObj;
        this.luckyNumber = new Random().nextInt(MAX_RANDOM_VALUE);
        this.isHomeTeam = isHomeTeam;
    }

    public void run() {
        boolean run = true;
        while (run) {
            run = game.play(scoreObj, luckyNumber, isHomeTeam, stopWatch);
        }
    }
}
