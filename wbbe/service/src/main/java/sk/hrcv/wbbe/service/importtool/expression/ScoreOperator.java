package sk.hrcv.wbbe.service.importtool.expression;

import sk.hrcv.wbbe.service.importtool.expression.elements.ScoreOperation;

/**
 * Represents - operation
 */
public class ScoreOperator extends BinaryOperator {
    public ScoreOperator(Expression firstOperand) {
        super(firstOperand, new ScoreOperation());
    }

    public ScoreOperator(Expression firstOperand, Expression secondOperand) {
        super(firstOperand, secondOperand);
        super.setExpressionElement(new ScoreOperation());
    }

    public ScoreOperand getScoreOfTeamA() {
        return (ScoreOperand) super.getFirst();
    }

    public ScoreOperand getScoreOfTeamB() {
        return (ScoreOperand) super.getSecond();
    }
}
