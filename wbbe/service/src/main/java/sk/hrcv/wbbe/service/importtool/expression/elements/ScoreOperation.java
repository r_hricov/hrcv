package sk.hrcv.wbbe.service.importtool.expression.elements;

import com.google.common.base.Objects;

public class ScoreOperation implements ExpressionElement {
    private static final String SCORE_OPERATION = ":";

    @Override
    public String stringValue() {
        return SCORE_OPERATION;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ScoreOperation that = (ScoreOperation) o;
        return Objects.equal(SCORE_OPERATION, that.stringValue());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(SCORE_OPERATION);
    }
}
