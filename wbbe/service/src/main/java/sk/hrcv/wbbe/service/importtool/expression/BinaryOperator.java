package sk.hrcv.wbbe.service.importtool.expression;

import com.google.common.base.Objects;
import sk.hrcv.wbbe.service.importtool.expression.elements.ExpressionElement;
import sk.hrcv.wbbe.service.importtool.expression.iterator.InOrderIterator;

import java.util.Iterator;

/**
 * Represents the Binary operations like + - etc.
 */
public abstract class BinaryOperator implements Expression {

    private final Expression first;
    private Expression second;
    private ExpressionElement elem;

    public BinaryOperator(Expression first, ExpressionElement elem) {
        this.first = first;
        this.elem = elem;
    }

    public BinaryOperator(Expression first, Expression second) {
        this.first = first;
        this.second = second;
    }

    public void setExpressionElement(ExpressionElement elem) {
        this.elem = elem;
    }

    public Object getFirst() {
        return first;
    }

    public Object getSecond() {
        return second;
    }

    public void setSecond(Expression second) {
        this.second = second;
    }

    @Override
    public Iterator<ExpressionElement> getInOrderIterator() {
        Iterator<ExpressionElement> first = this.first.getInOrderIterator();
        Iterator<ExpressionElement> second = this.second.getInOrderIterator();
        return new InOrderIterator((InOrderIterator) first, (InOrderIterator) second, new InOrderIterator(elem));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BinaryOperator that = (BinaryOperator) o;
        return Objects.equal(first, that.first) &&
                Objects.equal(second, that.second) &&
                Objects.equal(elem, that.elem);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(first, second, elem);
    }
}
