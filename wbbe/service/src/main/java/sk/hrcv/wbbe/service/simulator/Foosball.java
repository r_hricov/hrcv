package sk.hrcv.wbbe.service.simulator;

import com.google.common.base.Stopwatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sk.hrcv.wbbe.dto.type.Score;

import java.util.Random;
import java.util.concurrent.TimeUnit;

import static sk.hrcv.wbbe.service.simulator.FoosballPlayerThread.MAX_RANDOM_VALUE;

public class Foosball {

    private static final Logger LOG = LoggerFactory.getLogger(Foosball.class);
    private static final int WINNING_SCORE = 10;

    public synchronized boolean play(ScoreObj scoreObj, int luckyNumber, boolean isHomeTeam, Stopwatch stopWatch) {
        String currentPlayer = Thread.currentThread().getName();

        //first thread (team) always starts the game (eventually it could be changed that home team always starts)
        if (scoreObj.getLastShot() == null) {
            scoreObj.setLastShot(currentPlayer);
            return true;
        }

        //game finishes when one of teams scores 10 goals
        if (scoreObj.getScore().getHomeScore() == WINNING_SCORE || scoreObj.getScore().getVisitorScore() == WINNING_SCORE) {
            scoreObj.setLastShot(currentPlayer);
            return false;
        }

        if (!currentPlayer.equals(scoreObj.getLastShot())) {
            playTurn(currentPlayer, luckyNumber, isHomeTeam, scoreObj, stopWatch);
        } else {
            try {
                //wait for opponent
                wait(100);
            } catch (InterruptedException e) {
                return false;
            }
        }
        return true;
    }

    private void playTurn(String currentPlayer, int luckyNumber, boolean isHomeTeam, ScoreObj scoreObj, Stopwatch stopWatch) {
        LOG.debug("TRYING! (" + currentPlayer + ")");
        if (luckyNumber == new Random().nextInt(MAX_RANDOM_VALUE)) {
            if (isHomeTeam) {
                scoreObj.getScore().setHomeScore(scoreObj.getScore().getHomeScore() + 1);
            } else {
                scoreObj.getScore().setVisitorScore(scoreObj.getScore().getVisitorScore() + 1);
            }

            Score score = new Score();
            score.setHomeScore(scoreObj.getScore().getHomeScore());
            score.setVisitorScore(scoreObj.getScore().getVisitorScore());
            scoreObj.getGameEvents().add(new ScoreObj.GameEvent(score, stopWatch.elapsed(TimeUnit.MILLISECONDS)));
            LOG.trace(scoreObj.getScore().getHomeScore() + ":" + scoreObj.getScore().getVisitorScore() + " " + currentPlayer + " SCORES!");
        }
        scoreObj.setLastShot(currentPlayer);
        //my turn is done, notify other threads
        notifyAll();
    }
}
