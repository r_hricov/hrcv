package sk.hrcv.wbbe.service.importtool.expression;

import sk.hrcv.wbbe.service.importtool.expression.elements.VersusOperation;

/**
 * Represents 'vs.' operation
 */
public class TeamOperator extends BinaryOperator {

    public TeamOperator(Expression firstOperand) {
        super(firstOperand, new VersusOperation());
    }

    public TeamOperator(Expression firstOperand, Expression secondOperand) {
        super(firstOperand, secondOperand);
        super.setExpressionElement(new VersusOperation());
    }

    public TeamOperand getTeamA() {
        return (TeamOperand) super.getFirst();
    }

    public TeamOperand getTeamB() {
        return (TeamOperand) super.getSecond();
    }
}
