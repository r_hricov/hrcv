package sk.hrcv.wbbe.service;

import sk.hrcv.wbbe.dto.type.Game;
import sk.hrcv.wbbe.service.simulator.ScoreObj;

public interface FoosballGameSimulator {

    ScoreObj playFoosball(Game match);

}
