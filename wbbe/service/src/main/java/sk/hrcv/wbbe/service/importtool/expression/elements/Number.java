package sk.hrcv.wbbe.service.importtool.expression.elements;

public class Number implements ExpressionElement {

    private final Long value;

    public Number(Long value) {
        this.value = value;
    }

    @Override
    public String stringValue() {
        return String.valueOf(value);
    }
}
