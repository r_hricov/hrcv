package sk.hrcv.wbbe.service.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.annotation.PostConstruct;

@Validated
@ConfigurationProperties(prefix = "table-service-properties")
@Component
public class TableServiceProperties {
    private Long wonGamesMultiplier;
    private Long drawnGamesMultiplier;
    private Long actualSeason;

    @PostConstruct
    private void setDefaultValues() {
        //set default values if properties are not set in application.yml
        this.wonGamesMultiplier = 1L;
        this.drawnGamesMultiplier = 0L;
    }

    public Long getWonGamesMultiplier() {
        return wonGamesMultiplier;
    }

    public void setWonGamesMultiplier(Long wonGamesMultiplier) {
        this.wonGamesMultiplier = wonGamesMultiplier;
    }

    public Long getDrawnGamesMultiplier() {
        return drawnGamesMultiplier;
    }

    public void setDrawnGamesMultiplier(Long drawnGamesMultiplier) {
        this.drawnGamesMultiplier = drawnGamesMultiplier;
    }

    public Long getActualSeason() {
        return actualSeason;
    }

    public void setActualSeason(Long actualSeason) {
        this.actualSeason = actualSeason;
    }
}