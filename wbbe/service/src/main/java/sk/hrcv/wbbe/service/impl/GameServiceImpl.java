package sk.hrcv.wbbe.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import sk.hrcv.common.util.ApplicationContextHolder;
import sk.hrcv.config.exception.ResourceNotFoundException;
import sk.hrcv.wbbe.dto.CreateGameRequest;
import sk.hrcv.wbbe.dto.CreateGameRequestKt;
import sk.hrcv.wbbe.dto.CreateScoreRequest;
import sk.hrcv.wbbe.dto.type.Game;
import sk.hrcv.wbbe.dto.type.GameToImport;
import sk.hrcv.wbbe.repository.GameRepository;
import sk.hrcv.wbbe.repository.TeamRepository;
import sk.hrcv.wbbe.repository.entity.GameEntity;
import sk.hrcv.wbbe.repository.entity.TeamEntity;
import sk.hrcv.wbbe.service.GameService;
import sk.hrcv.wbbe.service.ScoreService;
import sk.hrcv.wbbe.service.TeamService;
import sk.hrcv.wbbe.service.mapper.MappersKt;
import sk.hrcv.wbbe.service.properties.GameServiceProperties;

import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
@Validated
public class GameServiceImpl implements GameService {

    private static final Logger log = LoggerFactory.getLogger(GameServiceImpl.class);

    private final GameRepository gameRepository;
    private final GameServiceProperties properties;
    private final ScoreService scoreService;
    private final TeamService teamService;

    @Autowired
    public GameServiceImpl(GameRepository gameRepository, GameServiceProperties gameServiceProperties, ScoreService scoreService, TeamService teamService) {
        this.gameRepository = gameRepository;
        this.properties = gameServiceProperties;
        this.scoreService = scoreService;
        this.teamService = teamService;
    }

    /**
     * if there is no score object in request it means that proposed game is about to save
     * score for proposed game can be save later
     * proposed game has no date set
     */
    @Override
    public Game createNewGame(CreateGameRequest request) {
        GameEntity gameToSave = new GameEntity();

        LocalDateTime dateTime = LocalDateTime.now();
        if (request.getDateTime() != null && request.getScore() != null) {
            dateTime = request.getDateTime();
        } else if (request.getScore() == null) {
            dateTime = null;
        }

        //getTeam also validates that teams exists
        gameToSave.setTeamA(teamService.getTeam(request.getTeamAId()));
        gameToSave.setTeamB(teamService.getTeam(request.getTeamBId()));
        gameToSave.setDate(dateTime);
        gameToSave.setSeason(properties.getActualSeason());

        gameRepository.saveAndFlush(gameToSave);

        if (request.getScore() != null) {
            CreateScoreRequest createScoreRequest = new CreateScoreRequest(gameToSave.getGameId(), request.getScore());
            scoreService.createScore(createScoreRequest);
        }

        return MappersKt.toGameDto(gameToSave);
    }

    @Override
    public void createNewImportedGame(@Valid GameToImport gameToImport) {
        TeamRepository teamRepository = ApplicationContextHolder.getApplicationContext().getBean(TeamRepository.class);
        TeamEntity teamA = teamRepository.findByTeamNameAbbr(gameToImport.getTeamAAbbr());
        TeamEntity teamB = teamRepository.findByTeamNameAbbr(gameToImport.getTeamBAbbr());

        createNewGame(CreateGameRequestKt.builder()
                .withTeamAId(Objects.requireNonNull(teamA).getTeamId())
                .withTeamBId(Objects.requireNonNull(teamB).getTeamId())
                .withScore(gameToImport.getScore())
                .withDate(gameToImport.getDateTime())
                .build());
    }

    @Override
    public List<Game> getAllGamesInfo(boolean played, boolean proposed) {
        List<GameEntity> games = new ArrayList<>();

        if (played) {
            games.addAll(gameRepository.findAllByDateNotNull());
        }

        if (proposed) {
            games.addAll(gameRepository.findAllByDateIsNull());
        }

        return MappersKt.toGameDtoList(games);
    }

    @Override
    public List<Game> getAllGamesInfoForTeam(final Long teamId, boolean played, boolean proposed) {
        List<GameEntity> games = gameRepository.findGamesByTeam(teamId);

        if (!(played && proposed)) {
            games = filterGames(games, played, proposed);
        }

        return MappersKt.toGameDtoList(games);
    }

    @Override
    public List<Game> getAllGamesInfoForPlayer(final Long playerId, boolean played, boolean proposed) {
        List<GameEntity> allGames = gameRepository.findAll();

        List<GameEntity> allGamesForPlayer = allGames.stream()
                .filter(game -> teamContainsPlayer(playerId, game.getTeamA()) || teamContainsPlayer(playerId, game.getTeamB()))
                .collect(Collectors.toList());

        if (!(played && proposed)) {
            allGamesForPlayer = filterGames(allGamesForPlayer, played, proposed);
        }

        return MappersKt.toGameDtoList(allGamesForPlayer);
    }

    @Override
    public Game getGameInfo(final Long gameId) {
        return MappersKt.toGameDto(getGame(gameId));
    }

    @Override
    public GameEntity getGame(final Long gameId) {
        GameEntity game = gameRepository.findByGameId(gameId);
        if (game == null) {
            throw new ResourceNotFoundException(String.format("Game for id %s not found", gameId));
        }
        return game;
    }

    private boolean teamContainsPlayer(Long playerId, TeamEntity team) {
        return playerId.equals(team.getPlayerOne().getPlayerId()) || (team.getPlayerTwo() != null &&
                playerId.equals(team.getPlayerTwo().getPlayerId()));
    }

    private List<GameEntity> filterGames(List<GameEntity> games, boolean played, boolean proposed) {
        List<GameEntity> filteredGames = new ArrayList<>();

        if (played) {
            filteredGames = games.stream().filter(it -> it.getDate() != null).collect(Collectors.toList());
        } else if (proposed) {
            filteredGames = games.stream().filter(it -> it.getDate() == null).collect(Collectors.toList());
        }

        return filteredGames;
    }
}