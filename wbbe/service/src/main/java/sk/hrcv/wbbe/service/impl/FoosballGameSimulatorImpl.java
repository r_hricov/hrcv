package sk.hrcv.wbbe.service.impl;

import com.google.common.base.Stopwatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import sk.hrcv.wbbe.dto.type.Game;
import sk.hrcv.wbbe.service.FoosballGameSimulator;
import sk.hrcv.wbbe.service.simulator.Foosball;
import sk.hrcv.wbbe.service.simulator.FoosballPlayerThread;
import sk.hrcv.wbbe.service.simulator.ScoreObj;

@Component
public class FoosballGameSimulatorImpl implements FoosballGameSimulator {

    private static final Logger LOG = LoggerFactory.getLogger(FoosballGameSimulatorImpl.class);

    @Override
    public ScoreObj playFoosball(Game match) {
        LOG.debug("Foosball game started");
        Stopwatch stopwatch = Stopwatch.createStarted();

        Foosball game = new Foosball();
        ScoreObj scoreObj = new ScoreObj();

        Thread homeTeam = new Thread(new FoosballPlayerThread(game, scoreObj, true, stopwatch), match.getTeamA().getTeamName());
        Thread visitorTeam = new Thread(new FoosballPlayerThread(game, scoreObj, false, stopwatch), match.getTeamB().getTeamName());

        homeTeam.start();
        visitorTeam.start();

        try {
            // Wait till game finish, If no team scores 10 goals within 500ms the game is ended
            Thread.sleep(500);
            homeTeam.interrupt();
            visitorTeam.interrupt();
        } catch (InterruptedException e) {
            System.out.println(e.getMessage());
        }

        stopwatch.stop();
        LOG.trace(homeTeam.getName() + " " + scoreObj.getScore().getHomeScore() + ":" + scoreObj.getScore().getVisitorScore() + " " + visitorTeam.getName());
        LOG.debug("Foosball game finished");
        return scoreObj;
    }
}