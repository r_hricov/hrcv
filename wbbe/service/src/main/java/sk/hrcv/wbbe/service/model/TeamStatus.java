package sk.hrcv.wbbe.service.model;

public class TeamStatus {

    private Long drawnGamesCount;
    private Long wonGamesCount;
    private Long lostGamesCount;

    public TeamStatus(Long drawnGamesCount, Long wonGamesCount, Long lostGamesCount) {
        this.drawnGamesCount = drawnGamesCount;
        this.wonGamesCount = wonGamesCount;
        this.lostGamesCount = lostGamesCount;
    }

    public Long getDrawnGamesCount() {
        return drawnGamesCount;
    }

    public void setDrawnGamesCount(Long drawnGamesCount) {
        this.drawnGamesCount = drawnGamesCount;
    }

    public Long getWonGamesCount() {
        return wonGamesCount;
    }

    public void setWonGamesCount(Long wonGamesCount) {
        this.wonGamesCount = wonGamesCount;
    }

    public Long getLostGamesCount() {
        return lostGamesCount;
    }

    public void setLostGamesCount(Long lostGamesCount) {
        this.lostGamesCount = lostGamesCount;
    }
}
