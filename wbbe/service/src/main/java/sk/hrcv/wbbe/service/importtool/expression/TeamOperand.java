package sk.hrcv.wbbe.service.importtool.expression;

import com.google.common.base.Objects;
import sk.hrcv.wbbe.service.importtool.expression.elements.ExpressionElement;
import sk.hrcv.wbbe.service.importtool.expression.elements.TeamAbbr;
import sk.hrcv.wbbe.service.importtool.expression.iterator.InOrderIterator;

import java.util.Iterator;

public class TeamOperand implements Expression {
    private final String teamAbbr;

    public TeamOperand(String teamAbbr) {
        this.teamAbbr = teamAbbr;
    }

    public String getTeamAbbr() {
        return teamAbbr;
    }

    @Override
    public Iterator<ExpressionElement> getInOrderIterator() {
        return new InOrderIterator(new TeamAbbr(teamAbbr));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TeamOperand that = (TeamOperand) o;
        return Objects.equal(teamAbbr, that.teamAbbr);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(teamAbbr);
    }
}
