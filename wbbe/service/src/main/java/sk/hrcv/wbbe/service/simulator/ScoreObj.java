package sk.hrcv.wbbe.service.simulator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import sk.hrcv.wbbe.dto.type.Score;

import java.util.ArrayList;
import java.util.List;

public class ScoreObj {
    @JsonIgnore
    private String lastShot;
    private Score score;
    private List<GameEvent> gameEvents;

    public ScoreObj() {
        score = new Score();
        gameEvents = new ArrayList<>();
    }

    public String getLastShot() {
        return lastShot;
    }

    public void setLastShot(String lastShot) {
        this.lastShot = lastShot;
    }

    public Score getScore() {
        return score;
    }

    public void setScore(Score score) {
        this.score = score;
    }

    public List<GameEvent> getGameEvents() {
        return gameEvents;
    }

    public void setGameEvents(List<GameEvent> gameEvents) {
        this.gameEvents = gameEvents;
    }

    public static class GameEvent {
        private Score score;
        private Long time;

        public GameEvent(Score score, Long time) {
            this.score = score;
            this.time = time;
        }

        public Score getScore() {
            return score;
        }

        public void setScore(Score score) {
            this.score = score;
        }

        public Long getTime() {
            return time;
        }

        public void setTime(Long time) {
            this.time = time;
        }
    }
}
