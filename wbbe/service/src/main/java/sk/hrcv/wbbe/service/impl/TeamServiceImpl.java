package sk.hrcv.wbbe.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;
import sk.hrcv.common.constant.ErrorCode;
import sk.hrcv.common.util.ConditionChecker;
import sk.hrcv.config.exception.ResourceNotFoundException;
import sk.hrcv.wbbe.dto.CreateTeamRequest;
import sk.hrcv.wbbe.dto.type.Team;
import sk.hrcv.wbbe.repository.GameRepository;
import sk.hrcv.wbbe.repository.ScoreRepository;
import sk.hrcv.wbbe.repository.TeamRepository;
import sk.hrcv.wbbe.repository.entity.GameEntity;
import sk.hrcv.wbbe.repository.entity.TeamEntity;
import sk.hrcv.wbbe.service.PlayerService;
import sk.hrcv.wbbe.service.TeamService;
import sk.hrcv.wbbe.service.mapper.MappersKt;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Validated
public class TeamServiceImpl implements TeamService {

    private static final Logger log = LoggerFactory.getLogger(TeamServiceImpl.class);

    private final TeamRepository teamRepository;
    private final GameRepository gameRepository;
    private final ScoreRepository scoreRepository;
    private final PlayerService playerService;

    @Autowired
    public TeamServiceImpl(TeamRepository teamRepository, GameRepository gameRepository, ScoreRepository scoreRepository, PlayerService playerService) {
        this.teamRepository = teamRepository;
        this.gameRepository = gameRepository;
        this.scoreRepository = scoreRepository;
        this.playerService = playerService;
    }

    @Override
    public Team createNewTeam(final CreateTeamRequest request) {
        // Check teamName/teamNameAbbr not used by another team
        ConditionChecker.checkCondition(teamRepository.existsByTeamNameAbbr(request.getTeamNameAbbr()) ||
                teamRepository.existsByTeamName(request.getTeamName()), ErrorCode.TEAM_ALREADY_EXISTS);

        TeamEntity teamToSave = new TeamEntity();
        teamToSave.setTeamName(request.getTeamName());
        teamToSave.setTeamNameAbbr(request.getTeamNameAbbr());
        //getPlayer also validates that players already exist in db
        teamToSave.setPlayerOne(playerService.getPlayer(request.getPlayerOneId()));
        if (request.getPlayerTwoId() != null) {
            teamToSave.setPlayerTwo(playerService.getPlayer(request.getPlayerTwoId()));
        }

        teamRepository.saveAndFlush(teamToSave);

        return MappersKt.toTeamDto(teamToSave);
    }

    @Override
    public List<Team> getAllTeamsInfo() {
        return MappersKt.toTeamDtoList(teamRepository.findAll());
    }

    @Override
    public Team getTeamInfo(final Long teamId) {
        return MappersKt.toTeamDto(getTeam(teamId));
    }

    @Override
    public void deleteTeam(final Long teamId) {
        //Ensure that team to be deleted exits in db. Throw exception if team not present in db.
        getTeamInfo(teamId);

        List<GameEntity> allByTeamInTeamAOrTeamB = gameRepository.findGamesByTeam(teamId);
        List<Long> gameIds = allByTeamInTeamAOrTeamB.stream().map(GameEntity::getGameId).collect(Collectors.toList());

        if (!allByTeamInTeamAOrTeamB.isEmpty()) {
            scoreRepository.deleteAllByGameIdIn(gameIds);
            gameRepository.deleteAllGamesIn(gameIds);
            log.trace(String.format("Dependent Games and Scores were successfully deleted for teamId: %s", teamId));
        }

        teamRepository.deleteTeamByIdIn(Collections.singletonList(teamId));
    }

    public TeamEntity getTeam(final Long teamId) {
        TeamEntity team = teamRepository.findByTeamId(teamId);
        if (team == null) {
            throw new ResourceNotFoundException(String.format("Team with id = %s  not found", teamId));
        }

        return team;
    }
}
