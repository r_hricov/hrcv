package sk.hrcv.wbbe.service.test;

import com.google.common.base.Objects;
import org.testng.Assert;
import org.testng.annotations.Test;

@Test
public class MergeLinkedList {

    public void test() {
        ListNode l1 = new ListNode(1, new ListNode(2, new ListNode(4)));
        ListNode l2 = new ListNode(1, new ListNode(3, new ListNode(4)));

        ListNode result = mergeTwoLists(l1, l2);

        ListNode expected = new ListNode(1, new ListNode(1, new ListNode(2, new ListNode(3, new ListNode(4, new ListNode(4))))));
        Assert.assertEquals(expected, result);
    }

    private ListNode mergeTwoLists(ListNode l1, ListNode l2) {
        ListNode root = new ListNode(-1);
        ListNode newRoot = root;

        while (true) {
            if (l1 == null) {
                newRoot.next = l2;
                break;
            }
            if (l2 == null) {
                newRoot.next = l1;
                break;
            }


            if (l1.val <= l2.val) {
                newRoot.next = l1;
                newRoot = l1;
                l1 = l1.next;
            } else {
                newRoot.next = l2;
                newRoot = l2;
                l2 = l2.next;
            }
        }
        return root.next;
    }

    private static class ListNode {
        final int val;
        ListNode next;

        ListNode(int val) {
            this.val = val;
        }

        ListNode(int val, ListNode next) {
            this.val = val;
            this.next = next;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            ListNode listNode = (ListNode) o;
            return val == listNode.val &&
                    Objects.equal(next, listNode.next);
        }

        @Override
        public int hashCode() {
            return Objects.hashCode(val, next);
        }
    }
}
