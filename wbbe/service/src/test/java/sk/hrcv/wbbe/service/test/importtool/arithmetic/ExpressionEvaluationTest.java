package sk.hrcv.wbbe.service.test.importtool.arithmetic;

import org.testng.annotations.Test;
import sk.hrcv.test.config.AbstractTransactionalSpringBootTestWithData;
import sk.hrcv.test.config.TestGroup;
import sk.hrcv.wbbe.dto.type.GameToImport;
import sk.hrcv.wbbe.service.test.importtool.TestExpressionsFactory;

import static org.testng.Assert.assertEquals;

@Test(groups = TestGroup.UNIT)
public class ExpressionEvaluationTest extends AbstractTransactionalSpringBootTestWithData {

    public void testExpression1Evaluation() {
        GameToImport game = TestExpressionsFactory.createExpression1().process();

        assertEquals("BAR", game.getTeamAAbbr());
        assertEquals("FOO", game.getTeamBAbbr());
    }
}
