package sk.hrcv.wbbe.service.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;
import sk.hrcv.test.config.AbstractTransactionalSpringBootTestWithData;
import sk.hrcv.test.config.TestGroup;
import sk.hrcv.wbbe.dto.CreateTeamRequest;
import sk.hrcv.wbbe.dto.type.Team;
import sk.hrcv.wbbe.repository.TeamRepository;
import sk.hrcv.wbbe.repository.entity.TeamEntity;
import sk.hrcv.wbbe.service.TeamService;

@Test(groups = TestGroup.UNIT)
public class TeamServiceTest extends AbstractTransactionalSpringBootTestWithData {

    @Autowired
    private TeamService teamService;
    @Autowired
    private TeamRepository teamRepository;

    public void createTeamTest() {
        CreateTeamRequest createTeamRequest = new CreateTeamRequest();
        createTeamRequest.setTeamName("DummyTeamName");
        createTeamRequest.setTeamNameAbbr("DTN");
        createTeamRequest.setPlayerOneId(2L);

        Team newTeam = teamService.createNewTeam(createTeamRequest);
        Assert.assertNotNull(newTeam.getTeamId());
        Assert.assertNotNull(teamService.getTeamInfo(newTeam.getTeamId()));
    }

    /**
     * Initial data are already in DB so teamService#getAllTeamsInfo should not return empty list
     */
    public void getAllTeamsTest() {
        Assert.assertFalse(teamService.getAllTeamsInfo().isEmpty());
        Assert.assertEquals(teamService.getAllTeamsInfo().size(), 3);
    }

    /**
     * For team with id=0 records from PLAYER and one from TEAM tables are deleted
     */
    public void deleteTeamTest() {
        teamService.deleteTeam(0L);

        TeamEntity nullTeam = teamRepository.findByTeamId(0L);
        Assert.assertNull(nullTeam);
    }


}
