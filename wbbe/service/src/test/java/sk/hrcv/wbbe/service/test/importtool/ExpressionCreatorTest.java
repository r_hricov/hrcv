package sk.hrcv.wbbe.service.test.importtool;

import org.testng.annotations.Test;
import sk.hrcv.wbbe.service.importtool.ExpressionCreator;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;

public class ExpressionCreatorTest {

    private final ExpressionCreator aec = new ExpressionCreator();

    @Test
    public void testComplexExpressionCreation() {
        assertEquals(
                TestExpressionsFactory.createExpression1(),
                aec.createGameFromString(TestExpressionsFactory.GAME_EXPRESION)
        );
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testInvalidExpressionCreation() {
        assertNotNull(aec.createGameFromString("BAR FOO > 5 : 4"));
    }

    /**
     * Empty expression is not valid expression
     */
    @Test(expectedExceptions = IllegalArgumentException.class)
    public void testEmptyExpressionCreation() {
        assertNotNull(aec.createGameFromString(""));
    }
}
