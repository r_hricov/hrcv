package sk.hrcv.wbbe.service.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;
import sk.hrcv.test.config.AbstractTransactionalSpringBootTestWithData;
import sk.hrcv.test.config.TestGroup;
import sk.hrcv.wbbe.dto.CreateScoreRequest;
import sk.hrcv.wbbe.dto.type.Score;
import sk.hrcv.wbbe.repository.ScoreRepository;
import sk.hrcv.wbbe.repository.entity.ScoreEntity;
import sk.hrcv.wbbe.service.ScoreService;

import java.util.Collections;
import java.util.List;

@Test(groups = TestGroup.UNIT)
public class ScoreServiceTest extends AbstractTransactionalSpringBootTestWithData {

    @Autowired
    private ScoreService scoreService;
    @Autowired
    private ScoreRepository scoreRepository;

    public void createScoreTest() {
        long gameId = 11L;
        //default score is 0:0
        Score score = new Score();

        CreateScoreRequest req = new CreateScoreRequest(gameId, score);
        scoreService.createScore(req);

        List<ScoreEntity> scores = scoreRepository.findByGame_GameIdIn(Collections.singletonList(gameId));
        Assert.assertFalse(scores.isEmpty());
        Assert.assertEquals(scores.size(), 1);
    }

}
