package sk.hrcv.wbbe.service.test.simulator

import org.springframework.beans.factory.annotation.Autowired
import org.testng.Assert
import org.testng.annotations.Test
import sk.hrcv.test.config.AbstractTransactionalSpringBootTestWithData
import sk.hrcv.test.config.TestGroup
import sk.hrcv.wbbe.dto.type.Game
import sk.hrcv.wbbe.dto.type.Team
import sk.hrcv.wbbe.service.FoosballGameSimulator

@Test(groups = [TestGroup.UNIT])
class FoosballGameSimulatorTest : AbstractTransactionalSpringBootTestWithData() {

    @Autowired
    private lateinit var foosballGameSimulator: FoosballGameSimulator

    fun `Foosball simulator test`() {
        // prepare test data
        Game(
            teamA = Team(teamName = "alice"),
            teamB = Team(teamName = "boob")
        ).run {
            // call tested method
            foosballGameSimulator.playFoosball(this)
        }.also {
            // check assertion on result of tested method
            Assert.assertNotNull(it.score)
        }
    }

    fun testThreads() {
        val t1 = Thread {
            for (i in 0..9) {
                println("$i ")
                try {
                    Thread.sleep(100)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }
            }
        }
        val t2 = Thread {
            for (i in 100..109) {
                println("$i ")
                try {
                    Thread.sleep(100)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }
            }
        }
        t1.start()
        t2.start()
    }
}