package sk.hrcv.wbbe.service.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;
import sk.hrcv.test.config.AbstractTransactionalSpringBootTestWithData;
import sk.hrcv.test.config.TestGroup;
import sk.hrcv.wbbe.dto.ImportGameRequest;
import sk.hrcv.wbbe.service.ImportGamesService;
import sk.hrcv.wbbe.service.test.importtool.TestExpressionsFactory;

import java.util.Arrays;
import java.util.List;

import static org.testng.Assert.assertEquals;

@Test(groups = TestGroup.UNIT)
public class ImportGamesTest extends AbstractTransactionalSpringBootTestWithData {

    private static final List<String> GAMES_TO_IMPORT = Arrays.asList(
            TestExpressionsFactory.GAME_EXPRESION,
            "BARvs.FOO>5:4",
            "BAR  vs.  FOO  >  5  :  4",
            "BAR vs. FOO " +
                    "> 5 : 4",
            "BAR vs. FOO > 5 : 4",
            //bla and alb do not exist in db so game can not be stored
            "BLA vs. ALB > 5 : 4",
            //teamA must be different from teamB
            "BLA vs. BLA > 5 : 4",
            //does not fulfill format for importing games
            "something stupid",
            //does not fulfill format for importing games
            "BAR : FOO  5 : 4"
    );

    @Autowired
    private ImportGamesService testedService;

    public void sunnyDayTest() {
        ImportGameRequest importGameRequest = new ImportGameRequest(GAMES_TO_IMPORT);

        List<String> gamesNotImported = testedService.importGames(importGameRequest);
        assertEquals(gamesNotImported.size(), 4);
    }

}
