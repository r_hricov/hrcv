package sk.hrcv.wbbe.service.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;
import sk.hrcv.common.constant.ErrorCode;
import sk.hrcv.config.exception.CommonBusinessException;
import sk.hrcv.test.config.AbstractTransactionalSpringBootTestWithData;
import sk.hrcv.test.config.TestGroup;
import sk.hrcv.wbbe.dto.CreatePlayerRequest;
import sk.hrcv.wbbe.dto.type.Player;
import sk.hrcv.wbbe.repository.PlayerRepository;
import sk.hrcv.wbbe.repository.entity.PlayerEntity;
import sk.hrcv.wbbe.service.PlayerService;

import static org.testng.Assert.*;

@Test(groups = TestGroup.UNIT)
public class PlayerServiceTest extends AbstractTransactionalSpringBootTestWithData {

    private static final String DUMMY_NICKNAME = "DummyNickname";
    private static final String EXISTING_PLAYER_NICKNAME = "JohnDoe";
    private static final Long TESTED_PLAYER_ID = 0L;

    @Autowired
    private PlayerService playerService;
    @Autowired
    private PlayerRepository playerRepository;

    public void createPlayerTest() {
        CreatePlayerRequest createPlayerRequest = new CreatePlayerRequest(DUMMY_NICKNAME);

        Player newPlayer = playerService.createNewPlayer(createPlayerRequest);
        assertNotNull(newPlayer.getPlayerId());
        assertNotNull(playerService.getPlayerInfo(newPlayer.getPlayerId()));
    }

    @Test(expectedExceptions = CommonBusinessException.class, expectedExceptionsMessageRegExp = ".*" + ErrorCode.PLAYER_ALREADY_EXISTS + ".*")
    public void createPlayerTestWithExistingNickname() {
        CreatePlayerRequest createPlayerRequest = new CreatePlayerRequest(EXISTING_PLAYER_NICKNAME);

        playerService.createNewPlayer(createPlayerRequest);
    }

    /**
     * Initial data are already in DB so {@link sk.hrcv.wbbe.service.impl.PlayerServiceImpl#getAllPlayersInfo getAllPlayersInfo} should not return empty list
     */
    public void getAllPlayersTest() {
        assertFalse(playerService.getAllPlayersInfo().isEmpty());
    }

    /**
     * For player with id=0 some records from PLAYER, GAME and TEAM tables are deleted
     */
    public void deletePlayerTest() {
        playerService.deletePlayer(TESTED_PLAYER_ID);

        PlayerEntity nullPlayer = playerRepository.findByPlayerId(TESTED_PLAYER_ID);
        Assert.assertNull(nullPlayer);
    }

    public void updatePlayerTest() {
        Player player = playerService.getPlayerInfo(TESTED_PLAYER_ID);
        assertNotEquals(player.getNickname(), DUMMY_NICKNAME);

        playerService.updatePlayer(TESTED_PLAYER_ID, DUMMY_NICKNAME);

        Player updatedPlayer = playerService.getPlayerInfo(TESTED_PLAYER_ID);
        assertEquals(updatedPlayer.getNickname(), DUMMY_NICKNAME);
    }
}