package sk.hrcv.wbbe.service.test.importtool;

import sk.hrcv.wbbe.service.importtool.expression.*;

public final class TestExpressionsFactory {

    private TestExpressionsFactory() {
        throw new UnsupportedOperationException();
    }

    public static final String GAME_EXPRESION = "BAR vs. FOO > 5 : 4";

    /**
     * Creates: BAR vs. FOO > 5 : 4
     */
    public static Expression createExpression1() {
        Expression tOperand1 = new TeamOperand("BAR");
        Expression tOperand2 = new TeamOperand("FOO");

        Expression sOperand1 = new ScoreOperand(5L);
        Expression sOperand2 = new ScoreOperand(4L);

        Expression tOperator = new TeamOperator(tOperand1, tOperand2);
        Expression sOperator = new ScoreOperator(sOperand1, sOperand2);

        return new GameOperator(tOperator, sOperator);
    }
}
