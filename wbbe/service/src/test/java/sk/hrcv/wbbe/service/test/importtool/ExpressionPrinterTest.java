package sk.hrcv.wbbe.service.test.importtool;

import org.testng.annotations.Test;
import sk.hrcv.wbbe.service.importtool.ExpressionPrinter;
import sk.hrcv.wbbe.service.importtool.expression.Expression;

import static org.testng.Assert.assertEquals;

public class ExpressionPrinterTest {

    @Test
    public void testInOrderPrintingOfExpression1() {
        Expression expression = TestExpressionsFactory.createExpression1();
        ExpressionPrinter printer = new ExpressionPrinter(expression);

        assertEquals(TestExpressionsFactory.GAME_EXPRESION, printer.printInOrder());
    }
}
