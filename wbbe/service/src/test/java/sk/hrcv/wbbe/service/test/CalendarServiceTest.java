package sk.hrcv.wbbe.service.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;
import sk.hrcv.test.config.AbstractTransactionalSpringBootTestWithData;
import sk.hrcv.test.config.TestGroup;
import sk.hrcv.wbbe.service.CalendarService;

import java.util.Arrays;
import java.util.List;

@Test(groups = TestGroup.UNIT)
public class CalendarServiceTest extends AbstractTransactionalSpringBootTestWithData {

    //Sample input
    private static final List<List<String>> FIRST_UNAVAILABILITY_CALENDAR = Arrays.asList(
            Arrays.asList("9:00", "10:30"),
            Arrays.asList("12:00", "13:00"),
            Arrays.asList("16:00", "18:00")
    );
    private static final List<String> FIRST_OVERALL_AVAILABILITY = Arrays.asList("9:00", "20:00");

    private static final List<List<String>> SECOND_UNAVAILABILITY_CALENDAR = Arrays.asList(
            Arrays.asList("10:00", "11:30"),
            Arrays.asList("12:30", "14:30"),
            Arrays.asList("14:30", "15:00"),
            Arrays.asList("16:00", "17:00")
    );
    private static final List<String> SECOND_OVERALL_AVAILABILITY = Arrays.asList("10:00", "18:30");

    //Sample output
    private static final List<List<String>> EXPECTED_OUTPUT = Arrays.asList(
            Arrays.asList("11:30", "12:00"),
            Arrays.asList("15:00", "16:00"),
            Arrays.asList("18:00", "18:30")
    );

    @Autowired
    private CalendarService calendarService;

    public void mergeCalendarsTest() {
        List<List<String>> freeSlots = calendarService.computeFreeSlots(
                FIRST_UNAVAILABILITY_CALENDAR, FIRST_OVERALL_AVAILABILITY,
                SECOND_UNAVAILABILITY_CALENDAR, SECOND_OVERALL_AVAILABILITY);

        Assert.assertEquals(freeSlots, EXPECTED_OUTPUT);
    }
}
