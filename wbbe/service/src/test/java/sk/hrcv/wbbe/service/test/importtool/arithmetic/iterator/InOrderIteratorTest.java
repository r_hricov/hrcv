package sk.hrcv.wbbe.service.test.importtool.arithmetic.iterator;

import org.testng.annotations.Test;
import sk.hrcv.test.config.TestGroup;
import sk.hrcv.wbbe.service.importtool.expression.Expression;
import sk.hrcv.wbbe.service.importtool.expression.elements.ExpressionElement;
import sk.hrcv.wbbe.service.test.importtool.TestExpressionsFactory;

import java.util.Iterator;

import static org.testng.Assert.*;

@Test(groups = TestGroup.UNIT)
public class InOrderIteratorTest {

    public void testIteration() {
        Expression e = TestExpressionsFactory.createExpression1();
        Iterator<ExpressionElement> it = e.getInOrderIterator();
        assertNotNull(it);

        assertEquals("BAR", it.next().stringValue());
        assertEquals("vs.", it.next().stringValue());
        assertEquals("FOO", it.next().stringValue());
        assertEquals(">", it.next().stringValue());
        assertEquals("5", it.next().stringValue());
        assertEquals(":", it.next().stringValue());
        assertEquals("4", it.next().stringValue());
        assertFalse(it.hasNext());
    }

    @Test(expectedExceptions = UnsupportedOperationException.class)
    public void testRemoveOperationUnsupported() {
        Expression e = TestExpressionsFactory.createExpression1();
        Iterator<ExpressionElement> it = e.getInOrderIterator();
        assertNotNull(it);
        it.remove();
    }
}