package sk.hrcv.wbbe.service.test;

import mockit.Deencapsulation;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import sk.hrcv.test.config.AbstractTransactionalSpringBootTestWithData;
import sk.hrcv.test.config.TestGroup;
import sk.hrcv.wbbe.dto.CreateGameRequest;
import sk.hrcv.wbbe.dto.type.Game;
import sk.hrcv.wbbe.dto.type.Team;
import sk.hrcv.wbbe.repository.entity.PlayerEntity;
import sk.hrcv.wbbe.service.GameService;

import java.util.List;

@Test(groups = TestGroup.UNIT)
public class GameServiceTest extends AbstractTransactionalSpringBootTestWithData {

    @Autowired
    private GameService gameService;

    public void createGameTest() {
        CreateGameRequest createGameRequest = new CreateGameRequest();
        createGameRequest.setTeamAId(1L);
        createGameRequest.setTeamBId(2L);

        Game newGame = gameService.createNewGame(createGameRequest);
        Assert.assertNotNull(newGame.getGameId());
        Assert.assertNotNull(gameService.getGameInfo(newGame.getGameId()));
    }

    public void getAllGamesTest() {
        Assert.assertFalse(gameService.getAllGamesInfo(true, true).isEmpty());
    }

    @Test(dataProvider = "getAllGamesByTeam")
    public void gatAllGamesByTeam(long desiredTeamId, boolean played, boolean proposed, long expectedGamesCount) {
        List<Game> allGamesInfoForTeam = gameService.getAllGamesInfoForTeam(desiredTeamId, played, proposed);

        Assert.assertEquals(allGamesInfoForTeam.size(), expectedGamesCount);

        allGamesInfoForTeam.forEach(game ->
                Assert.assertTrue(desiredTeamId == game.getTeamA().getTeamId() || desiredTeamId == game.getTeamB().getTeamId())
        );
    }

    @DataProvider(name = "getAllGamesByTeam")
    private Object[][] getAllGamesByTeamData() {
        return new Object[][]{
                //{desired team id, include played games, include proposed games, expected games count}
                {0L, true, true, 12L},
                {0L, true, false, 8L},
                {0L, false, true, 4L},
        };
    }

    @Test(dataProvider = "getAllGamesByPlayerData")
    public void getAllGamesByPlayer(long desiredPlayerId, boolean played, boolean proposed, long expectedGamesCount) {
        List<Game> allGamesInfoForTeam = gameService.getAllGamesInfoForPlayer(desiredPlayerId, played, proposed);

        Assert.assertEquals(allGamesInfoForTeam.size(), expectedGamesCount);

        allGamesInfoForTeam.forEach(game -> {
                    Team teamA = game.getTeamA();
                    Team teamB = game.getTeamB();
                    Assert.assertTrue(teamContainsPlayer(desiredPlayerId, teamA) || teamContainsPlayer(desiredPlayerId, teamB));
                }
        );
    }

    //It looks same as getAllGamesByTeamData since player with id 0 is member of team with id 0
    @DataProvider(name = "getAllGamesByPlayerData")
    private Object[][] getAllGamesByPlayerData() {
        return new Object[][]{
                //{desired player id, include played games, include proposed games, expected games count}
                {0L, true, true, 12L},
                {0L, true, false, 8L},
                {0L, false, true, 4L},
        };
    }

    private boolean teamContainsPlayer(Long playerId, Team team) {
        return playerId.equals(team.getPlayerOne().getPlayerId()) || (team.getPlayerTwo() != null && playerId.equals(team.getPlayerTwo().getPlayerId()));
    }

    /**
     * Use deencapsulation it only for mocking classes that have no public constructor and fields setters.
     * It could be used e.g. to prepare object to be returned from mocked method.
     */
    public void testDeencapsulation() {
        PlayerEntity player = Deencapsulation.newUninitializedInstance(PlayerEntity.class);
        Deencapsulation.setField(player, "playerId", 1L);
        Deencapsulation.setField(player, "nickname", "playerMockedNickname");

        Assert.assertEquals((long) player.getPlayerId(), 1L);
        Assert.assertEquals(player.getNickname(), "playerMockedNickname");
    }

}
