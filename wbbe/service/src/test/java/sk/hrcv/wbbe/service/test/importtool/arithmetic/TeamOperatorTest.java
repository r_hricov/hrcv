package sk.hrcv.wbbe.service.test.importtool.arithmetic;

import org.testng.annotations.Test;
import sk.hrcv.test.config.TestGroup;
import sk.hrcv.wbbe.service.importtool.expression.ScoreOperand;
import sk.hrcv.wbbe.service.importtool.expression.TeamOperator;

import static org.testng.Assert.assertEquals;

@Test(groups = TestGroup.UNIT)
public class TeamOperatorTest {

    private final ScoreOperand o1 = new ScoreOperand(1L);
    private final ScoreOperand o2 = new ScoreOperand(2L);

    public void testGetFirstOperand() {
        TeamOperator o = new TeamOperator(o1, o2);
        assertEquals(o1, o.getFirst());
    }

    public void testGetSecondOperand() {
        TeamOperator o = new TeamOperator(o1, o2);
        assertEquals(o2, o.getSecond());
    }
}
