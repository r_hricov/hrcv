package sk.hrcv.wbbe.service.test;

import mockit.Mock;
import mockit.MockUp;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import sk.hrcv.test.config.AbstractTransactionalSpringBootTestWithData;
import sk.hrcv.test.config.TestGroup;
import sk.hrcv.wbbe.dto.type.TableRecord;
import sk.hrcv.wbbe.service.TableService;
import sk.hrcv.wbbe.service.properties.TableServiceProperties;

import java.util.List;

@Test(groups = TestGroup.UNIT)
public class TableServiceTest extends AbstractTransactionalSpringBootTestWithData {

    @Autowired
    private TableService tableService;

    @BeforeTest
    private void setPointsMultiplier() {
        new MockUp<TableServiceProperties>() {
            @Mock
            public Long getWonGamesMultiplier() {
                return 3L;
            }
        };

        new MockUp<TableServiceProperties>() {
            @Mock
            public Long getDrawnGamesMultiplier() {
                return 1L;
            }
        };
    }

    public void testTableService() {
        List<TableRecord> tableRecords = tableService.getTableRecords();

        Assert.assertFalse(tableRecords.isEmpty());
        Assert.assertEquals(tableRecords.size(), 3);

        TableRecord firstTeamRecord = tableRecords.get(0);
        TableRecord secondTeamRecord = tableRecords.get(1);
        TableRecord thirdTeamRecord = tableRecords.get(2);

        //chceck that records are sorted according score
        Assert.assertTrue(firstTeamRecord.getPoints() > secondTeamRecord.getPoints() && secondTeamRecord.getPoints() > thirdTeamRecord.getPoints());

        Assert.assertEquals(firstTeamRecord.getTeam().getTeamId(), 2);
        Assert.assertEquals(secondTeamRecord.getTeam().getTeamId(), 0);
        Assert.assertEquals(thirdTeamRecord.getTeam().getTeamId(), 1);

        //check played games
        Assert.assertEquals(firstTeamRecord.getPlayedGamesCount(), 6L);
        Assert.assertEquals(secondTeamRecord.getPlayedGamesCount(), 6L);
        Assert.assertEquals(thirdTeamRecord.getPlayedGamesCount(), 4L);

        Assert.assertEquals(firstTeamRecord.getWonGamesCount(), 4L);
        Assert.assertEquals(secondTeamRecord.getWonGamesCount(), 2L);
        Assert.assertEquals(thirdTeamRecord.getWonGamesCount(), 1L);

        Assert.assertEquals(firstTeamRecord.getDrawnGamesCount(), 1L);
        Assert.assertEquals(secondTeamRecord.getDrawnGamesCount(), 1L);
        Assert.assertEquals(thirdTeamRecord.getDrawnGamesCount(), 0L);

        Assert.assertEquals(firstTeamRecord.getLostGamesCount(), 1L);
        Assert.assertEquals(secondTeamRecord.getLostGamesCount(), 3L);
        Assert.assertEquals(thirdTeamRecord.getLostGamesCount(), 3L);

        Assert.assertEquals(firstTeamRecord.getPoints(), 13L);
        Assert.assertEquals(secondTeamRecord.getPoints(), 7L);
        Assert.assertEquals(thirdTeamRecord.getPoints(), 3L);
    }

}
