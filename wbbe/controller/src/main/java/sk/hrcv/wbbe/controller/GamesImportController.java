package sk.hrcv.wbbe.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import sk.hrcv.wbbe.controller.constant.Url;
import sk.hrcv.wbbe.dto.ImportGameRequest;
import sk.hrcv.wbbe.dto.ImportGameResponse;
import sk.hrcv.wbbe.service.ImportGamesService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(path = Url.GAMES_URL, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
@Transactional
public class GamesImportController {

    private final ImportGamesService importGamesService;

    @Autowired
    public GamesImportController(ImportGamesService importGamesService) {
        this.importGamesService = importGamesService;
    }

    @PostMapping(path = Url.IMPORT)
    @ResponseStatus(HttpStatus.CREATED)
    @Valid
    public ImportGameResponse importGames(@Valid @RequestBody ImportGameRequest request) {
        List<String> gamesNotImported = importGamesService.importGames(request);

        return new ImportGameResponse(gamesNotImported);
    }
}
