package sk.hrcv.wbbe.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import sk.hrcv.wbbe.controller.constant.Url;
import sk.hrcv.wbbe.dto.CreateTeamRequest;
import sk.hrcv.wbbe.dto.type.Team;
import sk.hrcv.wbbe.service.TeamService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(path = Url.TEAMS_URL, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
@Transactional
public class TeamController {

    private final TeamService teamService;

    @Autowired
    public TeamController(TeamService teamService) {
        this.teamService = teamService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Valid
    public Team createNewTeam(@Valid @RequestBody CreateTeamRequest request) {
        return teamService.createNewTeam(request);
    }

    @GetMapping(path = Url.BY_TEAM_ID)
    @ResponseStatus(HttpStatus.OK)
    @Valid
    public Team getTeamInfo(@PathVariable Long teamId) {
        return teamService.getTeamInfo(teamId);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    @Valid
    public List<Team> getAllTeamsInfo() {
        return teamService.getAllTeamsInfo();
    }

    @DeleteMapping(path = Url.BY_TEAM_ID)
    @ResponseStatus(HttpStatus.OK)
    public void deleteTeam(@PathVariable Long teamId) {
        teamService.deleteTeam(teamId);
    }
}