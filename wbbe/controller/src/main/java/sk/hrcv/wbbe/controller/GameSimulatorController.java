package sk.hrcv.wbbe.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import sk.hrcv.wbbe.controller.constant.Url;
import sk.hrcv.wbbe.dto.type.Game;
import sk.hrcv.wbbe.service.FoosballGameSimulator;
import sk.hrcv.wbbe.service.GameService;
import sk.hrcv.wbbe.service.simulator.ScoreObj;

import javax.validation.Valid;

@RestController
@RequestMapping(path = Url.GAMES_URL, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
@Transactional
public class GameSimulatorController {

    private final FoosballGameSimulator simulator;
    private final GameService gameService;

    @Autowired
    public GameSimulatorController(FoosballGameSimulator simulator, GameService gameService) {
        this.simulator = simulator;
        this.gameService = gameService;
    }

    @GetMapping(path = Url.SIMULATOR_FOR_GAME)
    @ResponseStatus(HttpStatus.OK)
    @Valid
    public ScoreObj simulateMatch(@PathVariable Long gameId) {
        Game match = gameService.getGameInfo(gameId);
        if (match.getDate() != null) {
            throw new IllegalStateException("Foosball match has been already played");
        }

        return simulator.playFoosball(match);
    }
}