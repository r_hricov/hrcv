package sk.hrcv.wbbe.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import sk.hrcv.wbbe.controller.constant.Url;
import sk.hrcv.wbbe.dto.TableResponse;
import sk.hrcv.wbbe.service.TableService;

import javax.validation.Valid;

@RestController
@RequestMapping(path = Url.TABLE_URL, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
@Transactional
public class TableController {

    private final TableService tableService;

    @Autowired
    public TableController(TableService tableService) {
        this.tableService = tableService;
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    @Valid
    public TableResponse getTable() {
        return new TableResponse(tableService.getTableRecords());
    }
}
