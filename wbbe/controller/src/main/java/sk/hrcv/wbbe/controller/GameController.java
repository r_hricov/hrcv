package sk.hrcv.wbbe.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import sk.hrcv.wbbe.controller.constant.Url;
import sk.hrcv.wbbe.dto.CreateGameRequest;
import sk.hrcv.wbbe.dto.PlannerRequest;
import sk.hrcv.wbbe.dto.type.Game;
import sk.hrcv.wbbe.dto.type.GameFilter;
import sk.hrcv.wbbe.service.CalendarService;
import sk.hrcv.wbbe.service.GameService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(path = Url.GAMES_URL, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
@Transactional
public class GameController {

    private final GameService gameService;
    private final CalendarService calendarService;

    @Autowired
    public GameController(GameService gameService, CalendarService calendarService) {
        this.gameService = gameService;
        this.calendarService = calendarService;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    @Valid
    public Game createNewGame(@Valid @RequestBody CreateGameRequest request) {
        return gameService.createNewGame(request);
    }

    @GetMapping(path = Url.BY_GAME_ID)
    @ResponseStatus(HttpStatus.OK)
    @Valid
    public Game getGameInfo(@PathVariable Long gameId) {
        return gameService.getGameInfo(gameId);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    @Valid
    public List<Game> getAllGamesInfo() {
        return gameService.getAllGamesInfo(true, true);
    }

    @GetMapping(Url.BY_TEAM)
    @ResponseStatus(HttpStatus.OK)
    @Valid
    public List<Game> getAllGamesInfoForTeam(@PathVariable Long teamId, @Valid @RequestBody GameFilter gameFilter) {
        return gameService.getAllGamesInfoForTeam(teamId, gameFilter.getPlayedGames(), gameFilter.getProposedGames());
    }

    @GetMapping(Url.BY_PLAYER)
    @ResponseStatus(HttpStatus.OK)
    @Valid
    public List<Game> getAllGamesInfoForPlayer(@PathVariable Long playerId, @Valid @RequestBody GameFilter gameFilter) {
        return gameService.getAllGamesInfoForPlayer(playerId, gameFilter.getPlayedGames(), gameFilter.getProposedGames());
    }

    @GetMapping(Url.PLANNER)
    @ResponseStatus(HttpStatus.OK)
    @Valid
    public List<List<String>> getFreeCalendarSlots(@Valid @RequestBody PlannerRequest request) {
        return calendarService.computeFreeSlots(request.getFirstUnavailabilityCalendar(), request.getFirstOverallAvailability(),
                request.getSecondUnavailabilityCalendar(), request.getSecondOverallAvailability());
    }
}