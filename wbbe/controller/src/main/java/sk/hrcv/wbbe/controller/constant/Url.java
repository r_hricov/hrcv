package sk.hrcv.wbbe.controller.constant;

import sk.hrcv.common.constant.UrlBase;

public class Url {
    private static final String GAMES = "/games";
    private static final String PLAYERS = "/players";
    private static final String TEAMS = "/teams";
    private static final String TABLE = "/table";

    private static final String BY_ID = "/%s";
    public static final String BY_GAME_ID = "/{gameId}";
    public static final String BY_TEAM_ID = "/{teamId}";
    public static final String BY_PLAYER_ID = "/{playerId}";

    private static final String TEAM = "/team";
    private static final String TEAM_BY_ID = TEAM + BY_ID;
    public static final String BY_TEAM = TEAM + BY_TEAM_ID;

    private static final String PLAYER = "/player";
    private static final String PLAYER_BY_ID = PLAYER + BY_ID;
    public static final String BY_PLAYER = PLAYER + BY_PLAYER_ID;

    public static final String PLANNER = "/planner";

    public static final String IMPORT = "/import";

    private static final String SIMULATOR = "/simulator";
    private static final String SIMULATOR_BY_ID = SIMULATOR + BY_ID;
    public static final String SIMULATOR_FOR_GAME = SIMULATOR + BY_GAME_ID;

    public static final String GAMES_URL = UrlBase.API_V1 + GAMES;
    public static final String GAMES_URL_BY_ID = GAMES_URL + BY_ID;
    public static final String GAMES_URL_BY_TEAM = GAMES_URL + TEAM_BY_ID;
    public static final String GAMES_URL_BY_PLAYER = GAMES_URL + PLAYER_BY_ID;
    public static final String GAMES_URL_PLANNER = GAMES_URL + PLANNER;
    public static final String GAMES_URL_IMPORT = GAMES_URL + IMPORT;
    public static final String GAMES_URL_SIMULATOR = GAMES_URL + SIMULATOR_BY_ID;

    public static final String PLAYERS_URL = UrlBase.API_V1 + PLAYERS;
    public static final String PLAYERS_URL_BY_ID = PLAYERS_URL + BY_ID;

    public static final String TEAMS_URL = UrlBase.API_V1 + TEAMS;
    public static final String TEAMS_URL_BY_ID = TEAMS_URL + BY_ID;

    public static final String TABLE_URL = UrlBase.API_V1 + TABLE;

    public static String resolve(String s, Long a) {
        return String.format(s, a);
    }
}
