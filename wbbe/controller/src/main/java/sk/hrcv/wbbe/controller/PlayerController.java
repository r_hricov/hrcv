package sk.hrcv.wbbe.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import sk.hrcv.wbbe.controller.constant.Url;
import sk.hrcv.wbbe.dto.CreatePlayerRequest;
import sk.hrcv.wbbe.dto.type.Player;
import sk.hrcv.wbbe.service.PlayerService;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping(path = Url.PLAYERS_URL, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
@Transactional
public class PlayerController {

    private final PlayerService playerService;

    @Autowired
    public PlayerController(PlayerService playerService) {
        this.playerService = playerService;
    }

    @PostMapping
    @ResponseStatus(value = HttpStatus.CREATED)
    @Valid
    public Player createNewPlayer(@Valid @RequestBody CreatePlayerRequest request) {
        return playerService.createNewPlayer(request);
    }

    @GetMapping(path = Url.BY_PLAYER_ID)
    @ResponseStatus(HttpStatus.OK)
    @Valid
    public Player getPlayerInfo(@PathVariable Long playerId) {
        return playerService.getPlayerInfo(playerId);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    @Valid
    public List<Player> getAllPlayersInfo() {
        return playerService.getAllPlayersInfo();
    }

    @DeleteMapping(path = Url.BY_PLAYER_ID)
    @ResponseStatus(HttpStatus.OK)
    public void deletePlayer(@PathVariable Long playerId) {
        playerService.deletePlayer(playerId);
    }

    @PutMapping(path = Url.BY_PLAYER_ID)
    @ResponseStatus(HttpStatus.OK)
    public void updatePlayer(@PathVariable Long playerId, @Valid @RequestBody CreatePlayerRequest request) {
        playerService.updatePlayer(playerId, request.getNickname());
    }
}
