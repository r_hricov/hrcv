package sk.hrcv.wbbe.controller.test.playerControllerTests;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import sk.hrcv.common.util.TestUtils;
import sk.hrcv.common.util.TestUtils.TestRequestResponse;
import sk.hrcv.test.config.AbstractTransactionalSpringBootTestWithData;
import sk.hrcv.test.config.TestGroup;
import sk.hrcv.wbbe.controller.constant.Url;

@Test(groups = TestGroup.UNIT)
// Just create database and do not load data
@Sql({"classpath:authTEST.sql", "classpath:/schemaTEST.sql"})
public class CreatePlayerTest extends AbstractTransactionalSpringBootTestWithData {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Value("classpath:player/createPlayer/request.json")
    private Resource request;
    @Value("classpath:player/createPlayer/response.json")
    private Resource expectedResponse;

    @BeforeClass
    private void initTest() {
        super.initTest(webApplicationContext);
    }

    public void testCreatePlayer() throws Exception {
        TestRequestResponse testRequestResponse = TestUtils.prepareTestRequestResponse(request, expectedResponse);

        post(Url.PLAYERS_URL, testRequestResponse.getRequest())
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.content().json(testRequestResponse.getResponse()))
                .andReturn();
    }

}