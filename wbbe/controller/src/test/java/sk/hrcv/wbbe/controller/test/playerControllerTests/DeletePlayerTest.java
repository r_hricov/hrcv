package sk.hrcv.wbbe.controller.test.playerControllerTests;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import sk.hrcv.test.config.AbstractTransactionalSpringBootTestWithData;
import sk.hrcv.test.config.TestGroup;
import sk.hrcv.wbbe.controller.constant.Url;

import static sk.hrcv.wbbe.controller.constant.Url.PLAYERS_URL_BY_ID;

@Test(groups = {TestGroup.UNIT, TestGroup.INTEGRATION})
public class DeletePlayerTest extends AbstractTransactionalSpringBootTestWithData {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @BeforeClass
    private void initTest() {
        super.initTest(webApplicationContext);
    }

    public void testDeletePlayer() throws Exception {

        delete(Url.resolve(PLAYERS_URL_BY_ID, 0L))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andReturn();
    }
}