package sk.hrcv.wbbe.controller.test.gameContorllerTests;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import sk.hrcv.common.util.TestUtils;
import sk.hrcv.test.config.AbstractTransactionalSpringBootTestWithData;
import sk.hrcv.test.config.TestGroup;
import sk.hrcv.wbbe.controller.constant.Url;

@Test(groups = {TestGroup.UNIT, TestGroup.INTEGRATION})
public class CreateGameTest extends AbstractTransactionalSpringBootTestWithData {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Value("classpath:game/createGame/request.json")
    private Resource request;
    @Value("classpath:game/createGame/response.json")
    private Resource expectedResponse;

    @BeforeClass
    private void init() {
        super.initTest(webApplicationContext);
    }

    public void testCreateGame() throws Exception {
        TestUtils.TestRequestResponse testRequestResponse = TestUtils.prepareTestRequestResponse(request, expectedResponse);

        post(Url.GAMES_URL, testRequestResponse.getRequest())
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.content().json(testRequestResponse.getResponse()))
                .andReturn();
    }
}