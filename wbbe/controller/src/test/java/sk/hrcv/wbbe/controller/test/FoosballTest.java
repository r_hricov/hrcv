package sk.hrcv.wbbe.controller.test;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.jdbc.Sql;
import org.testng.Assert;
import org.testng.annotations.Test;
import sk.hrcv.test.config.AbstractTransactionalSpringBootTestWithData;
import sk.hrcv.test.config.TestGroup;
import sk.hrcv.wbbe.controller.GameController;
import sk.hrcv.wbbe.controller.PlayerController;
import sk.hrcv.wbbe.controller.TeamController;
import sk.hrcv.wbbe.dto.CreateGameRequest;
import sk.hrcv.wbbe.dto.CreatePlayerRequest;
import sk.hrcv.wbbe.dto.CreateTeamRequest;
import sk.hrcv.wbbe.dto.type.Player;
import sk.hrcv.wbbe.dto.type.Score;
import sk.hrcv.wbbe.dto.type.Team;

@Test(groups = TestGroup.UNIT)
// Just create database and do not load data
@Sql({"classpath:authTEST.sql", "classpath:/schemaTEST.sql"})
public class FoosballTest extends AbstractTransactionalSpringBootTestWithData {

    private static final Logger log = LoggerFactory.getLogger(FoosballTest.class);

    @Autowired
    private GameController gameController;
    @Autowired
    private PlayerController playerController;
    @Autowired
    private TeamController teamController;

    /**
     * Integration test that creates players. Then the teams are created from players and
     * finally the game is created. Within the test some entities are deleted from DB and every
     * action is checked by assertions.
     */
    public void foosballTest() {
        Player ronaldo = playerController.createNewPlayer(getCreatePlayerRequest("Ronaldo"));
        Player modric = playerController.createNewPlayer(getCreatePlayerRequest("Modric"));
        Player mbappe = playerController.createNewPlayer(getCreatePlayerRequest("Mbappé"));
        Player messi = playerController.createNewPlayer(getCreatePlayerRequest("Messi"));

        Assert.assertEquals(playerController.getAllPlayersInfo().size(), 4);
        playerController.deletePlayer(messi.getPlayerId());
        //messi does not exist anymore
        Assert.assertEquals(playerController.getAllPlayersInfo().size(), 3);

        Team blueTeam = teamController.createNewTeam(getCreateTeamRequest("Blue", "BL", ronaldo, modric));
        Team redTeam = teamController.createNewTeam(getCreateTeamRequest("Red", "rd", mbappe, null));
        Team teamToDelete = teamController.createNewTeam(getCreateTeamRequest("Black", "BLK", ronaldo, null));

        Assert.assertEquals(teamController.getAllTeamsInfo().size(), 3);
        Assert.assertEquals(teamController.getTeamInfo(teamToDelete.getTeamId()).getTeamName(), teamToDelete.getTeamName());
        teamController.deleteTeam(teamToDelete.getTeamId());
        Assert.assertEquals(teamController.getAllTeamsInfo().size(), 2);

        gameController.createNewGame(getCreateGameRequest(blueTeam.getTeamId(), redTeam.getTeamId()));
        gameController.createNewGame(getCreateGameRequest(redTeam.getTeamId(), blueTeam.getTeamId()));

        Assert.assertEquals(gameController.getAllGamesInfo().size(), 2);

        playerController.deletePlayer(mbappe.getPlayerId());
        //one player was deleted and all his games and team too. Two players left
        Assert.assertEquals(playerController.getAllPlayersInfo().size(), 2);
        //all games was deleted
        Assert.assertEquals(gameController.getAllGamesInfo().size(), 0);
        //one team was deleted, one left
        Assert.assertEquals(teamController.getAllTeamsInfo().size(), 1);
    }

    private CreatePlayerRequest getCreatePlayerRequest(String nickname) {
        return new CreatePlayerRequest(nickname);
    }

    private CreateTeamRequest getCreateTeamRequest(String teamName, String teamNameAbbr, Player playerOne, Player playerTwo) {
        CreateTeamRequest request = new CreateTeamRequest();
        request.setTeamName(teamName);
        request.setTeamNameAbbr(teamNameAbbr);
        request.setPlayerOneId(playerOne.getPlayerId());

        if (playerTwo != null) {
            request.setPlayerTwoId(playerTwo.getPlayerId());
        }

        return request;
    }

    private CreateGameRequest getCreateGameRequest(Long teamAId, Long teamBId) {
        CreateGameRequest request = new CreateGameRequest();
        request.setTeamAId(teamAId);
        request.setTeamBId(teamBId);

        //default score is 0:0
        Score score = new Score();
        request.setScore(score);

        return request;
    }
}