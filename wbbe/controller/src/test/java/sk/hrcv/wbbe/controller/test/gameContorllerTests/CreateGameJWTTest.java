package sk.hrcv.wbbe.controller.test.gameContorllerTests;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.testng.annotations.Test;
import sk.hrcv.common.util.TestUtils;
import sk.hrcv.config.constant.HeaderParameters;
import sk.hrcv.test.config.TestConfig;
import sk.hrcv.test.config.TestGroup;
import sk.hrcv.wbbe.controller.constant.Url;

import java.util.Base64;

/**
 * Just within this test the web security is tested
 */
@SpringBootTest(classes = {TestConfig.class})
//AutoConfigureMockMvc enable to test security configuration but it affecting WebApplicationContext for other tests
//that are not prepared for authentication
@AutoConfigureMockMvc
@Sql({"classpath:authTEST.sql", "classpath:/schemaTEST.sql", "classpath:/dataTEST.sql"})
@Test(groups = {TestGroup.INTEGRATION})
public class CreateGameJWTTest extends AbstractTransactionalTestNGSpringContextTests {

    @Value("classpath:game/createGame/request.json")
    private Resource request;
    @Value("classpath:game/createGame/response.json")
    private Resource expectedResponse;

    @Autowired
    private MockMvc mockMvc;

    @TestConfiguration
    public class AdditionalConfig {
        //nothing here
    }

    public void testCreateGameWithJWTAuthorization() throws Exception {
        TestUtils.TestRequestResponse testRequestResponse = TestUtils.prepareTestRequestResponse(request, expectedResponse);

        mockMvc.perform(MockMvcRequestBuilders.post(Url.GAMES_URL)
                .header(HeaderParameters.CALL_ID_HEADER, "HRCV123456789")
                .header(HttpHeaders.AUTHORIZATION, "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiIiLCJpYXQiOjE2MTE2MDE5NzIsImV4cCI6MjUyNjc1MDc3MiwiYXVkIjoiaHJjdiIsInN1YiI6ImFkbWluX2p3dCJ9.6kJJJ4QD1CfcD9LNI_3Fac15x4T1n5cx56iVOYFPnPo")
                .contentType(MediaType.APPLICATION_JSON)
                .content(testRequestResponse.getRequest()))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.content().json(testRequestResponse.getResponse()))
                .andReturn();
    }

    @Test(groups = TestGroup.DO_NOT_RUN)
    public void testCreateGameWithBasicAuthorization() throws Exception {
        TestUtils.TestRequestResponse testRequestResponse = TestUtils.prepareTestRequestResponse(request, expectedResponse);

        String basicDigestHeaderValue = "Basic " + new String(Base64.getEncoder().encode(("ADMIN:ADMIN").getBytes()));

        mockMvc.perform(MockMvcRequestBuilders.post(Url.GAMES_URL)
                .header(HeaderParameters.CALL_ID_HEADER, "HRCV123456789")
                .header(HttpHeaders.AUTHORIZATION, basicDigestHeaderValue)
                .contentType(MediaType.APPLICATION_JSON)
                .content(testRequestResponse.getRequest()))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.content().json(testRequestResponse.getResponse()))
                .andReturn();
    }
}