package sk.hrcv.wbbe.controller.test.playerControllerTests;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import sk.hrcv.common.util.TestUtils;
import sk.hrcv.test.config.AbstractTransactionalSpringBootTestWithData;
import sk.hrcv.test.config.TestGroup;
import sk.hrcv.wbbe.controller.constant.Url;

import static sk.hrcv.wbbe.controller.constant.Url.PLAYERS_URL_BY_ID;

@Test(groups = {TestGroup.UNIT, TestGroup.INTEGRATION})
public class GetPlayerTest extends AbstractTransactionalSpringBootTestWithData {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Value("classpath:player/getPlayer/response.json")
    private Resource expectedResponse;

    @BeforeClass
    private void initTest() {
        super.initTest(webApplicationContext);
    }

    public void testGetPlayer() throws Exception {
        String expectedResponse = TestUtils.prepareTestResponse(this.expectedResponse);

        get(Url.resolve(PLAYERS_URL_BY_ID, 0L))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.content().json(expectedResponse))
                .andReturn();
    }
}