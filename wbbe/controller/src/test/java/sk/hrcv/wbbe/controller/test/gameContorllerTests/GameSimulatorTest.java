package sk.hrcv.wbbe.controller.test.gameContorllerTests;

import mockit.Mock;
import mockit.MockUp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import sk.hrcv.common.util.TestUtils;
import sk.hrcv.test.config.AbstractTransactionalSpringBootTestWithData;
import sk.hrcv.test.config.TestGroup;
import sk.hrcv.wbbe.controller.constant.Url;
import sk.hrcv.wbbe.dto.type.Game;
import sk.hrcv.wbbe.service.impl.GameServiceImpl;

import static sk.hrcv.wbbe.controller.constant.Url.GAMES_URL_SIMULATOR;

@Test(groups = {TestGroup.UNIT, TestGroup.INTEGRATION})
// Just create database and do not load data
@Sql({"classpath:authTEST.sql", "classpath:/schemaTEST.sql"})
public class GameSimulatorTest extends AbstractTransactionalSpringBootTestWithData {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Value("classpath:game/gameSimulator/gameToPlay.json")
    private Resource mockedResponse;

    @BeforeClass
    private void initTest() {
        super.initTest(webApplicationContext);
    }

    public void testGameSimulator() throws Exception {
        mockGetGameInfo();

        get(Url.resolve(GAMES_URL_SIMULATOR, 11L))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andReturn();
    }

    private void mockGetGameInfo() {
        new MockUp<GameServiceImpl>() {
            @Mock
            public Game getGameInfo(final Long gameId) {
                return TestUtils.getMockedResponse(Game.class, mockedResponse);
            }
        };
    }
}