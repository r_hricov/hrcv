package sk.hrcv.wbbe.controller.test.tableControllerTests;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.springframework.web.context.WebApplicationContext;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import sk.hrcv.common.util.TestUtils;
import sk.hrcv.test.config.AbstractTransactionalSpringBootTestWithData;
import sk.hrcv.test.config.TestGroup;
import sk.hrcv.wbbe.controller.constant.Url;

@Test(groups = {TestGroup.UNIT, TestGroup.INTEGRATION})
public class GetTableRecordsTest extends AbstractTransactionalSpringBootTestWithData {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Value("classpath:table/getTableRecords/response.json")
    private Resource expectedResponse;

    @BeforeClass
    private void initTest() {
        super.initTest(webApplicationContext);
    }

    public void testGetTable() throws Exception {
        String expectedResponse = TestUtils.prepareTestResponse(this.expectedResponse);

        get(Url.TABLE_URL)
                .andDo(MockMvcResultHandlers.print())
                .andExpect(MockMvcResultMatchers.status().is2xxSuccessful())
                .andExpect(MockMvcResultMatchers.content().json(expectedResponse))
                .andReturn();
    }
}
