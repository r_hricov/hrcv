# Foosball controller

Sample

- [calls](./src/test/resources/requests.http)
- [requests and responses](./src/test/resources)

Services are available by:

## API Player

##### GET All Players

```
{GET /api/v1/players, consumes [application/json], produces [application/json]}
```

##### GET Player by id

```
{GET /api/v1/players/{playerId}, consumes [application/json], produces [application/json]}
```

##### UPDATE Player

```
{PUT /api/v1/players/{playerId}, consumes [application/json], produces [application/json]}
```

##### CREATE Player

```
{POST /api/v1/players, consumes [application/json], produces [application/json]}
```

##### DELETE Player

```
{DELETE /api/v1/players/{playerId}, consumes [application/json], produces [application/json]}
```

-----------------------------------------

## API Team

##### DELETE Team

```
{DELETE /api/v1/teams/{teamId}, consumes [application/json], produces [application/json]}
```

##### GET ALL Teams

```
{GET /api/v1/teams, consumes [application/json], produces [application/json]}
```

##### CREATE Team

```
{POST /api/v1/teams, consumes [application/json], produces [application/json]}
```

##### GET Team

```
{GET /api/v1/teams/{teamId}, consumes [application/json], produces [application/json]}
```

-----------------------------------------

## API Game

##### GET All Games for Team

```
{GET /api/v1/games/team/{teamId}, consumes [application/json], produces [application/json]}
```

##### GET Planner

```
{GET /api/v1/games/planner, consumes [application/json], produces [application/json]}
```

##### GET All Games for Player

```
{GET /api/v1/games/player/{playerId}, consumes [application/json], produces [application/json]}
```

##### GET Game

```
{GET /api/v1/games/{gameId}, consumes [application/json], produces [application/json]}
```

##### GET All Games

```
{GET /api/v1/games, consumes [application/json], produces [application/json]}
```

##### CREATE Game

```
{POST /api/v1/games, consumes [application/json], produces [application/json]}
```

##### Import Games

```
{POST /api/v1/games/import, consumes [application/json], produces [application/json]}
```

##### Simulate Game

```
{GET /api/v1/games/simulator/{gameId}, consumes [application/json], produces [application/json]}
```

-----------------------------------------

## API Table

##### GET Summary Table

```
{GET /api/v1/table, consumes [application/json], produces [application/json]}
```

[comment]: <> (-----------------------------------------)

[comment]: <> (```)

[comment]: <> (curl -i -H "Content-Type: application/json" -X GET -u ADMIN:ADMIN http://localhost:8080/foosball/api/v1/players)

[comment]: <> (```)

[comment]: <> (```)

[comment]: <> (curl -i -d '{"nickname":"LaraCroft","lostGamesCount":5}' -H "Content-Type: application/json" -X POST -u ADMIN:ADMIN http://localhost:8080/foosball/api/v1/players)

[comment]: <> (```)

[comment]: <> (```)

[comment]: <> (curl -i -H "Content-Type: application/json" -X DELETE -u ADMIN:ADMIN http://localhost:8080/foosball/api/v1/players/{$playerId})

[comment]: <> (```)