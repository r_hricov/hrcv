# Object fields description - MOST LIKELY IT IS DEPRECATED

List of all objects used for transfer data to client.

#### Object Player

````json
{
  "id": 2,                      //Mandatory Long
  "nickname": "IndianaJones",   //Mandatory String
  "wonGames": 2,                //Mandatory Long
  "lostGames": 2                //Mandatory Long
}
````

#### Object Team

````json
{
  "id": 1,                      //Mandatory Long
  "teamName": "BAR",            //Mandatory String
  "playerOne": {                //Mandatory Object Player
    "id": 2,
    "nickname": "IndianaJones",
    "wonGames": 2,
    "lostGames": 2
  },
  "playerTwo": {                //Optional Object Player
    "id": 3,
    "nickname": "JohnMcClane",
    "wonGames": 2,
    "lostGames": 2
  }
}
````

#### Object Game

````json
{
  "id": 1,                    //Mandatory Long
  "teamA": {                  //Mandatory Object Team
    "id": 0,
    "teamName": "FOO",
    "playerOne": {
      "id": 0,
      "nickname": "JohnDoe",
      "wonGames": 2,
      "lostGames": 0
    },
    "playerTwo": {
      "id": 1,
      "nickname": "JamesBond",
      "wonGames": 2,
      "lostGames": 0
    }
  },
  "teamB": {                  //Mandatory Object Team
    "id": 2,
    "teamName": "ABC",
    "playerOne": {
      "id": 4,
      "nickname": "EthanHunt",
      "wonGames": 1,
      "lostGames": 3
    },
    "playerTwo": {
      "id": 5,
      "nickname": "TonyStark",
      "wonGames": 1,
      "lostGames": 3
    }
  },
  "winner": {                  //Mandatory Object Team
    "id": 0,
    "teamName": "FOO",
    "playerOne": {
      "id": 0,
      "nickname": "JohnDoe",
      "wonGames": 2,
      "lostGames": 0
    },
    "playerTwo": {
      "id": 1,
      "nickname": "JamesBond",
      "wonGames": 2,
      "lostGames": 0
    }
  },
  "date": "2018-08-30T00:00:00" //Mandatory LocalDateTime
}
````

#### Object CreatePlayerRequest

````json
{
  "nickname": "ForesGump",    //Mandatory String (notblank)
  "wonGames": 1,              //Optional Long (mininal value 0)
  "lostGames": 1              //Optional Long (mininal value 0)
}
````

#### Object CreateTeamRequest

````json
{
  "teamName": "AllStarsTeam", //Mandatory String (notblank)
  "playerOneId": 1,           //Mandatory Long
  "playerTwoId": 2            //Optional Long
}
````

#### Object CreateGameRequest

````json
{
  "teamAId": 1,   //Mandatory Long
  "teamBId": 2,   //Mandatory Long
  "winnerId": 1   //Mandatory Long
}
````

#### Object ErrorResponse

````json
{
  "errorMessage": "Player with id = 10  not found" //String
}
````
