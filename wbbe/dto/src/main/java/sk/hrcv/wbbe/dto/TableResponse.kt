package sk.hrcv.wbbe.dto

import sk.hrcv.wbbe.dto.type.TableRecord

data class TableResponse(
    val table: List<TableRecord> = ArrayList(),
)