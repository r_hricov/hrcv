package sk.hrcv.wbbe.dto

data class ImportGameResponse(
    val gamesNotImported: List<String> = ArrayList()
)