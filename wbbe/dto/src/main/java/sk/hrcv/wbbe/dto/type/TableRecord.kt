package sk.hrcv.wbbe.dto.type

data class TableRecord(
    //because of compatibility with java, fields must be "var" since setters are needed
    //ideally val should be used
    var team: Team = Team(),
    var playedGamesCount: Long = 0,
    var wonGamesCount: Long = 0,
    var lostGamesCount: Long = 0,
    var drawnGamesCount: Long = 0,
    var points: Long = 0
)