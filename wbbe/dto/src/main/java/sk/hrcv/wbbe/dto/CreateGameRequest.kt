package sk.hrcv.wbbe.dto

import sk.hrcv.wbbe.dto.type.Score
import java.time.LocalDateTime

data class CreateGameRequest(
    var teamAId: Long = 0,
    var teamBId: Long = 0,
    var score: Score? = null,
    var dateTime: LocalDateTime? = null,
) {
    class Builder {
        var game = CreateGameRequest()
        fun withTeamAId(teamAId: Long): Builder {
            game = game.copy(teamAId = teamAId)
            return this
        }

        fun withTeamBId(teamBId: Long): Builder {
            game = game.copy(teamBId = teamBId)
            return this
        }

        fun withScore(score: Score): Builder {
            game = game.copy(score = score)
            return this
        }

        fun withDate(date: LocalDateTime): Builder {
            game = game.copy(dateTime = date)
            return this
        }

        fun build(): CreateGameRequest {
            return game
        }

    }

}

fun builder(): CreateGameRequest.Builder {
    return CreateGameRequest.Builder()
}
