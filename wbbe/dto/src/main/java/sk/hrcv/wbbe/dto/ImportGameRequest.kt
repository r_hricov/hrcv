package sk.hrcv.wbbe.dto

data class ImportGameRequest(
    val gamesToImport: List<String> = ArrayList()
)