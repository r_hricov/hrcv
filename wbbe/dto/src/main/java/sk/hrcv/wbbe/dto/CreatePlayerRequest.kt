package sk.hrcv.wbbe.dto

import javax.validation.constraints.NotBlank

data class CreatePlayerRequest(
    @field:NotBlank(message = "nickname is required in request")
    val nickname: String
)