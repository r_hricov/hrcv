package sk.hrcv.wbbe.dto.type

import javax.validation.constraints.NotBlank

data class Player(
    val playerId: Long = 0,

    @field:NotBlank
    val nickname: String = ""
)