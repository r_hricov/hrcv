package sk.hrcv.wbbe.dto.type

import com.fasterxml.jackson.databind.annotation.JsonSerialize
import sk.hrcv.common.DateTimeSerializer
import java.time.LocalDateTime
import javax.validation.Valid

data class Game(
    val gameId: Long = 0,

    @field:Valid
    val teamA: Team = Team(),

    @field:Valid
    val teamB: Team = Team(),

    @field:JsonSerialize(using = DateTimeSerializer::class)
//        @field:JsonDeserialize(using = DateTimeDeserializer::class)
    val date: LocalDateTime? = null,

    val season: Long = 0
)