package sk.hrcv.wbbe.dto

import sk.hrcv.wbbe.dto.type.Score

data class CreateScoreRequest(
    var gameId: Long = 0,
    var score: Score
)