package sk.hrcv.wbbe.dto

import javax.validation.constraints.NotBlank

data class CreateTeamRequest(
    @field:NotBlank(message = "teamName is required in request")
    var teamName: String = "",

    @field:NotBlank(message = "teamNameAbbr is required in request")
    var teamNameAbbr: String = "",

    var playerOneId: Long = 0,

    var playerTwoId: Long? = null
)