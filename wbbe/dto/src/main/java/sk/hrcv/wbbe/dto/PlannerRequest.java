package sk.hrcv.wbbe.dto;

import java.util.List;

public class PlannerRequest {

    List<java.util.List<String>> firstUnavailabilityCalendar;
    List<String> firstOverallAvailability;
    List<List<String>> secondUnavailabilityCalendar;
    List<String> secondOverallAvailability;

    public List<List<String>> getFirstUnavailabilityCalendar() {
        return firstUnavailabilityCalendar;
    }

    public void setFirstUnavailabilityCalendar(List<List<String>> firstUnavailabilityCalendar) {
        this.firstUnavailabilityCalendar = firstUnavailabilityCalendar;
    }

    public List<String> getFirstOverallAvailability() {
        return firstOverallAvailability;
    }

    public void setFirstOverallAvailability(List<String> firstOverallAvailability) {
        this.firstOverallAvailability = firstOverallAvailability;
    }

    public List<List<String>> getSecondUnavailabilityCalendar() {
        return secondUnavailabilityCalendar;
    }

    public void setSecondUnavailabilityCalendar(List<List<String>> secondUnavailabilityCalendar) {
        this.secondUnavailabilityCalendar = secondUnavailabilityCalendar;
    }

    public List<String> getSecondOverallAvailability() {
        return secondOverallAvailability;
    }

    public void setSecondOverallAvailability(List<String> secondOverallAvailability) {
        this.secondOverallAvailability = secondOverallAvailability;
    }

    @Override
    public String toString() {
        return "PlannerRequest{" +
                "firstUnavailabilityCalendar=" + firstUnavailabilityCalendar +
                ", firstOverallAvailability=" + firstOverallAvailability +
                ", secondUnavailabilityCalendar=" + secondUnavailabilityCalendar +
                ", secondOverallAvailability=" + secondOverallAvailability +
                '}';
    }
}