package sk.hrcv.wbbe.dto.type

data class GameFilter(
    val playedGames: Boolean = false,
    val proposedGames: Boolean = false,
)