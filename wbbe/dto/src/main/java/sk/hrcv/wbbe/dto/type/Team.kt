package sk.hrcv.wbbe.dto.type

import javax.validation.Valid
import javax.validation.constraints.NotBlank

data class Team(
    val teamId: Long = 0,

    @field:NotBlank
    val teamName: String = "",

//        Team is considered as response after successful store into DB so after this
//        teamNameAbbr does not have to be validated.
//        Otherwise it is impossible to obtain teamNameAbbr if it was created by JPA and selected by jdbc
//        within one transaction.
//        @field:TeamNameAbbr
    @field:NotBlank
    val teamNameAbbr: String = "",

    @field:Valid
    val playerOne: Player = Player(),

    @field:Valid
    val playerTwo: Player? = null
)