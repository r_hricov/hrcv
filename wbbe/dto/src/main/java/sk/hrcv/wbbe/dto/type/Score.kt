package sk.hrcv.wbbe.dto.type

import javax.validation.constraints.Min

data class Score(
    var gameId: Long = 0,

    @field:Min(0)
    var homeScore: Long = 0,

    @field:Min(0)
    var visitorScore: Long = 0
)