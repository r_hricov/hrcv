package sk.hrcv.wbbe.dto.type

import sk.hrcv.common.validator.TeamNameAbbr
import java.time.LocalDateTime

data class GameToImport(
    @field:TeamNameAbbr
    val teamAAbbr: String = "",

    @field:TeamNameAbbr
    val teamBAbbr: String = "",

    val score: Score = Score(),

    val dateTime: LocalDateTime = LocalDateTime.now()
) {
    class Builder {
        var game = GameToImport()

        fun withTeamA(teamA: String): Builder {
            game = game.copy(teamAAbbr = teamA)
            return this
        }

        fun withTeamB(teamB: String): Builder {
            game = game.copy(teamBAbbr = teamB)
            return this
        }

        fun withScore(score: Score): Builder {
            game = game.copy(score = score)
            return this
        }

        fun withDate(date: LocalDateTime): Builder {
            game = game.copy(dateTime = date)
            return this
        }

        fun build(): GameToImport {
            return game
        }
    }
}

fun builder(): GameToImport.Builder {
    return GameToImport.Builder()
}

