package sk.hrcv.wbbe.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import sk.hrcv.wbbe.repository.entity.GameEntity;

import javax.annotation.Nullable;
import java.util.List;

public interface GameRepository extends JpaRepository<GameEntity, Long>, GameRepositoryCustom {

    /**
     * @return Games with not null date, so they have already been played
     */
    List<GameEntity> findAllByDateNotNull();

    /**
     * @return Games with null date, so they have not been played and have no reference from Score table
     */
    List<GameEntity> findAllByDateIsNull();

    List<GameEntity> findAllByDateNotNullAndSeason(Long season);

    @Nullable
    GameEntity findByGameId(Long gameId);

    boolean existsByGameId(Long gameId);

    @Modifying
    @Query("DELETE FROM GameEntity g WHERE g.gameId in :games")
    void deleteAllGamesIn(List<Long> games);
}
