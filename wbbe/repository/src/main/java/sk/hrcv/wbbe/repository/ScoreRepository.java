package sk.hrcv.wbbe.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import sk.hrcv.wbbe.repository.entity.ScoreEntity;

import java.util.List;

public interface ScoreRepository extends JpaRepository<ScoreEntity, Long> {
    List<ScoreEntity> findByGame_GameIdIn(List<Long> gamesIds);

    boolean existsByGame_GameId(Long gameId);

    @Modifying
    @Query("DELETE FROM ScoreEntity s WHERE s.game.gameId IN :gameIds")
    void deleteAllByGameIdIn(List<Long> gameIds);
}
