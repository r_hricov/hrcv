package sk.hrcv.wbbe.repository.entity

import org.springframework.data.util.ProxyUtils
import java.io.Serializable
import javax.persistence.MappedSuperclass

/**
 * https://kotlinexpertise.com/hibernate-with-kotlin-spring-boot/
 */
@MappedSuperclass
abstract class AbstractJpaPersistable<T : Serializable> {

    companion object {
        private const val serialVersionUID = -5554308939380869754L
    }

    abstract fun getId(): T

    override fun equals(other: Any?): Boolean {
        other ?: return false

        if (this === other) return true

        if (javaClass != ProxyUtils.getUserClass(other)) return false

        other as AbstractJpaPersistable<*>

        return this.getId() == other.getId()
    }

    override fun hashCode(): Int {
        return javaClass.hashCode()
    }

}