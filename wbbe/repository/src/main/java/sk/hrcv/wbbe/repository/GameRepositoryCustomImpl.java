package sk.hrcv.wbbe.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import sk.hrcv.wbbe.repository.entity.GameEntity;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class GameRepositoryCustomImpl implements GameRepositoryCustom {

    private static final String GAMES_BY_TEAM = "SELECT g FROM GameEntity g WHERE g.teamA.teamId = :id OR g.teamB.teamId = :id";

    @Autowired
    @PersistenceContext(unitName = "foosballPersistenceUnit")
    private EntityManager entityManager;

    @Override
    public List<GameEntity> findGamesByTeam(Long teamId) {

        return entityManager.createQuery(GAMES_BY_TEAM, GameEntity.class)
                .setParameter("id", teamId)
                .getResultList();
    }
}