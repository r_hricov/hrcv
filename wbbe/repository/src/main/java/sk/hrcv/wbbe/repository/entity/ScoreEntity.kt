package sk.hrcv.wbbe.repository.entity

import sk.hrcv.wbbe.repository.listener.TechnicalColumnsListener
import javax.persistence.*

@Entity
@EntityListeners(TechnicalColumnsListener::class)
@Table(name = "SCORE", schema = "HRCV")
class ScoreEntity(
    @Embedded
    private var technicalColumns: TechnicalColumns,

    @Id
    @SequenceGenerator(name = "scoreSeq", sequenceName = "HRCV.SCORE_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "scoreSeq")
    @Column(name = "SCORE_ID")
    val id: Long,

    @OneToOne(fetch = FetchType.LAZY, optional = false) @JoinColumn(name = "GAME_ID")
    var game: GameEntity,

    @Basic
    @Column(name = "HOME_SCORE", nullable = false)
    var homeScore: Long,

    @Basic
    @Column(name = "VISITOR_SCORE", nullable = false)
    var visitorScore: Long
) : EntityWithTechnicalColumns, AbstractJpaPersistable<Long>() {

    override fun getId(): Long {
        return id
    }

    override fun setTechnicalColumns(technicalColumns: TechnicalColumns) {
        this.technicalColumns = technicalColumns
    }

    override fun getTechnicalColumns(): TechnicalColumns {
        return technicalColumns
    }

    override fun toString(): String {
        return "ScoreEntity(" +
                "technicalColumns=$technicalColumns, " +
                "id=$id, " +
                "game=$game, " +
                "homeScore=$homeScore, " +
                "visitorScore=$visitorScore" +
                ")"
    }
}