package sk.hrcv.wbbe.repository.listener;

import sk.hrcv.common.util.ApplicationContextHolder;

import javax.persistence.EntityManager;
import javax.persistence.PostPersist;

/**
 * Refreshes joined entities after save
 * https://stackoverflow.com/questions/45491551/refresh-and-fetch-an-entity-after-save-jpa-spring-data-hibernate
 */
public class UpdateEntityListener {
    /**
     * Refreshes joined entities after save
     *
     * @param object Entity to be refreshed
     */
    @PostPersist
    public void postPersists(Object object) {
        EntityManager entityManager = ApplicationContextHolder.getApplicationContext().getBean(EntityManager.class);
        entityManager.refresh(object);
    }
}
