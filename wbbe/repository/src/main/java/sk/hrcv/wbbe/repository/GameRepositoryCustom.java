package sk.hrcv.wbbe.repository;

import sk.hrcv.wbbe.repository.entity.GameEntity;

import java.util.List;

public interface GameRepositoryCustom {

    List<GameEntity> findGamesByTeam(Long teamId);

}