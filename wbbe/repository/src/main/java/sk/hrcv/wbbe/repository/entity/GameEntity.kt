package sk.hrcv.wbbe.repository.entity

import sk.hrcv.wbbe.repository.listener.TechnicalColumnsListener
import sk.hrcv.wbbe.repository.listener.UpdateEntityListener
import java.time.LocalDateTime
import javax.persistence.*

@Entity
@EntityListeners(TechnicalColumnsListener::class, UpdateEntityListener::class)
@Table(name = "GAME", schema = "HRCV")
class GameEntity(
    @Embedded
    private var technicalColumns: TechnicalColumns,

    @Id
    @SequenceGenerator(name = "gameSeq", sequenceName = "HRCV.GAME_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "gameSeq")
    @Column(name = "GAME_ID")
    val gameId: Long,

    @OneToOne(fetch = FetchType.EAGER, optional = false) @JoinColumn(name = "TEAM_A_ID")
    var teamA: TeamEntity,

    @OneToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "TEAM_B_ID")
    var teamB: TeamEntity,

    @Basic
    @Column(name = "DATE")
    var date: LocalDateTime? = null,

    @Basic
    @Column(name = "SEASON", nullable = false)
    var season: Long

) : EntityWithTechnicalColumns, AbstractJpaPersistable<Long>() {

    override fun getId(): Long {
        return gameId
    }

    override fun setTechnicalColumns(technicalColumns: TechnicalColumns) {
        this.technicalColumns = technicalColumns
    }

    override fun getTechnicalColumns(): TechnicalColumns {
        return technicalColumns
    }

    override fun toString(): String {
        return "GameEntity(" +
                "technicalColumns=$technicalColumns, " +
                "gameId=$gameId, " +
                "teamA=$teamA, " +
                "teamB=$teamB, " +
                "date=$date, " +
                "season=$season" +
                ")"
    }
}