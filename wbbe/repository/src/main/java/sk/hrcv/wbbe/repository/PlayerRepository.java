package sk.hrcv.wbbe.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import sk.hrcv.wbbe.repository.entity.PlayerEntity;

import javax.annotation.Nullable;

public interface PlayerRepository extends JpaRepository<PlayerEntity, Long> {

    @Nullable
    PlayerEntity findByPlayerId(Long playerId);

    boolean existsByPlayerId(Long playerId);

    boolean existsByNickname(String nickname);

    @Modifying
    @Query("DELETE FROM PlayerEntity p WHERE p.playerId = ?1")
    void deletePlayerById(Long playerId);

}