package sk.hrcv.wbbe.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import sk.hrcv.wbbe.repository.entity.TeamEntity;

import javax.annotation.Nullable;
import java.util.List;

public interface TeamRepository extends JpaRepository<TeamEntity, Long> {

    @Nullable
    TeamEntity findByTeamId(Long teamId);

    @Nullable
    TeamEntity findByTeamNameAbbr(String teamAbbr);

    boolean existsByTeamId(Long teamId);

    boolean existsByTeamNameAbbr(String teamAbbr);

    boolean existsByTeamName(String teamName);

    @Query("SELECT t.teamId FROM TeamEntity t " +
            "WHERE " +
            ":playerId IN(t.playerOne, t.playerTwo)")
    List<Long> findAllByPlayerIdInPlayerOneOrPlayerTwo(@Param("playerId") Long playerId);

    @Modifying
    @Query("DELETE FROM TeamEntity t WHERE t.teamId IN ?1")
    void deleteTeamByIdIn(List<Long> teamIds);
}
