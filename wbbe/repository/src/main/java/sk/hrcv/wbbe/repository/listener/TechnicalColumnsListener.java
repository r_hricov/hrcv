package sk.hrcv.wbbe.repository.listener;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import sk.hrcv.wbbe.repository.entity.EntityWithTechnicalColumns;
import sk.hrcv.wbbe.repository.entity.TechnicalColumns;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.time.LocalDateTime;

/**
 * This listener automatically fills some mandatory technical columns so they do not have to be filled by programmer.
 * Technical columns contain e.g. creating date/modifying date/call id etc...
 */
public class TechnicalColumnsListener {

    private static final Logger log = LoggerFactory.getLogger(TechnicalColumnsListener.class);

    @PrePersist
    public void prePersist(Object object) {
        EntityWithTechnicalColumns entityWithTechnicalColumns = (EntityWithTechnicalColumns) object;

        TechnicalColumns technicalColumns = new TechnicalColumns(LocalDateTime.now());

        entityWithTechnicalColumns.setTechnicalColumns(technicalColumns);
    }

    @PreUpdate
    public void preUpdate(Object object) {
        EntityWithTechnicalColumns entityWithTechnicalColumns = (EntityWithTechnicalColumns) object;

        TechnicalColumns actualTC = entityWithTechnicalColumns.getTechnicalColumns();

        TechnicalColumns newTC = new TechnicalColumns(LocalDateTime.now());

        log.debug(String.format("Is about to change technical columns from [%s] to [%s]", actualTC, newTC));

        entityWithTechnicalColumns.setTechnicalColumns(newTC);
    }
}