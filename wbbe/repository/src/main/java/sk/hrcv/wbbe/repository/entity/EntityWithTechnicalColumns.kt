package sk.hrcv.wbbe.repository.entity

interface EntityWithTechnicalColumns {
    fun setTechnicalColumns(technicalColumns: TechnicalColumns)

    fun getTechnicalColumns(): TechnicalColumns
}