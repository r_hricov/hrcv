package sk.hrcv.wbbe.repository.entity

import sk.hrcv.wbbe.repository.listener.TechnicalColumnsListener
import sk.hrcv.wbbe.repository.listener.UpdateEntityListener
import javax.persistence.*

@Entity
@EntityListeners(TechnicalColumnsListener::class, UpdateEntityListener::class)
@Table(name = "TEAM", schema = "HRCV")
class TeamEntity(
    @Embedded
    private var technicalColumns: TechnicalColumns,

    @Id
    @SequenceGenerator(name = "teamSeq", sequenceName = "HRCV.TEAM_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "teamSeq") @Column(name = "TEAM_ID")
    val teamId: Long,

    @Basic
    @Column(name = "TEAM_NAME", nullable = false)
    var teamName: String,

    @Basic
    @Column(name = "TEAM_NAME_ABBR", nullable = false)
    var teamNameAbbr: String,

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "PLAYER_ONE_ID", nullable = false)
    var playerOne: PlayerEntity,

    @OneToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "PLAYER_TWO_ID")
    var playerTwo: PlayerEntity? = null

) : EntityWithTechnicalColumns, AbstractJpaPersistable<Long>() {
    override fun getId(): Long {
        return teamId
    }

    override fun setTechnicalColumns(technicalColumns: TechnicalColumns) {
        this.technicalColumns = technicalColumns
    }

    override fun getTechnicalColumns(): TechnicalColumns {
        return technicalColumns
    }
}