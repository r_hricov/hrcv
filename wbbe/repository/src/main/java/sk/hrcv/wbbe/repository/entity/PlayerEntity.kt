package sk.hrcv.wbbe.repository.entity

import sk.hrcv.wbbe.repository.listener.TechnicalColumnsListener
import javax.persistence.*

/**
 * https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
 */
@Entity
@EntityListeners(TechnicalColumnsListener::class)
@Table(name = "PLAYER", schema = "HRCV")
class PlayerEntity(
    @Embedded
    private var technicalColumns: TechnicalColumns,

    @Id
    @SequenceGenerator(name = "playerSeq", sequenceName = "HRCV.PLAYER_SEQ", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "playerSeq")
    @Column(name = "PLAYER_ID")
    val playerId: Long,

    @Basic
    @Column(name = "NICKNAME", nullable = false)
    var nickname: String

) : EntityWithTechnicalColumns, AbstractJpaPersistable<Long>() {

    override fun getId(): Long {
        return playerId
    }

    override fun setTechnicalColumns(technicalColumns: TechnicalColumns) {
        this.technicalColumns = technicalColumns
    }

    override fun getTechnicalColumns(): TechnicalColumns {
        return technicalColumns
    }

    override fun toString(): String {
        return "PlayerEntity(" +
                "technicalColumns=$technicalColumns, " +
                "playerId=$playerId, " +
                "nickname='$nickname'" +
                ")"
    }
}