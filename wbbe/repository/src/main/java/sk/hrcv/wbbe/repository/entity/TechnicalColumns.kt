package sk.hrcv.wbbe.repository.entity

import java.time.LocalDateTime
import javax.persistence.Column
import javax.persistence.Embeddable

@Embeddable
class TechnicalColumns(
    @Column(name = "CREATED_DATE", nullable = false)
    val createdDate: LocalDateTime? = null
) {
    override fun toString(): String {
        return "TechnicalColumns(" +
                "createdDate=$createdDate" +
                ")"
    }
}