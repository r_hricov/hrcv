package sk.hrcv.wbbe.repository.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;
import sk.hrcv.test.config.AbstractTransactionalSpringBootTestWithData;
import sk.hrcv.test.config.TestGroup;
import sk.hrcv.wbbe.repository.GameRepository;
import sk.hrcv.wbbe.repository.entity.GameEntity;

import java.util.List;

@Test(groups = TestGroup.UNIT)
public class GameRepositoryTest extends AbstractTransactionalSpringBootTestWithData {

    @Autowired
    private GameRepository gameRepository;

    /**
     * Test custom repository
     */
    public void testFindGamesPlayedByTeam() {
        Long desiredTeamId = 2L;
        List<GameEntity> allGamesByTeamInTeamAOrTeamB = gameRepository.findGamesByTeam(desiredTeamId);
        Assert.assertEquals(allGamesByTeamInTeamAOrTeamB.size(), 12);

        allGamesByTeamInTeamAOrTeamB.forEach(game ->
                Assert.assertTrue(desiredTeamId.equals(game.getTeamA().getTeamId()) ||
                        desiredTeamId.equals(game.getTeamB().getTeamId()))
        );
    }
}