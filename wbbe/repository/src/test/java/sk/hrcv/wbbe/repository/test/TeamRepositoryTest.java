package sk.hrcv.wbbe.repository.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;
import sk.hrcv.test.config.AbstractTransactionalSpringBootTestWithData;
import sk.hrcv.test.config.TestGroup;
import sk.hrcv.wbbe.repository.TeamRepository;
import sk.hrcv.wbbe.repository.entity.TeamEntity;

import java.util.List;

@Test(groups = TestGroup.UNIT)
public class TeamRepositoryTest extends AbstractTransactionalSpringBootTestWithData {

    @Autowired
    private TeamRepository teamRepository;

    public void testFindAllByTeamInTeamAOrTeamB() {
        Long desiredPlayerId = 2L;
        List<Long> allTeamsByPlayerInTeamAOrTeamB = teamRepository.findAllByPlayerIdInPlayerOneOrPlayerTwo(desiredPlayerId);
        Assert.assertEquals(allTeamsByPlayerInTeamAOrTeamB.size(), 1);

        allTeamsByPlayerInTeamAOrTeamB.forEach(teamId -> {
            TeamEntity team = teamRepository.findByTeamId(teamId);

            Assert.assertNotNull(team);
            Assert.assertTrue(desiredPlayerId.equals(team.getPlayerOne().getPlayerId()) ||
                    (team.getPlayerTwo() != null && desiredPlayerId.equals(team.getPlayerTwo().getPlayerId())));
        });
    }
}