package sk.hrcv.wbbe.repository.test;

import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;
import sk.hrcv.test.config.AbstractTransactionalSpringBootTestWithData;
import sk.hrcv.test.config.TestGroup;
import sk.hrcv.wbbe.repository.PlayerRepository;
import sk.hrcv.wbbe.repository.entity.PlayerEntity;

@Test(groups = TestGroup.UNIT)
public class TechnicalColumnsListenerTest extends AbstractTransactionalSpringBootTestWithData {

    @Autowired
    private PlayerRepository playerRepository;

    public void testTechnicalColumnsLister() {
        PlayerEntity player = new PlayerEntity();
        player.setNickname("XxXxXxX");

        Assert.assertNull(player.getTechnicalColumns());

        playerRepository.saveAndFlush(player);

        Assert.assertNotNull(player.getTechnicalColumns().getCreatedDate());
    }

}
