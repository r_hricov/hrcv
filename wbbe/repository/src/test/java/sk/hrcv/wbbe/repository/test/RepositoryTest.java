package sk.hrcv.wbbe.repository.test;

import org.testng.annotations.Test;
import sk.hrcv.test.config.AbstractTransactionalSpringBootTestWithData;
import sk.hrcv.test.config.TestGroup;

@Test(groups = TestGroup.UNIT)
public class RepositoryTest extends AbstractTransactionalSpringBootTestWithData {

    public void validateRepositories() {
        //This test seems to be empty, but it actually validates repositories
    }

}