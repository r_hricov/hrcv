package sk.hrcv.config;

import com.github.benmanes.caffeine.cache.CaffeineSpec;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.caffeine.CaffeineCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

@EnableCaching
@Configuration
public class CacheConfig {

    private static CacheProperties.SpecificCacheProperties DEFAULT_CACHE_PROPERTIES;
    private static CacheProperties.SpecificCacheProperties NUMBER_ENCODING_CACHE_PROPERTIES;

    @Autowired
    public CacheConfig(CacheProperties cacheProperties) {
        DEFAULT_CACHE_PROPERTIES = cacheProperties.getSpecificCacheProperties().get(CacheProperties.DEFAULT);
        NUMBER_ENCODING_CACHE_PROPERTIES = cacheProperties.getSpecificCacheProperties().get(CacheProperties.NUMBER_ENCODING);
    }

    @Bean
    @Primary
    public CacheManager cacheManager() {
        return createCacheManager(DEFAULT_CACHE_PROPERTIES.getExpireAfterWriteInSeconds(), DEFAULT_CACHE_PROPERTIES.getCacheMaximumSize());
    }

    @Bean(name = "numberEncodingCacheManager")
    public CacheManager numberEncodingCacheManager() {
        return createCacheManager(NUMBER_ENCODING_CACHE_PROPERTIES.getExpireAfterWriteInSeconds(),
                NUMBER_ENCODING_CACHE_PROPERTIES.getCacheMaximumSize());
    }

    @Bean(name = "tableCacheManager")
    public CacheManager tableCacheManager() {
        return createCacheManager(DEFAULT_CACHE_PROPERTIES.getExpireAfterWriteInSeconds(), DEFAULT_CACHE_PROPERTIES.getCacheMaximumSize());
    }

    @Bean(name = "teamNameAbbrValidatorCacheManager")
    public CacheManager teamNameAbbrValidatorCacheManager() {
        return createCacheManager(DEFAULT_CACHE_PROPERTIES.getExpireAfterWriteInSeconds(), DEFAULT_CACHE_PROPERTIES.getCacheMaximumSize());
    }

    private CaffeineCacheManager createCacheManager(Long expiredSecond, Long cacheMaximumSize) {
        CaffeineCacheManager caffeineCacheManager = new CaffeineCacheManager();

        caffeineCacheManager.setCaffeineSpec(
                CaffeineSpec.parse(String.format("expireAfterWrite=%ds,maximumSize=%d",
                        expiredSecond, cacheMaximumSize))
        );

        return caffeineCacheManager;
    }

}
