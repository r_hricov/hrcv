package sk.hrcv.config;

import org.aspectj.lang.annotation.Pointcut;
import org.springframework.aop.Advisor;
import org.springframework.aop.aspectj.AspectJExpressionPointcut;
import org.springframework.aop.support.DefaultPointcutAdvisor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import sk.hrcv.config.logging.CallingStackLoggerInterceptor;

@EnableAspectJAutoProxy
@Configuration
public class LoggingAspectConfig {

    private final CallingStackLoggerInterceptor callingStackLoggerInterceptor;

    public LoggingAspectConfig(CallingStackLoggerInterceptor callingStackLoggerInterceptor) {
        this.callingStackLoggerInterceptor = callingStackLoggerInterceptor;
    }

    @Pointcut("execution(public * (@org.springframework.web.bind.annotation.RestController *).*(..))")
    public void controller() {
        //nothing to do here
    }

    @Pointcut("execution(public * org.springframework.stereotype.Service+.*(..))")
    public void service() {
        //nothing to do here
    }

    @Pointcut("execution(public * org.springframework.data.jpa.repository.JpaRepository+.*(..))")
    public void repository() {
        //nothing to do here
    }

    @Pointcut("execution(* java.lang.Object.*(..))")
    public void obj() {
        //nothing to do here
    }

    @Pointcut("(controller() || service() || repository()) && !obj())")
    public void getAggregatedPointcuts() {
        //nothing to do here
    }

    @Bean
    public Advisor callingAdvisor() {
        AspectJExpressionPointcut aspectJExpressionPointcut = new AspectJExpressionPointcut();
        aspectJExpressionPointcut.setExpression(this.getClass().getName() + ".getAggregatedPointcuts()");

        return new DefaultPointcutAdvisor(aspectJExpressionPointcut, callingStackLoggerInterceptor);
    }
}
