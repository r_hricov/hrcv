package sk.hrcv.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("sk.hrcv")
public class ProjectSpringConfig {
}
