package sk.hrcv.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import sk.hrcv.config.logging.CallContextInterceptor;
import sk.hrcv.config.logging.RequestResponseLoggingInterceptor;
import sk.hrcv.config.logging.ServiceLoggerInterceptor;

/**
 * Within this configuration all required interceptors are registered
 */
@Configuration
@EnableWebMvc
public class RestConfig implements WebMvcConfigurer {

    private final CallContextInterceptor callContextInterceptor;

    private final RequestResponseLoggingInterceptor requestResponseLoggingInterceptor;

    private final ServiceLoggerInterceptor serviceLoggerInterceptor;

    @Autowired
    public RestConfig(CallContextInterceptor callContextInterceptor, RequestResponseLoggingInterceptor requestResponseLoggingInterceptor, ServiceLoggerInterceptor serviceLoggerInterceptor) {
        this.callContextInterceptor = callContextInterceptor;
        this.requestResponseLoggingInterceptor = requestResponseLoggingInterceptor;
        this.serviceLoggerInterceptor = serviceLoggerInterceptor;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(callContextInterceptor);
        registry.addInterceptor(requestResponseLoggingInterceptor);
        registry.addInterceptor(serviceLoggerInterceptor);
    }
}
