package sk.hrcv.config.logging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import javax.annotation.Nullable;
import java.time.LocalDateTime;

@Component
public class ServiceLogger {

    private final CallContext callContext;

    private static final Logger serviceResultLog = LoggerFactory.getLogger(ServiceResultLogger.class);
    private static final Logger applicationLog = LoggerFactory.getLogger(ApplicationLogger.class);

    public ServiceLogger(CallContext callContext) {
        this.callContext = callContext;
    }

    public void log(LocalDateTime time, long duration, String methodName, @Nullable Exception ex) {
        String serviceResult = String.format("%s | %s | %s | %s | %s | %s", time, callContext.getCallId(), callContext.getCallChainId(), duration, methodName,
                ex != null ? ex.getClass().getSimpleName() : "");
        serviceResultLog.trace(serviceResult);

        if (ex != null) {
            String errorResult = String.format("%s | %s | %s | %s | %s", time, callContext.getCallId(), callContext.getCallChainId(), methodName, ex.getMessage());
            applicationLog.error(errorResult);
        }
    }

}

class ServiceResultLogger {
}

class ApplicationLogger {
}