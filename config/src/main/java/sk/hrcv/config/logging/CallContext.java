package sk.hrcv.config.logging;

import org.springframework.stereotype.Component;

@Component
public class CallContext {
    private String callChainId;
    private String callId;

    public String getCallChainId() {
        return callChainId;
    }

    public void setCallChainId(String callChainId) {
        this.callChainId = callChainId;
    }

    public String getCallId() {
        return callId;
    }

    public void setCallId(String callId) {
        this.callId = callId;
    }
}
