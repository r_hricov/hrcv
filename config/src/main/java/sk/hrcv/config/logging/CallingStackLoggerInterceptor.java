package sk.hrcv.config.logging;

import org.springframework.aop.interceptor.CustomizableTraceInterceptor;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * This interceptor logs start and finish of called methods on spring managed beans (controller->service->repository, etc)
 * within unique call.
 * Non spring managed classes are logged by TracedExecutionLogger.
 * </br>
 * See also {@link sk.hrcv.config.LoggingAspectConfig}
 */
@Component
public class CallingStackLoggerInterceptor extends CustomizableTraceInterceptor {

    private static final String ENTER_MESSAGE = "$[targetClassShortName].$[methodName] Started";
    private static final String EXIT_MESSAGE = "$[targetClassShortName].$[methodName] - In $[invocationTime] finished";

    @PostConstruct
    private void configureLogging() {
        this.setLoggerName(this.getClass().getName());
        this.setEnterMessage(ENTER_MESSAGE);
        this.setExitMessage(EXIT_MESSAGE);
        this.setLogExceptionStackTrace(Boolean.TRUE);
        this.setExceptionMessage(EXIT_MESSAGE);
        this.setHideProxyClassNames(Boolean.TRUE);
    }

    @Override
    protected Class<?> getClassForLogging(Object target) {
        return super.getClassForLogging(target);
    }
}
