package sk.hrcv.config.logging;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import sk.hrcv.config.constant.HeaderParameters;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.util.Random;

/**
 * This interceptor reads headers e.g. call-id-header from incoming request and stores it to {@link CallContext}
 */
@Component
public class CallContextInterceptor implements HandlerInterceptor {

    private final CallContext callContext;

    @Autowired
    public CallContextInterceptor(CallContext callContext) {
        this.callContext = callContext;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        String callId = request.getHeader(HeaderParameters.CALL_ID_HEADER);

        if (callId == null || callId.isEmpty()) {
            throw new IllegalStateException("Missing call-id-header in http request");
        }

        callContext.setCallChainId(callId);
        callContext.setCallId(getCallId());

        return true;
    }

    private String getCallId() {
        LocalDateTime now = LocalDateTime.now();

        Random random = new Random();
        //minimum + random.nextInt(maxValue - minvalue + 1)
        //range 10000 - 99999
        int randomNumber = 10000 + random.nextInt(90000);

        return "HRCV" +
                now.getYear() +
                dasd(now.getMonth().getValue()) +
                dasd(now.getDayOfMonth()) +
                dasd(now.getHour()) +
                dasd(now.getMinute()) +
                dasd(now.getSecond()) +
                randomNumber;
    }

    private String dasd(int i) {
        if (i < 10) {
            return "0" + i;
        }
        return "" + i;
    }
}
