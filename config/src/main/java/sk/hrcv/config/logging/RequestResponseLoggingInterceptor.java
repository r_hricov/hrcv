package sk.hrcv.config.logging;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;

import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.util.Collections;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * This interceptor logs request (including headers and url parameters) and response (including http status)
 * https://www.baeldung.com/spring-http-logging
 * https://www.baeldung.com/spring-reading-httpservletrequest-multiple-times
 * https://www.baeldung.com/mdc-in-log4j-2-logback
 * http://logback.qos.ch/manual/mdc.html
 */
@Component
public class RequestResponseLoggingInterceptor implements HandlerInterceptor {

    private static final Logger log = LoggerFactory.getLogger(RequestResponseLoggingInterceptor.class);

    private final CallContext callContext;

    @Autowired
    public RequestResponseLoggingInterceptor(CallContext callContext) {
        this.callContext = callContext;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        MDC.put("callId", callContext.getCallId());
        MDC.put("callChainId", callContext.getCallChainId());

        String parameters = formatParameters(request.getParameterMap());
        String headers = formatHeaders(request);
        String req = formatRequest(request);

        log.info(String.format("Request %s: %s | Headers [%s] | Parameters [%s] | %s",
                request.getMethod(), request.getRequestURI(), headers, parameters, req));

        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable Exception ex) throws Exception {
        String req = formatRequest(request);
        String res = formatResponse(response);

        log.info(String.format("Response %s -> %s | on request: %s", response.getStatus(), res, req));
        MDC.clear();
    }

    private String formatRequest(HttpServletRequest request) {
        if (!(request instanceof ContentCachingRequestWrapper)) {
            request = new ContentCachingRequestWrapper(request);
        }
        String unformattedJson = new String(((ContentCachingRequestWrapper) request).getContentAsByteArray(), Charset.forName(request.getCharacterEncoding()));

        Gson gsonBuilder = new GsonBuilder().create();
        JsonParser jsonParser = new JsonParser();

        JsonElement jsonElement = jsonParser.parse(unformattedJson);
        return gsonBuilder.toJson(jsonElement);
    }

    private String formatResponse(HttpServletResponse response) {
        if (!(response instanceof ContentCachingResponseWrapper)) {
            response = new ContentCachingResponseWrapper(response);
        }
        BufferedReader reader = new BufferedReader(new InputStreamReader(((ContentCachingResponseWrapper) response).getContentInputStream(), Charset.forName(response.getCharacterEncoding())));
        String unformattedJson = reader.lines().collect(Collectors.joining(System.lineSeparator()));

        Gson gsonBuilder = new GsonBuilder().create();
        JsonParser jsonParser = new JsonParser();

        JsonElement jsonElement = jsonParser.parse(unformattedJson);
        return gsonBuilder.toJson(jsonElement);
    }

    private String formatParameters(Map<String, String[]> parameterMap) {
        StringBuilder parametersResult = new StringBuilder();
        for (Map.Entry<String, String[]> parameters : parameterMap.entrySet()) {
            parametersResult
                    .append(parameters.getKey().toUpperCase())
                    .append(": ")
                    .append(String.join(", ", parameters.getValue()))
                    .append("; ");
        }

        return parametersResult.toString();
    }

    private String formatHeaders(HttpServletRequest request) {
        StringBuilder headersResult = new StringBuilder();

        Map<String, String> headersMap = Collections.list(request.getHeaderNames()).stream().collect(Collectors.toMap(it -> it, request::getHeader));
        for (Map.Entry<String, String> parameters : headersMap.entrySet()) {
            headersResult
                    .append(parameters.getKey().toUpperCase())
                    .append(": ")
                    .append(String.join(", ", parameters.getValue()))
                    .append("; ");
        }

        return headersResult.toString();
    }
}
