package sk.hrcv.config.logging;

import com.google.common.base.Stopwatch;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.annotation.Nullable;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.concurrent.TimeUnit;

/**
 * This interceptor logs duration of called service (time between incoming request and outgoing response)
 */
@Component
public class ServiceLoggerInterceptor implements HandlerInterceptor {

    private final ServiceLogger serviceLogger;

    private final ThreadLocal<Stopwatch> stopWatch = new ThreadLocal<>();

    @Autowired
    public ServiceLoggerInterceptor(ServiceLogger serviceLogger) {
        this.serviceLogger = serviceLogger;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        stopWatch.set(Stopwatch.createStarted());
        return true;
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, @Nullable Exception ex) throws Exception {
        long duration = stopWatch.get().elapsed(TimeUnit.MILLISECONDS);
        stopWatch.set(null);

        String methodName = ((HandlerMethod) handler).getMethod().getName();
        serviceLogger.log(LocalDateTime.now().minus(duration, ChronoUnit.MILLIS), duration, methodName, ex);
    }
}
