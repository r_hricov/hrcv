package sk.hrcv.config.logging;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.util.ContentCachingRequestWrapper;
import org.springframework.web.util.ContentCachingResponseWrapper;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * This filter wraps servlet request/response into ContentCachingRequestWrapper/ContentCachingResponseWrapper
 * https://www.baeldung.com/spring-http-logging
 * https://www.baeldung.com/spring-reading-httpservletrequest-multiple-times
 */
@Component
public class RequestResponseLoggingCacheFilter implements Filter {
    private static final Logger log = LoggerFactory.getLogger(RequestResponseLoggingCacheFilter.class);

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        log.debug("Init filter");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        ContentCachingResponseWrapper wrappedResponse = new ContentCachingResponseWrapper((HttpServletResponse) servletResponse);
        try {
            ContentCachingRequestWrapper wrappedRequest = new ContentCachingRequestWrapper((HttpServletRequest) servletRequest);
            wrappedRequest.getParameterMap();
            filterChain.doFilter(wrappedRequest, wrappedResponse);
        } finally {
            wrappedResponse.copyBodyToResponse();
        }
    }

    @Override
    public void destroy() {
        log.debug("Destroy filter");
    }
}
