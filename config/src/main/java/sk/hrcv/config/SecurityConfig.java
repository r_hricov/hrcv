package sk.hrcv.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import sk.hrcv.config.security.SecurityProperties;
import sk.hrcv.config.security.basic.SecurityBasicConfig;
import sk.hrcv.config.security.jwt.AuthenticationExceptionHandler;
import sk.hrcv.config.security.jwt.AuthenticationFilter;
import sk.hrcv.config.security.jwt.AuthenticationProvider;
import sk.hrcv.config.security.jwt.SecurityJWTConfig;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
//@Order(99)
public class SecurityConfig {

    private static final Logger log = LoggerFactory.getLogger(SecurityConfig.class);

    private final DataSource dataSource;
    private final AuthenticationProvider authenticationProvider;
    private final AuthenticationFilter authenticationFilter;
    private final AuthenticationExceptionHandler authenticationExceptionHandler;
    private final SecurityProperties securityProperties;

    @Autowired
    public SecurityConfig(@Lazy DataSource dataSource,
                          @Lazy AuthenticationProvider authenticationProvider,
                          @Lazy AuthenticationFilter authenticationFilter,
                          @Lazy AuthenticationExceptionHandler authenticationExceptionHandler,
                          SecurityProperties securityProperties) {
        this.dataSource = dataSource;
        this.authenticationProvider = authenticationProvider;
        this.authenticationFilter = authenticationFilter;
        this.authenticationExceptionHandler = authenticationExceptionHandler;
        this.securityProperties = securityProperties;
    }

    @Bean
    public WebSecurityConfigurerAdapter webSecurityConfig() {
        log.debug("Initializing {} security config", securityProperties.getSecurityType());
        if (SecurityProperties.SecurityType.JWT.equals(securityProperties.getSecurityType())) {
            return new SecurityJWTConfig(authenticationProvider, authenticationFilter, authenticationExceptionHandler);
        } else if (SecurityProperties.SecurityType.BASIC.equals(securityProperties.getSecurityType())) {
            return new SecurityBasicConfig(dataSource);
        } else {
            throw new IllegalStateException(String.format("Not supported security type %s", securityProperties.getSecurityType()));
        }
    }

}
