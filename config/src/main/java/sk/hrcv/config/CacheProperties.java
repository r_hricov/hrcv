package sk.hrcv.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import java.util.HashMap;
import java.util.Map;

@Validated
@ConfigurationProperties(prefix = "cache-config")
@Component
public class CacheProperties {
    public static final String DEFAULT = "default";
    public static final String NUMBER_ENCODING = "numberEncoding";

    private Map<String, SpecificCacheProperties> specificCacheProperties = new HashMap<>();

    public Map<String, SpecificCacheProperties> getSpecificCacheProperties() {
        return specificCacheProperties;
    }

    public void setSpecificCacheProperties(Map<String, SpecificCacheProperties> specificCacheProperties) {
        this.specificCacheProperties = specificCacheProperties;
    }

    public static class SpecificCacheProperties {
        private Long expireAfterWriteInSeconds;
        private Long cacheMaximumSize;

        public Long getExpireAfterWriteInSeconds() {
            return expireAfterWriteInSeconds;
        }

        public void setExpireAfterWriteInSeconds(Long expireAfterWriteInSeconds) {
            this.expireAfterWriteInSeconds = expireAfterWriteInSeconds;
        }

        public Long getCacheMaximumSize() {
            return cacheMaximumSize;
        }

        public void setCacheMaximumSize(Long cacheMaximumSize) {
            this.cacheMaximumSize = cacheMaximumSize;
        }
    }
}
