package sk.hrcv.config;

import net.ttddyy.dsproxy.listener.logging.DefaultQueryLogEntryCreator;
import net.ttddyy.dsproxy.listener.logging.SLF4JQueryLoggingListener;
import net.ttddyy.dsproxy.support.ProxyDataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseFactory;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import javax.sql.DataSource;

/**
 * DataSource configuration that use DatasourceProxy to log queries
 * http://ttddyy.github.io/datasource-proxy/docs/current/user-guide/index.html
 */
@Configuration
public class DataSourceConfig {

    private static final String DATABASE_NAME = "foosballdb";

    @Bean(name = "foosballDbDataSource")
    @Primary
    public DataSource foosballDbDataSource() {
        EmbeddedDatabaseFactory embeddedDatabaseFactory = new EmbeddedDatabaseFactory();
        embeddedDatabaseFactory.setDatabaseName(DATABASE_NAME);
        embeddedDatabaseFactory.setDatabaseType(EmbeddedDatabaseType.H2);

        SLF4JQueryLoggingListener listener = new SLF4JQueryLoggingListener();
        listener.setQueryLogEntryCreator(new DefaultQueryLogEntryCreator());

        return ProxyDataSourceBuilder.create()
                .dataSource(embeddedDatabaseFactory.getDatabase())
                .listener(listener)
                .name("FoosballDatasource")
                .countQuery()
                .build();
    }

    //TODO Add another datasource e.g. for remote DB
}