package sk.hrcv.config.security.jwt;

import io.jsonwebtoken.*;
import io.jsonwebtoken.impl.DefaultJws;
import io.jsonwebtoken.impl.DefaultJwsHeader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import sk.hrcv.config.security.SecurityProperties;

import javax.crypto.spec.SecretKeySpec;
import javax.sql.DataSource;
import java.security.Key;
import java.sql.SQLException;
import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.Base64;
import java.util.Date;
import java.util.Objects;

/**
 * http://jwtbuilder.jamiekurtz.com/
 * {
 * "iss": "",
 * "iat": 1611601972,
 * "exp": 2526750772,
 * "aud": "hrcv",
 * "sub": "admin_jwt"
 * }
 * pass> admin
 */
@Service
public class AuthenticationService {

    private static final Logger log = LoggerFactory.getLogger(AuthenticationService.class);
    private static final String QUERY = "select USERNAME, E_PASSWORD, ENABLED from AUTH.USER where UPPER(USERNAME) = UPPER(?)";

    private final JdbcTemplate jdbcTemplate;
    private final SecurityProperties securityProperties;

    @Autowired
    public AuthenticationService(@Qualifier("foosballDbDataSource") DataSource dataSource, SecurityProperties securityProperties) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.securityProperties = securityProperties;
    }

    public String verifyToken(String token) {
        Jws<Claims> claims;

        try {
            claims = Jwts.parser()
                    .setSigningKeyResolver(new SigningKeyResolver() {
                        @Override
                        public Key resolveSigningKey(JwsHeader jwsHeader, Claims claims) {
                            SignatureAlgorithm alg = SignatureAlgorithm.forName(jwsHeader.getAlgorithm());

                            Credentials credentials = getCredentials(claims.getSubject());
                            if (credentials.isEnabled()) {
                                byte[] encodedPswd = encodePassword(credentials.getPassword());
                                return new SecretKeySpec(encodedPswd, alg.getJcaName());
                            }
                            throw new IllegalStateException("Not  authorized since subject is not allowed");
                        }

                        @Override
                        public Key resolveSigningKey(JwsHeader jwsHeader, String s) {
                            throw new IllegalStateException("No claims");
                        }
                    })
                    .parseClaimsJws(token);
        } catch (ExpiredJwtException e) {
            //TODO set proper signature and test exception
            claims = new DefaultJws<>(new DefaultJwsHeader(e.getHeader()), e.getClaims(), "");
            log.debug("Token is expired but probably still in range now +/- {} ms", securityProperties.getJwtProperties().getExpirationInSeconds());
        } catch (MalformedJwtException | SignatureException e) {
            log.debug("Token is not valid - is malformed or wrong signed");
            throw e;
        }

        verifyIAT(claims.getBody().getIssuedAt());
        verifyAUD(claims.getBody().getAudience());

        return claims.getBody().getSubject();
    }

    private byte[] encodePassword(String password) {
        //For testing purpose password is stored as encoded base64 hash
        return Base64.getDecoder().decode(password);
    }

    private void verifyIAT(Date issuedAt) {
        Instant iat = issuedAt.toInstant();
        Long expirationInSeconds = securityProperties.getJwtProperties().getExpirationInSeconds();

        boolean isEarly = iat.minus(expirationInSeconds, ChronoUnit.SECONDS).isAfter(Instant.now());
        boolean isOld = iat.plus(expirationInSeconds, ChronoUnit.SECONDS).isBefore(Instant.now());

        if (isOld || isEarly) {
            throw new IllegalStateException(String.format("IAT not verified since it is not in range now +/- %s ms", expirationInSeconds));
        }
    }

    private void verifyAUD(String audience) {
        String expectedAUD = securityProperties.getJwtProperties().getAudience();
        if (!expectedAUD.equals(audience)) {
            throw new IllegalStateException(String.format("AUD not verified since found unexpected audience %s", audience));
        }
    }

    private Credentials getCredentials(String username) {
        return Objects.requireNonNull(jdbcTemplate.query(QUERY, row -> {
            if (row.next()) {
                return new Credentials(
                        row.getString("USERNAME"),
                        row.getString("E_PASSWORD"),
                        row.getBoolean("ENABLED")
                );
            } else {
                throw new SQLException();
            }
        }, username));
    }

    public static class Credentials {
        private final String username;
        private final String password;
        private final boolean enabled;

        public Credentials(String username, String password, boolean enabled) {
            this.username = username;
            this.password = password;
            this.enabled = enabled;
        }

        public String getUsername() {
            return username;
        }

        public String getPassword() {
            return password;
        }

        public boolean isEnabled() {
            return enabled;
        }
    }
}