package sk.hrcv.config.security.jwt;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.MediaType;
import org.springframework.security.core.AuthenticationException;
import org.springframework.stereotype.Component;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Handles authentication exception
 */
@Component
public class AuthenticationExceptionHandler implements org.springframework.security.web.AuthenticationEntryPoint {

    @Override
    public void commence(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, AuthenticationException e) throws IOException, ServletException {
        httpServletResponse.setStatus(HttpServletResponse.SC_FORBIDDEN);
        httpServletResponse.setContentType(MediaType.APPLICATION_JSON_VALUE);

        Map<String, String> responseMap = new HashMap<>(2);
        responseMap.put("message", e.getMessage());
        if (e.getCause() != null) {
            responseMap.put("cause", e.getCause().getMessage());
        }

        httpServletResponse.getOutputStream().write(
                new ObjectMapper().writeValueAsBytes(responseMap)
        );
    }
}