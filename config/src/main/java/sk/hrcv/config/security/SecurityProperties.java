package sk.hrcv.config.security;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.annotation.Nullable;
import javax.annotation.PostConstruct;

@Validated
@ConfigurationProperties(prefix = "security-config")
@Component
public class SecurityProperties {

    private boolean enabled;
    private SecurityType securityType;
    @Nullable
    private JwtProperties jwtProperties;

    @PostConstruct
    private void init() {
        //default configuration if not configured in application.yml
        securityType = SecurityType.JWT;
        enabled = true;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public SecurityType getSecurityType() {
        return securityType;
    }

    public void setSecurityType(SecurityType securityType) {
        this.securityType = securityType;
    }

    @Nullable
    public JwtProperties getJwtProperties() {
        return jwtProperties;
    }

    public void setJwtProperties(@Nullable JwtProperties jwtProperties) {
        this.jwtProperties = jwtProperties;
    }

    public enum SecurityType {
        BASIC,
        JWT
    }

    public static class JwtProperties {
        private Long expirationInSeconds;
        private String audience;

        public Long getExpirationInSeconds() {
            return expirationInSeconds;
        }

        public void setExpirationInSeconds(Long expirationInSeconds) {
            this.expirationInSeconds = expirationInSeconds;
        }

        public String getAudience() {
            return audience;
        }

        public void setAudience(String audience) {
            this.audience = audience;
        }
    }
}
