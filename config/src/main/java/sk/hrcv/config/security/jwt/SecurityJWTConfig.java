package sk.hrcv.config.security.jwt;

import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

public class SecurityJWTConfig extends WebSecurityConfigurerAdapter {

    private final AuthenticationProvider authenticationProvider;
    private final AuthenticationFilter authenticationFilter;
    private final AuthenticationExceptionHandler authenticationExceptionHandler;

    public SecurityJWTConfig(AuthenticationProvider authenticationProvider, AuthenticationFilter authenticationFilter, AuthenticationExceptionHandler authenticationExceptionHandler) {
        this.authenticationProvider = authenticationProvider;
        this.authenticationFilter = authenticationFilter;
        this.authenticationExceptionHandler = authenticationExceptionHandler;
    }

    @Override
    public void configure(HttpSecurity http) throws Exception {
        http.csrf().disable();
        http.headers().frameOptions().disable();
        http.authorizeRequests()
                .antMatchers("/h2/**").permitAll()
                .antMatchers("/api/v1").authenticated()
                .antMatchers("/api/v1/**").authenticated()
                .antMatchers("/api/v1/*").authenticated()
                .and()
                .authenticationProvider(authenticationProvider)
                .addFilterBefore(authenticationFilter, UsernamePasswordAuthenticationFilter.class)
                .exceptionHandling()
                .authenticationEntryPoint(authenticationExceptionHandler);
    }
}
