package sk.hrcv.config.security.jwt

import org.springframework.security.core.Authentication
import org.springframework.security.core.GrantedAuthority
import java.util.*

data class JsonWebToken(
    val token: String
) : Authentication {

    override fun getName(): String {
        return "Not defined"
    }

    override fun getAuthorities(): MutableCollection<out GrantedAuthority> {
        return Collections.emptyList()
    }

    override fun getCredentials(): Any = token

    override fun getDetails(): Any {
        return ""
    }

    override fun getPrincipal(): Any {
        return ""
    }

    override fun isAuthenticated(): Boolean = false

    override fun setAuthenticated(authenticated: Boolean) {

    }
}