package sk.hrcv.config.security.basic;

import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.sql.DataSource;

public class SecurityBasicConfig extends WebSecurityConfigurerAdapter {

    private static final String REALM_NAME = "Foosball";
    private static final String USERS_BY_USERNAME = "select username, e_password, enabled from AUTH.USER where username = ?";
    private static final String AUTHORITIES_BY_USERNAME = "select username, authority from AUTH.AUTHORITIES where username = ?";

    private final DataSource dataSource;

    public SecurityBasicConfig(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @Override
    protected void configure(AuthenticationManagerBuilder builder) throws Exception {
        builder.jdbcAuthentication()
                .passwordEncoder(getPasswordEncoder())
                .dataSource(dataSource)
                .usersByUsernameQuery(USERS_BY_USERNAME)
                .authoritiesByUsernameQuery(AUTHORITIES_BY_USERNAME);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.headers().frameOptions().disable()
                .and().httpBasic().realmName(REALM_NAME)
                .and().sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and().csrf().disable();
        http.authorizeRequests()
                .antMatchers("/h2/**").permitAll()
                .anyRequest().authenticated()
                .and().formLogin();
    }

//    @Bean
//    public WebSecurityConfigurerAdapter webSecurityConfig() {
//        return new WebSecurityConfigurerAdapter() {
//            @Override
//            protected void configure(HttpSecurity http) throws Exception {
//
//            }
//
//            @Override
//            protected void configure(AuthenticationManagerBuilder builder) throws Exception {
//
//            }
//        };
//    }

    private PasswordEncoder getPasswordEncoder() {
        return new BCryptPasswordEncoder();
    }
}


