package sk.hrcv.config.security.jwt;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class AuthenticationFilter implements Filter {

    private static final Logger log = LoggerFactory.getLogger(AuthenticationFilter.class);
    private static final String ACTUATOR_SUFFIX = "actuator";
    private static final String H2_SUFFIX = "h2";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        log.debug("init jwt");
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        String authorization = ((HttpServletRequest) servletRequest).getHeader(HttpHeaders.AUTHORIZATION);
        String requestURI = ((HttpServletRequest) servletRequest).getRequestURI();

        if (requestURI.contains(ACTUATOR_SUFFIX) || requestURI.contains(H2_SUFFIX)) {
            log.debug("Authorization is not required for {}", requestURI);
            filterChain.doFilter(servletRequest, servletResponse);
            return;
        }

        if (authorization == null) {
            log.debug("Authorization needed from {}", requestURI);
            ((HttpServletResponse) servletResponse).setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        } else {
            JsonWebToken jsonWebToken = new JsonWebToken(authorization);
            SecurityContextHolder.getContext().setAuthentication(jsonWebToken);

            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    @Override
    public void destroy() {

    }
}
