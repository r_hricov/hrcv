package sk.hrcv.config.exception;

public class CommonBusinessException extends RuntimeException {

    public CommonBusinessException(String s) {
        super(s);
    }

}