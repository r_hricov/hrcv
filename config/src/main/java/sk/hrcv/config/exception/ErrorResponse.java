package sk.hrcv.config.exception;

import javax.annotation.Nullable;

public class ErrorResponse {

    @Nullable
    private String errorMessage;
    private String errorCode;

    @Nullable
    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(@Nullable String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }
}
