SETLOCAL

echo %JBOSS_HOME%

call "%JBOSS_HOME%\bin\standalone.bat" ^
    -P  ..\..\..\src\main\resources\jboss-conf\app.properties ^
    -Djboss.server.base.dir=..\..\..\src\main\resources\jboss-conf ^
    -Djboss.server.log.dir=..\..\..\src\main\resources\log
    -Djboss.bind.address=0.0.0.0

ENDLOCAL
EXIT /B